<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('index');
}); */
Route::get('/', function () {
    return view('index');
});

				//Registration Controllers
Route::get('/register', 'RegistrationController@create');
Route::post('register', 'RegistrationController@store');
    
                //Login Controllers
Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');

               //Pages Routes
Route::get('/Markets', function () {
    return view('Markets');
});
Route::get('/New-Account', function () {
    return view('New-Account');
});
Route::get('/Bittrex-Support', function () {
    return view('Bittrex-Support');
});
Route::get('/News-and-Announcements', function () {
    return view('News-and-Announcements');
});
Route::get('/Announcements', function () {
    return view('Announcements');
});
Route::get('/Submit-Request', function () {
    return view('Submit-Request');
}); 
Route::get('/sectionSummary', function () {
    return view('sectionSummary');
}); 
Route::get('/sectionApi', function () {
    return view('sectionApi');
}); 
Route::get('/sectionIpAddressWhiteList', function () {
    return view('sectionIpAddressWhiteList');
}); 
Route::get('/sectionEnableAccount', function () {
    return view('sectionEnableAccount');
}); 
Route::get('/sectionEnhancedVerification', function () {
    return view('sectionEnhancedVerification');
}); 
Route::get('/sectionUi', function () {
    return view('sectionUi');
}); 
Route::get('/sectionNotifications', function () {
    return view('sectionNotifications');
}); 
Route::get('/sectionPassword', function () {
    return view('sectionPassword');
}); 
Route::get('/sectionBasicVerification', function () {
    return view('sectionBasicVerification');
}); 
Route::get('/section2Fa', function () {
    return view('section2Fa');
}); 
Route::get('/sectionWithdrawAddressWhiteList', function () {
    return view('sectionWithdrawAddressWhiteList');
}); 

			//Front Pages Roures End


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
