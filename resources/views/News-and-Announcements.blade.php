
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
  <meta charset="utf-8">
  <!-- v13576 -->
  <title>News and Announcements &ndash; Bittrex Support</title>

  <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="2ExuITckv8s/FOFIcmnec0JS++CVaPcJpQmtikW1PgyaWu0VItIN7WurriiIByTGlrCHAlECojpY3P5QKPegVA==" />
  <link rel="canonical" href="https://support.bittrex.com/hc/en-us/categories/200236600-News-and-Announcements" />
<link rel="alternate" hreflang="en" href="https://support.bittrex.com/hc/en-us/categories/200236600-News-and-Announcements" />

  <!-- Entypo pictograms by Daniel Bruce — www.entypo.com -->
  <link rel="stylesheet" media="all" href="//p13.zdassets.com/hc/assets/application-3b0b6df180f05e3fa954d2e4d90e4600.css" id="stylesheet" />
  <link rel="stylesheet" type="text/css" href="//p13.zdassets.com/hc/themes/478848/115000640712/style-badae25e3dedd74860d69cd8fe0c5fcc.css?brand_id=149544&amp;locale=en-us" />

  <link rel="shortcut icon" type="image/x-icon" href="//p13.zdassets.com/hc/settings_assets/478848/200016824/fLQwWyw6nq0FP0k0jDd9Hw-bittrex-favicon-32x32.png" />

  <!--[if lt IE 9]>
  <script>
    //Enable HTML5 elements for <IE9
    'abbr article aside audio bdi canvas data datalist details dialog \
    figcaption figure footer header hgroup main mark meter nav output \
    progress section summary template time video'.replace(/\w+/g,function(n){document.createElement(n)});
  </script>
<![endif]-->

  <script src="//p13.zdassets.com/hc/assets/jquery-b60ddb79ff2563b75442a6bac88b00b5.js"></script>
  
  
  

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,300,300italic"  type="text/css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/highlight.js/9.10.0/styles/github.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.magnific-popup/1.0.0/magnific-popup.css" />
<style>
.hero-bg {
  background-image: url(//p13.zdassets.com/hc/theme_assets/478848/200016824/cover-1.3.jpg);
}
</style>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script>var cfaRules = [{"fieldType":"tagger","field":360000516212,"value":"need_new_account_created_yes","select":[360000543171,360000518832],"formId":114093958552,"requireds":[360000518832,360000543171]},{"fieldType":"tagger","field":360000517092,"value":"outside_united_states","select":[360000543651],"formId":114093958552,"requireds":[360000543651]},{"fieldType":"tagger","field":360000542871,"value":"corporate_account","select":[360000544311,360000544331,114096792652,360000517092],"formId":114093958552,"requireds":[360000544311,360000544331,114096792652,360000517092]}];</script>
<script src="//cdn.jsdelivr.net/jquery.magnific-popup/1.0.0/jquery.magnific-popup.min.js"></script>
<script src="//cdn.jsdelivr.net/highlight.js/9.10.0/highlight.min.js"></script>

  <script type="text/javascript" src="//p13.zdassets.com/hc/themes/478848/115000640712/script-badae25e3dedd74860d69cd8fe0c5fcc.js?brand_id=149544&amp;locale=en-us"></script>
</head>
<body class="">
  <div class="layout">
  <header class="topbar container" data-topbar>
    <div class="container-inner">
      <div class="topbar__inner">
        <div class="topbar__col clearfix">
          <div class="logo-wrapper">
            <div class="logo">
              <a title="Home" href="Bittrex-Support">
                <img src="//p13.zdassets.com/hc/settings_assets/478848/200016824/xSaHUeYiwl0eOCb0Fou1tg-bittrex-logo.svg" alt="Logo">
              </a> 
            </div>
          </div>
          <!-- <p class="help-center-name">Bittrex Support</p> -->
          <button type="button" role="button" aria-label="Toggle Navigation" class="lines-button x" data-toggle-menu> <span class="lines"></span> </button>
        </div>
        <div class="topbar__col topbar__menu">
          <div class="topbar__collapse" data-menu>
            <div class="topbar__controls">
               
              <a class="btn btn--topbar submit-a-request" href="Submit-Request">Submit a request</a> 
                <a class="login" data-auth-action="signin" role="button" href="login">Sign in</a>

            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  

  <main role="main">
    <div class="container category-page">
  <div class="container-inner">
    <div class="row clearfix">
  <div class="column column--sm-8">
    <ol class="breadcrumbs">
  
    <li title="Bittrex Support">
      
        <a href="Bittrex-Support">Bittrex Support</a>
      
    </li>
  
    <li title="News and Announcements">
      
        News and Announcements
      
    </li>
  
</ol>

  </div>
  <div class="column column--sm-4">
    <div class="search-box search-box--small">
      <form role="search" class="search" data-search="" data-instant="true" autocomplete="off" action="/hc/en-us/search" accept-charset="UTF-8" method="get"><input name="utf8" type="hidden" value="&#x2713;" /><input type="search" name="query" id="query" placeholder="Search" autocomplete="off" aria-label="Search" /></form>
    </div>
  </div>
</div>


    <div class="page-header page-header--with-border">
      <h1 class="h2">News and Announcements</h1>
      
    </div>

    
      <div class="row section-tree clearfix">
        
          <section class="column column--sm-6 column--md-4 section">
            <h3 class="section__title">
              
              <a class="section__title-link" href="/hc/en-us/sections/200142394-Announcements">Announcements</a>
            </h3>

            
              <ul class="article-list">
                
                <li class="article-list-item article-list-item--is-promoted" >
  
    <span class="fa fa-star" title="Promoted article"></span>
  
  <a class="article-list-item__link" href="/hc/en-us/articles/115000961172-Bittrex-s-Crosschain-Recovery-Policy">Bittrex&#39;s Crosschain Recovery Policy</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/360000857772-ETH-ETH-Token-Single-Address">ETH/ETH Token Single Address</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/115003463331-New-Account-Sign-Ups">New Account Sign Ups</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/115003209552-Important-Information-About-Identity-Verification">Important Information About Identity Verification</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/115002187632-Statement-on-disabled-accounts">Statement on disabled accounts</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/115001499832-Self-Service-Account-Recovery">Self-Service Account Recovery</a>
</li>

                
              </ul>
              
                <a href="/hc/en-us/sections/200142394-Announcements" class="btn btn--default see-all-articles">
                  See all 23 articles
                </a>
              
            
          </section>
        
          <section class="column column--sm-6 column--md-4 section">
            <h3 class="section__title">
              
              <a class="section__title-link" href="/hc/en-us/sections/200560334-Coin-Removals">Coin Removals</a>
            </h3>

            
              <ul class="article-list">
                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/360000985452-Pending-Market-Removals-3-2-2018">Pending Market Removals 3/2/2018</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/360000560051-Pending-Market-Removals-2-9-2018">Pending Market Removals 2/9/2018</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/115003977472-Pending-Market-Removals-1-26-2018">Pending Market Removals 1/26/2018</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/115003816652-Pending-Market-Removals-1-12-2018">Pending Market Removals 1/12/2018</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/115003472251-Pending-Market-Removals-12-22-2017">Pending Market Removals 12/22/2017</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/115003346031-Pending-Market-Removals-12-15-2017">Pending Market Removals 12/15/2017</a>
</li>

                
              </ul>
              
                <a href="/hc/en-us/sections/200560334-Coin-Removals" class="btn btn--default see-all-articles">
                  See all 100 articles
                </a>
              
            
          </section>
        
          <section class="column column--sm-6 column--md-4 section">
            <h3 class="section__title">
              
              <a class="section__title-link" href="/hc/en-us/sections/200567324-Changelist">Changelist</a>
            </h3>

            
              <ul class="article-list">
                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/115000152972-Changelist-05-09-17">Changelist 05/09/17</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/203302154-Changelist-10-28-14">Changelist 10/28/14</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/202998344-Changelist-09-16-14">Changelist 09/16/14</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/203185350-Changelist-09-09-14">Changelist 09/09/14</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/203150570-Changelist-09-02-14">Changelist 09/02/14</a>
</li>

                
                <li class="article-list-item " >
  
  <a class="article-list-item__link" href="/hc/en-us/articles/203147780-Changelist-08-31-14">Changelist 08/31/14</a>
</li>

                
              </ul>
              
                <a href="/hc/en-us/sections/200567324-Changelist" class="btn btn--default see-all-articles">
                  See all 11 articles
                </a>
              
            
          </section>
        
      </div>
    
  </div>
</div>

  </main>
  </div>
<!-- /.layout -->
<footer class="footer container">
  <div class="container-inner footer__inner">
    <div class="footer__col copyright">
      <p>&copy; Bittrex Support</p>
    </div>
    <div class="footer__col footer__col--social-links">
      <a href="https://www.facebook.com/bittrex" target="_blank" class="footer-social-link fa fa-facebook"></a>
      <a href="https://twitter.com/BittrexExchange" target="_blank" class="footer-social-link fa fa-twitter"></a>
    </div>
  </div>
</footer>
<a href="#" class="scroll-to-top fa fa-angle-up" data-scroll-to-top></a>



  <!-- / -->

  <script type="text/javascript" src="//p13.zdassets.com/hc/assets/locales/en-us-e9636f54e909a07cbf366cd8414b9cff.js"></script>
  <script src="https://bittrex.zendesk.com/auth/v2/host.js" data-brand-id="149544" data-return-to="https://support.bittrex.com/hc/en-us/categories/200236600-News-and-Announcements" data-theme="hc" data-locale="1" data-auth-origin="149544,true,true"></script>
  <script type="text/javascript" src="https://p13.zdassets.com/assets/zendesk_pci_hc.v4.js"></script>

  <script type="text/javascript">
  /*

    Greetings sourcecode lurker!

    This is for internal Zendesk and legacy usage,
    we don't support or guarantee any of these values
    so please don't build stuff on top of them.

  */

  HelpCenter = {};
  HelpCenter.account = {"subdomain":"bittrex","environment":"production","name":"Bittrex Support"};
  HelpCenter.user = {"identifier":"da39a3ee5e6b4b0d3255bfef95601890afd80709","email":null,"name":null,"role":"anonymous","avatar_url":"https://assets.zendesk.com/hc/assets/default_avatar.png","organizations":[],"groups":[]};
  HelpCenter.internal = {"asset_url":"//p13.zdassets.com/hc/assets/","current_session":{"locale":"en-us","csrf_token":"E1SziJ9N3dV/1ZvklJTthG83+h7Jcs77Nwx2q3p0vqRRQjC8irtv8ytq1IRu+hcxu9WG/A0Ym8jK2SVxFzYg/A==","shared_csrf_token":null},"settings":{"zopim_enabled":true,"spam_filter_enabled":true},"current_record_id":null,"current_record_url":null,"current_record_title":null,"search_results_count":null,"current_text_direction":"ltr","current_brand_url":"https://bittrex.zendesk.com","current_host_mapping":"support.bittrex.com","current_path":"/hc/en-us/categories/200236600-News-and-Announcements","authentication_domain":"https://bittrex.zendesk.com","show_autocomplete_breadcrumbs":true,"user_info_changing_enabled":false,"has_user_profiles_enabled":true,"has_anonymous_kb_voting":false,"has_advanced_upsell":false,"has_multi_language_help_center":true,"mobile_device":false,"mobile_site_enabled":false,"show_at_mentions":false,"has_copied_content":false,"embeddables_config":{"embeddables_web_widget":false,"embeddables_automatic_answers":true,"embeddables_connect_ipms":false},"embeddables_domain":"zendesk.com","answer_bot_subdomain":"static","plans_url":"https://support.bittrex.com/hc/admin/plan?locale=en-us","manage_content_url":"https://support.bittrex.com/hc/en-us","arrange_content_url":"https://support.bittrex.com/hc/admin/arrange_contents?locale=en-us","general_settings_url":"https://support.bittrex.com/hc/admin/general_settings?locale=en-us","user_segments_url":"https://support.bittrex.com/hc/admin/user_segments?locale=en-us","import_articles_url":"https://support.bittrex.com/hc/admin/import_articles?locale=en-us","has_community_enabled":false,"has_multiselect_field":true,"has_groups":true,"has_internal_sections":true,"has_organizations":true,"has_tag_restrictions":true,"has_answer_bot_web_form_enabled":false,"has_answer_bot_embeddable_standalone":true,"billing_url":"/access/return_to?return_to=https://bittrex.zendesk.com/billing","has_answer_bot":true,"has_guide_docs_importer":false,"answer_bot_management_url":"https://support.bittrex.com/hc/admin/answer_bot?locale=en-us","is_account_owner":false,"has_theming_templates":false,"theming_center_url":"https://support.bittrex.com/theming","theming_cookie_key":"hc-da39a3ee5e6b4b0d3255bfef95601890afd80709-preview","is_preview":false};
</script>

  <script src="//p13.zdassets.com/hc/assets/hc_enduser-99ecbf6c6034819f1a2d9f538431319f.js"></script>
  

  <script type="text/javascript">
    (function() {
  var Tracker = {};

  Tracker.track = function(eventName, data) {
    var url = "https://support.bittrex.com/hc/tracking/events?locale=en-us";

    var payload = {
      "event": eventName,
      "data": data,
      "referrer": document.referrer
    };

    var xhr = new XMLHttpRequest();

    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    xhr.send(JSON.stringify(payload));
  };

    Tracker.track("category_viewed", "BAh7BzoQY2F0ZWdvcnlfaWRpBDhe7ws6C2xvY2FsZUkiCmVuLXVzBjoGRVQ=--e7999ffc732f4513ec4db07a99018385a6fbd30f");
})();

  </script>
</body>
</html>