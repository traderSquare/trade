<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
  <meta charset="utf-8">
  <!-- v13576 -->
  <title>New Account Sign Ups &ndash; Bittrex Support</title>

  <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="6DGsuD7oor81cO3TZZ4ldcw06yrW/GOO9cfJtl4SOLyqJy+MKx4QmWHPorOf8N/AGNaXyBKWNr0IEppsM1Cm5A==" />
  <meta name="description" content="Dear new users: We are currently only accepting new account requests from our corporate customers and select invitees. We are excited to..." /><meta property="og:image" content="https://p13.zdassets.com/hc/settings_assets/478848/200016824/xSaHUeYiwl0eOCb0Fou1tg-bittrex-logo.svg" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="Bittrex Support" />
<meta property="og:title" content="New Account Sign Ups" />
<meta property="og:description" content="Dear new users: We are currently only accepting new account requests from our corporate customers and select invitees. We are excited to have so many new users who want to join the Bittrex communit..." />
<meta property="og:url" content="http://support.bittrex.com/hc/en-us/articles/115003463331-New-Account-Sign-Ups" />
<link rel="canonical" href="https://support.bittrex.com/hc/en-us/articles/115003463331-New-Account-Sign-Ups" />
<link rel="alternate" hreflang="en" href="https://support.bittrex.com/hc/en-us/articles/115003463331-New-Account-Sign-Ups" />

  <!-- Entypo pictograms by Daniel Bruce — www.entypo.com -->
  <link rel="stylesheet" media="all" href="//p13.zdassets.com/hc/assets/application-3b0b6df180f05e3fa954d2e4d90e4600.css" id="stylesheet" />
  <link rel="stylesheet" type="text/css" href="//p13.zdassets.com/hc/themes/478848/115000640712/style-badae25e3dedd74860d69cd8fe0c5fcc.css?brand_id=149544&amp;locale=en-us" />

  <link rel="shortcut icon" type="image/x-icon" href="//p13.zdassets.com/hc/settings_assets/478848/200016824/fLQwWyw6nq0FP0k0jDd9Hw-bittrex-favicon-32x32.png" />

  <!--[if lt IE 9]>
  <script>
    //Enable HTML5 elements for <IE9
    'abbr article aside audio bdi canvas data datalist details dialog \
    figcaption figure footer header hgroup main mark meter nav output \
    progress section summary template time video'.replace(/\w+/g,function(n){document.createElement(n)});
  </script>
<![endif]-->

  <script src="//p13.zdassets.com/hc/assets/jquery-b60ddb79ff2563b75442a6bac88b00b5.js"></script>
  
  
  

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,300,300italic"  type="text/css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/highlight.js/9.10.0/styles/github.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.magnific-popup/1.0.0/magnific-popup.css" />
<style>
.hero-bg {
  background-image: url(//p13.zdassets.com/hc/theme_assets/478848/200016824/cover-1.3.jpg);
}
</style>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script>var cfaRules = [{"fieldType":"tagger","field":360000516212,"value":"need_new_account_created_yes","select":[360000543171,360000518832],"formId":114093958552,"requireds":[360000518832,360000543171]},{"fieldType":"tagger","field":360000517092,"value":"outside_united_states","select":[360000543651],"formId":114093958552,"requireds":[360000543651]},{"fieldType":"tagger","field":360000542871,"value":"corporate_account","select":[360000544311,360000544331,114096792652,360000517092],"formId":114093958552,"requireds":[360000544311,360000544331,114096792652,360000517092]}];</script>
<script src="//cdn.jsdelivr.net/jquery.magnific-popup/1.0.0/jquery.magnific-popup.min.js"></script>
<script src="//cdn.jsdelivr.net/highlight.js/9.10.0/highlight.min.js"></script>

  <script type="text/javascript" src="//p13.zdassets.com/hc/themes/478848/115000640712/script-badae25e3dedd74860d69cd8fe0c5fcc.js?brand_id=149544&amp;locale=en-us"></script>
</head>
<body class="">
  <div class="layout">
  <header class="topbar container" data-topbar>
    <div class="container-inner">
      <div class="topbar__inner">
        <div class="topbar__col clearfix">
          <div class="logo-wrapper">
            <div class="logo">
              <a title="Home" href="Bittrex-Support">
                <img src="//p13.zdassets.com/hc/settings_assets/478848/200016824/xSaHUeYiwl0eOCb0Fou1tg-bittrex-logo.svg" alt="Logo">
              </a> 
            </div>
          </div>
          <!-- <p class="help-center-name">Bittrex Support</p> -->
          <button type="button" role="button" aria-label="Toggle Navigation" class="lines-button x" data-toggle-menu> <span class="lines"></span> </button>
        </div>
        <div class="topbar__col topbar__menu">
          <div class="topbar__collapse" data-menu>
            <div class="topbar__controls">
               
              <a class="btn btn--topbar submit-a-request" href="Submit-Request">Submit a request</a> 
                <a class="login" role="button" href="login">Sign in</a>

            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  

  <main role="main">
    <div class="container article-page">
  <div class="container-inner">
    <div class="row clearfix">
  <div class="column column--sm-8">
    <ol class="breadcrumbs">
  
    <li title="Bittrex Support">
      
        <a href="Bittrex-Support">Bittrex Support</a>
      
    </li>
  
    <li title="News and Announcements">
      
        <a href="News-and-Announcements">News and Announcements</a>
      
    </li>
  
    <li title="Announcements">
      
        <a href="Announcements">Announcements</a>
      
    </li>
  
</ol>

  </div>
  <div class="column column--sm-4">
    <div class="search-box search-box--small">
      <form role="search" class="search" data-search="" data-instant="true" autocomplete="off" action="/hc/en-us/search" accept-charset="UTF-8" method="get"><input name="utf8" type="hidden" value="&#x2713;" /><input type="search" name="query" id="query" placeholder="Search" autocomplete="off" aria-label="Search" /></form>
    </div>
  </div>
</div>


    <div class="row clearfix">
      <div class="column column--sm-8">
        <article class="article clearfix" itemscope itemtype="http://schema.org/Article">
          <header class="article-header">
            <h1 class="article__title" itemprop="name">
              
              New Account Sign Ups
            </h1>

            <div class="article-meta">
              <div class="article-meta__col article-meta__col--main">
                <div class="entry-info ">
                  <div class="entry-info__avatar">
                    <div class="avatar avatar--agent">
                      <img class="user-avatar user-avatar--default" src="https://secure.gravatar.com/avatar/625edc9d617a6e5d2abf0d368dc33ce3?default=https%3A%2F%2Fassets.zendesk.com%2Fhc%2Fassets%2Fdefault_avatar.png&amp;r=g" alt="Avatar">
                    </div>
                  </div>
                  <div class="entry-info__content">
                    <b class="author">
                      <a href="/hc/en-us/profiles/304971319-Bill">
                        Bill
                      </a>
                    </b>
                    <div class="meta"><time datetime="2017-12-15T16:21:27Z" title="2017-12-15T16:21:27Z" data-datetime="calendar">December 15, 2017 16:21</time></div>
                  </div>
                </div>
              </div>
              <div class="article-meta__col article-meta__col--button">
                <a class="article-subscribe" rel="nofollow" role="button" data-auth-action="signin" aria-selected="false" href="#">Follow</a>
              </div>
            </div>
          </header>

          <div class="article__body markdown" itemprop="articleBody">
            <p>Dear new users:<br /> <br />We are currently only accepting new account requests from our corporate customers and select invitees. We are excited to have so many new users who want to join the Bittrex community. To those that want to join our community, we will keep you posted on when we open up registration more broadly. If you already have an account on Bittrex, you will not be affected by this change. Please continue to the log in as you normally do.<br /> <br />If you are a corporate account or a select invitee, please click the button below to apply for a new account. Given the volume of requests, we may not be able to respond to every request for a new account at this time.</p>
<p class="wysiwyg-text-align-center"><a class="btn btn--primary" href="Submit-Request" target="_blank" rel="noopener">New Account Application Form</a></p>
<p>The Bittrex Team</p>
          </div>

          <div class="article__attachments">
            
          </div>

        </article>

        <footer class="article-footer clearfix">
          
            <div class="article-vote">
              <span class="article-vote-question">Was this article helpful?</span>
              <div class="article-vote-controls">
                <a role="button" rel="nofollow" class="fa fa-thumbs-up article-vote-controls__item article-vote-controls__item--up" title="Yes" aria-selected="false" data-auth-action="signin" href="/hc/en-us/signin?return_to=https%3A%2F%2Fsupport.bittrex.com%2Fhc%2Fen-us%2Farticles%2F115003463331-New-Account-Sign-Ups"></a>
                <a role="button" rel="nofollow" class="fa fa-thumbs-down article-vote-controls__item article-vote-controls__item--down" title="No" aria-selected="false" data-auth-action="signin" href="/hc/en-us/signin?return_to=https%3A%2F%2Fsupport.bittrex.com%2Fhc%2Fen-us%2Farticles%2F115003463331-New-Account-Sign-Ups"></a>
              </div>
              <small class="article-vote-count">
                <span class="article-vote-label">4409 out of 7671 found this helpful</span>
              </small>
            </div>
          
        </footer>

        <div class="article__share"><ul class="share">
  <li><a href="https://www.facebook.com/share.php?title=New+Account+Sign+Ups&u=https%3A%2F%2Fsupport.bittrex.com%2Fhc%2Fen-us%2Farticles%2F115003463331-New-Account-Sign-Ups" class="share-facebook">Facebook</a></li>
  <li><a href="https://twitter.com/share?lang=en&text=New+Account+Sign+Ups&url=https%3A%2F%2Fsupport.bittrex.com%2Fhc%2Fen-us%2Farticles%2F115003463331-New-Account-Sign-Ups" class="share-twitter">Twitter</a></li>
  <li><a href="https://www.linkedin.com/shareArticle?mini=true&source=Bittrex+Support&title=New+Account+Sign+Ups&url=https%3A%2F%2Fsupport.bittrex.com%2Fhc%2Fen-us%2Farticles%2F115003463331-New-Account-Sign-Ups" class="share-linkedin">LinkedIn</a></li>
  <li><a href="https://plus.google.com/share?hl=en-us&url=https%3A%2F%2Fsupport.bittrex.com%2Fhc%2Fen-us%2Farticles%2F115003463331-New-Account-Sign-Ups" class="share-googleplus">Google+</a></li>
</ul>
</div>

        <div class="article-more-questions">Have more questions? <a href="Submit-Request">Submit a request</a></div>

        
<!--
        <section class="comments" id="comments" itemscope itemtype="http://schema.org/UserComments">
          <div class="comments__header">
            <h3>Comments</h3>

            <div class="comment-sorter">
              <div class="comment-sorter__col comment-sorter__col--main"> 
                0 comments
              </div>

              
            </div>
          </div>

          

          <p class="comments__callout">Please <a data-auth-action="signin" href="/hc/en-us/signin?return_to=https%3A%2F%2Fsupport.bittrex.com%2Fhc%2Fen-us%2Farticles%2F115003463331">sign in</a> to leave a comment.</p>

          
        </section>

        -->
      </div>
      <div class="column column--sm-4">
        <div class="article-sidebar">
          <section class="section-articles">
            <h3 class="section-articles__title">Articles in this section</h3>
            <ul class="section-articles__list">
              
                <li class="section-articles__item">
                  <a href="/hc/en-us/articles/115000961172-Bittrex-s-Crosschain-Recovery-Policy" class="section-articles__link ">Bittrex&#39;s Crosschain Recovery Policy</a>
                </li>
              
                <li class="section-articles__item">
                  <a href="/hc/en-us/articles/360000857772-ETH-ETH-Token-Single-Address" class="section-articles__link ">ETH/ETH Token Single Address</a>
                </li>
              
                <li class="section-articles__item">
                  <a href="/hc/en-us/articles/115003463331-New-Account-Sign-Ups" class="section-articles__link is-active">New Account Sign Ups</a>
                </li>
              
                <li class="section-articles__item">
                  <a href="/hc/en-us/articles/115003209552-Important-Information-About-Identity-Verification" class="section-articles__link ">Important Information About Identity Verification</a>
                </li>
              
                <li class="section-articles__item">
                  <a href="/hc/en-us/articles/115002187632-Statement-on-disabled-accounts" class="section-articles__link ">Statement on disabled accounts</a>
                </li>
              
                <li class="section-articles__item">
                  <a href="/hc/en-us/articles/115001499832-Self-Service-Account-Recovery" class="section-articles__link ">Self-Service Account Recovery</a>
                </li>
              
                <li class="section-articles__item">
                  <a href="/hc/en-us/articles/115000318252-Performance-Improvements-Part-1" class="section-articles__link ">Performance Improvements - Part 1</a>
                </li>
              
                <li class="section-articles__item">
                  <a href="/hc/en-us/articles/115004827287-Fast-Deposit-Program" class="section-articles__link ">Fast Deposit Program</a>
                </li>
              
                <li class="section-articles__item">
                  <a href="/hc/en-us/articles/115004479127-Statement-about-potential-Bitcoin-hard-fork" class="section-articles__link ">Statement about potential Bitcoin hard fork</a>
                </li>
              
                <li class="section-articles__item">
                  <a href="/hc/en-us/articles/236016728-Security-Announcement-Keeping-Your-Account-Secure" class="section-articles__link ">Security Announcement - Keeping Your Account Secure</a>
                </li>
              
            </ul>
            
              <a href="/hc/en-us/sections/200142394-Announcements" class="btn btn--default">See more</a>
            
          </section>
        </div>
      </div>
    </div>
  </div>
</div>

  </main>

  </div>
<!-- /.layout -->
<footer class="footer container">
  <div class="container-inner footer__inner">
    <div class="footer__col copyright">
      <p>&copy; Bittrex Support</p>
    </div>
    <div class="footer__col footer__col--social-links">
      <a href="https://www.facebook.com/bittrex" target="_blank" class="footer-social-link fa fa-facebook"></a>
      <a href="https://twitter.com/BittrexExchange" target="_blank" class="footer-social-link fa fa-twitter"></a>
    </div>
  </div>
</footer>
<a href="#" class="scroll-to-top fa fa-angle-up" data-scroll-to-top></a>



  <!-- / -->

  <script type="text/javascript" src="//p13.zdassets.com/hc/assets/locales/en-us-e9636f54e909a07cbf366cd8414b9cff.js"></script>
  <script src="https://bittrex.zendesk.com/auth/v2/host.js" data-brand-id="149544" data-return-to="https://support.bittrex.com/hc/en-us/articles/115003463331" data-theme="hc" data-locale="1" data-auth-origin="149544,true,true"></script>
  <script type="text/javascript" src="https://p13.zdassets.com/assets/zendesk_pci_hc.v4.js"></script>

  <script type="text/javascript">
  /*

    Greetings sourcecode lurker!

    This is for internal Zendesk and legacy usage,
    we don't support or guarantee any of these values
    so please don't build stuff on top of them.

  */

  HelpCenter = {};
  HelpCenter.account = {"subdomain":"bittrex","environment":"production","name":"Bittrex Support"};
  HelpCenter.user = {"identifier":"da39a3ee5e6b4b0d3255bfef95601890afd80709","email":null,"name":null,"role":"anonymous","avatar_url":"https://assets.zendesk.com/hc/assets/default_avatar.png","organizations":[],"groups":[]};
  HelpCenter.internal = {"asset_url":"//p13.zdassets.com/hc/assets/","current_session":{"locale":"en-us","csrf_token":"UUJRZc1hR6F/u2qhgqyH8Wp8Of29QW1C9CFe8UvQA/MTVNJR2Jf1hysEJcF4wn1Evp5FH3krOHEJ9A0rJpKdqw==","shared_csrf_token":null},"settings":{"zopim_enabled":true,"spam_filter_enabled":true},"current_record_id":"115003463331","current_record_url":"/hc/en-us/articles/115003463331-New-Account-Sign-Ups","current_record_title":"New Account Sign Ups","search_results_count":null,"current_text_direction":"ltr","current_brand_url":"https://bittrex.zendesk.com","current_host_mapping":"support.bittrex.com","current_path":"/hc/en-us/articles/115003463331-New-Account-Sign-Ups","authentication_domain":"https://bittrex.zendesk.com","show_autocomplete_breadcrumbs":true,"user_info_changing_enabled":false,"has_user_profiles_enabled":true,"has_anonymous_kb_voting":false,"has_advanced_upsell":false,"has_multi_language_help_center":true,"mobile_device":false,"mobile_site_enabled":false,"show_at_mentions":false,"has_copied_content":false,"embeddables_config":{"embeddables_web_widget":false,"embeddables_automatic_answers":true,"embeddables_connect_ipms":false},"embeddables_domain":"zendesk.com","answer_bot_subdomain":"static","plans_url":"https://support.bittrex.com/hc/admin/plan?locale=en-us","manage_content_url":"https://support.bittrex.com/hc/en-us","arrange_content_url":"https://support.bittrex.com/hc/admin/arrange_contents?locale=en-us","general_settings_url":"https://support.bittrex.com/hc/admin/general_settings?locale=en-us","user_segments_url":"https://support.bittrex.com/hc/admin/user_segments?locale=en-us","import_articles_url":"https://support.bittrex.com/hc/admin/import_articles?locale=en-us","has_community_enabled":false,"has_multiselect_field":true,"has_groups":true,"has_internal_sections":true,"has_organizations":true,"has_tag_restrictions":true,"has_answer_bot_web_form_enabled":false,"has_answer_bot_embeddable_standalone":true,"billing_url":"/access/return_to?return_to=https://bittrex.zendesk.com/billing","has_answer_bot":true,"has_guide_docs_importer":false,"answer_bot_management_url":"https://support.bittrex.com/hc/admin/answer_bot?locale=en-us","is_account_owner":false,"has_theming_templates":false,"theming_center_url":"https://support.bittrex.com/theming","theming_cookie_key":"hc-da39a3ee5e6b4b0d3255bfef95601890afd80709-preview","is_preview":false};
</script>

  <script src="//p13.zdassets.com/hc/assets/hc_enduser-99ecbf6c6034819f1a2d9f538431319f.js"></script>
  

  <script type="text/javascript">
    (function() {
  var Tracker = {};

  Tracker.track = function(eventName, data) {
    var url = "https://support.bittrex.com/hc/tracking/events?locale=en-us";

    var payload = {
      "event": eventName,
      "data": data,
      "referrer": document.referrer
    };

    var xhr = new XMLHttpRequest();

    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    xhr.send(JSON.stringify(payload));
  };

    Tracker.track("article_viewed", "BAh7BzoLbG9jYWxlSSIKZW4tdXMGOgZFVDoPYXJ0aWNsZV9pZGwrCKOWvcYaAA==--586d3a87d6d76f50020f6249adfa5cc6ecb6272f");
})();

  </script>
</body>
</html>