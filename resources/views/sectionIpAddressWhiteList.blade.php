<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />

    <!-- One Page Module Specific Stylesheet -->
    <link rel="stylesheet" href="css/onepage.css" type="text/css" />
    <!-- / -->
     <!-- Slider Syle -->
    <link rel="stylesheet" href="css/slider.css" type="text/css" />
    <!-- / -->

    <link rel="stylesheet" href="css/dark.css" type="text/css" />
    <link rel="stylesheet" href="css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="css/et-line.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="css/fonts.css" type="text/css" />

    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title
    ============================================= -->
    <title>Bittrex.com - Manage Settings</title>
</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Header
        ============================================= -->

                </div>


        <header>
        <!-- #header Start-->
        
     <div id="header-navigation" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid container-header">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/Home/Markets">
                    <img src="{{ asset('assets/images/bittrex/bittrex-logo.png')}}" alt='Bittrex.com' height="24" />
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li class="dropdown" id="dash_menu_btc2">
                        <a href="#dash_menu_btc" class="dropdown-toggle" data-toggle="collapse">
                            <i class="fa fa-btc"></i>
                            <span>Markets</span>
                            <b class="caret"></b>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-left">
                    <li class="dropdown" id="dash_menu_fiat2">
                        <a href="#dash_menu_fiat" class="dropdown-toggle" data-toggle="collapse">
                            <i class="fa fa-usd"></i>
                            <span>Markets</span>
                            <b class="caret"></b>
                        </a>
                    </li>
                </ul>


        <ul class="nav navbar-nav navbar-right">

                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-flask"></i><span class="hidden-sm">&nbsp; Lab</span>  <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/Lab/Any2Any">
                                        <i class="fa fa-random"></i>
                                        <span>&nbsp; Any 2 Any</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/Lab/AutoSell">
                                        <i class="fa fa-recycle"></i>
                                        <span>&nbsp; Auto-Sell</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/Lab/ProfitLoss">
                                        <i class="fa fa-line-chart"></i>
                                        <span>&nbsp; Profit-Loss</span>
                                    </a>
                                </li>
                                </ul>

                                  <li id="dash_menu_history">
                            <a href="/History">
                                <i class="fa fa-calendar"></i>
                                <span class="hidden-sm">&nbsp; Orders</span>
                            </a>
                        </li>

                            <li id="dash_menu_wallet">
                            <a href="/Balance">
                                <i class="fa fa-btc"></i>
                                <span class="hidden-sm">&nbsp; Wallets</span>
                            </a>
                        </li>

                          <li id="dash_menu_settings">
                            <a href="/Manage">
                                <i class="fa fa-cog"></i>
                                <span class="hidden-sm">&nbsp; Settings</span>
                            </a>
                        </li>

                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-question-circle"></i><span class="hidden-sm">&nbsp; Help</span>  <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="dash_menu_news">
                                <a href="/News">
                                    <i class="fa fa-bullhorn"></i>
                                    <span>&nbsp; News</span>
                                </a>
                            </li>
                            <li id="dash_menu_status">
                                <a href="/Status">
                                    <i class="fa fa-lightbulb-o"></i>
                                    <span>&nbsp; Site Status</span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="/Home/About">About</a></li>
                            <li><a href="/Privacy">Your Privacy and Security</a></li>
                            <li><a href="/Home/Cookies">Cookie Policy</a></li>
                            <li><a href="/Home/Terms">Terms and Conditions</a></li>
                            <li><a href="/Home/Api">API Documentation</a></li>
                            <li><a href="/Fees">Fees</a></li>
                            <li><a href="https://bittrex.zendesk.com/hc/en-us" rel="noreferrer">Support</a></li>
                            <li><a href="/Home/Contact">Contact Us</a></li>
                        </ul>
                    </li>
        <li >
            <a href="/Account/Login">
                <i class="fa fa-sign-out"></i>
                <span >&nbsp; Logout </span>
            </a>
        </li>
    </ul>      
            </div>
        </div>
    </div> -->
</header>  
<!-- ##Header End -->
<br><br>
    <div id="media-width-detection-element"></div>
    <div id="layout-navigation" class="col-sm-12 menu-wrapper">
        <div id="dash_menu_btc" class="collapse menu-content" style="z-index:300">
            <p class="menu-sort">
                Sort By:
                <select data-bind="options: menu.availableSort, optionsText: 'sortName', value: menu.selectedSort, optionsCaption: 'Choose...'"></select>
                Max Rows:
                <select data-bind="options: menu.availableRows, optionsText: 'sortName', value: menu.selectedRows, optionsCaption: 'Choose...'"></select>
                Search:
                <input data-bind="value: menu.filter, valueUpdate: 'afterkeydown'" />
            </p>
            <table data-bind="foreach: menu.marketsBtcRows" class="menu-table">
                <tr data-bind="foreach: $data">
                    <td data-bind="css: { 'even': isEven(), 'odd': !isEven() }">
                        <div>
                            <div class="col-sm-2 name">
                                <a data-bind="attr: { href: urlPath, title:marketCurrencyName }, text:marketCurrency">Link</a>
                            </div>
                            <div class="col-sm-6 price"> <div data-bind="text: displayLast()"></div></div>
                            <div class="col-sm-4 delta"> <div data-bind="text: displayChange(), css: { 'dyn-div-up': change() > 0, 'dyn-div-down': change() < 0, 'dyn-div-none': change() == 0 }"></div></div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="dash_menu_fiat" class="collapse menu-content" style="z-index:300">
            <p class="menu-sort">
                Sort By:
                <select data-bind="options: menu.availableSort, optionsText: 'sortName', value: menu.selectedSort, optionsCaption: 'Choose...'"></select>
                Max Rows:
                <select data-bind="options: menu.availableRows, optionsText: 'sortName', value: menu.selectedRows, optionsCaption: 'Choose...'"></select>
                Search:
                <input data-bind="value: menu.filter, valueUpdate: 'afterkeydown'" />
            </p>
            <table data-bind="foreach: menu.marketsFiatRows" class="menu-table">
                <tr data-bind="foreach: $data">
                    <td data-bind="css: { 'even': isEven(), 'odd': !isEven() }">
                        <div>
                            <div class="col-sm-2 name">
                                <a data-bind="attr: { href: urlPath, title:marketCurrencyName }, text:marketCurrency">Link</a>
                            </div>
                            <div class="col-sm-6 price"> <div data-bind="text: displayLast()"></div></div>
                            <div class="col-sm-4 delta"> <div data-bind="text: displayChange(), css: { 'dyn-div-up': change() > 0, 'dyn-div-down': change() < 0, 'dyn-div-none': change() == 0 }"></div></div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="alert-news" class="alert alert-info alert-override" style="display:none; margin-top:48px; margin-bottom:0px">
        <i class="fa fa-exclamation-sign"></i>
        <span id="alert-news-text"></span>
        <a href="/News">&nbsp;For more information, see the News page.</a>
    </div>


<!-- <div id="pad-wrapper">
    <input name="__RequestVerificationToken" type="hidden" value="XPutwGf6rNU0_EgUiL_qu7S8hhgvdcaPQpPCB8TBuUvRedWYPGMnFYA3uBqny02rTJonqRJ1autgqWhgeMEOJ-09KobKD3-o6dHvS9f2bN3yenJRtZg5z7UGIQ0oAeWoSQ-rZg2" /> 
<div id="body-container">
        <div id="event-store"></div>
        <div class="content"></div>
<div class="container" id="manage-body">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="list-group" id="sidebar">
                    <a href="#sectionSummary" class="list-group-item active">
                        Summary
                    </a>
                    <hr />
                    <a href="#sectionBasicVerification" class="list-group-item" id="basicVerificationBtn">
                        Basic Verification
                    </a>
                    <a href="#sectionEnhancedVerification" class="list-group-item" id="enhancedVerificationBtn">
                        Enhanced Verification
                    </a>
                    <hr />
                    <a href="#sectionPassword" class="list-group-item" >
                        Password
                    </a>
                    <a href="#section2Fa" class="list-group-item" >
                        Two-Factor Authentication
                    </a>
                    <a href="#sectionApi" class="list-group-item">
                        API Keys
                    </a>
                    <a href="#sectionIpAddressWhiteList" class="list-group-item">
                        IP Whitelist
                    </a>
                    <a href="#sectionWithdrawAddressWhiteList" class="list-group-item">
                        Withdrawal Whitelist
                    </a>
                    <a href="#sectionEnableAccount" class="list-group-item">
                        Enable Account
                    </a>
                    <hr />
                    <a href="#sectionUi" class="list-group-item" >
                        UI Settings
                    </a>
                    <a href="#sectionNotifications" class="list-group-item" >
                        Notifications
                    </a>
                </div>
            </div>

<div class="col-md-9 col-xs-12" id="parentSection">
                <div class="manage-frame active" id="sectionSummary" >
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionBasicVerification" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionEnhancedVerification" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionPassword" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="section2Fa" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionApi" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionUi" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionNotifications" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionIpAddressWhiteList" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionWithdrawAddressWhiteList" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionEnableAccount" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div> -->


<div class="content">
            





<div id="pad-wrapper">
    <input name="__RequestVerificationToken" type="hidden" value="RiAQIDnYQHGGkLk6xrClIcsGwWNTW225zfGf7wcZTMDiEefA1lcveRi3mINMlR7i9NR-LBDPrJ06mSF0szXdyPLomqBjG-a4QGKX_Ts3pKRV08t5DvLXL__8kOnT8igvD9OdKA2">
    <div class="container" id="manage-body">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="list-group affix" id="sidebar">
                    <a href="#sectionSummary" class="list-group-item active">
                        Summary
                    </a>
                    <hr>
                    <a href="#sectionBasicVerification" class="list-group-item" id="basicVerificationBtn">
                        Basic Verification
                    </a>
                    <a href="#sectionEnhancedVerification" class="list-group-item" id="enhancedVerificationBtn">
                        Enhanced Verification
                    </a>
                    <hr>
                    <a href="#sectionPassword" class="list-group-item">
                        Password
                    </a>
                    <a href="#section2Fa" class="list-group-item">
                        Two-Factor Authentication
                    </a>
                    <a href="#sectionApi" class="list-group-item">
                        API Keys
                    </a>
                    <a href="#sectionIpAddressWhiteList" class="list-group-item">
                        IP Whitelist
                    </a>
                    <a href="#sectionWithdrawAddressWhiteList" class="list-group-item">
                        Withdrawal Whitelist
                    </a>
                    <a href="#sectionEnableAccount" class="list-group-item">
                        Enable Account
                    </a>
                    <hr>
                    <a href="#sectionUi" class="list-group-item">
                        UI Settings
                    </a>
                    <a href="#sectionNotifications" class="list-group-item">
                        Notifications
                    </a>
                </div>
            </div>
            <div class="col-md-9 col-xs-12" id="parentSection">
                <div class="manage-frame loaded active" id="sectionSummary" style="display: block;"></div></div>






<div id="pad-wrapper">
    <input name="__RequestVerificationToken" type="hidden" value="fyUVwe9i_PEhttX_QeVERI33M4jDUgw5nsfQSeHEgV-kX2Nc-CE0StGm_pk9HmdqaISldO5PBZ5oNWpLDfNOBB1D23x0PvQjP4Ji6fVMoGjzZmDMKqKs1wLaLYEz_36LMnAutQ2">
    <div class="container" id="manage-body">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="list-group affix" id="sidebar">
                    <a href="#sectionSummary" class="list-group-item">
                        Summary
                    </a>
                    <hr>
                    <a href="#sectionBasicVerification" class="list-group-item" id="basicVerificationBtn">
                        Basic Verification
                    </a>
                    <a href="#sectionEnhancedVerification" class="list-group-item" id="enhancedVerificationBtn">
                        Enhanced Verification
                    </a>
                    <hr>
                    <a href="#sectionPassword" class="list-group-item">
                        Password
                    </a>
                    <a href="#section2Fa" class="list-group-item">
                        Two-Factor Authentication
                    </a>
                    <a href="#sectionApi" class="list-group-item">
                        API Keys
                    </a>
                    <a href="#sectionIpAddressWhiteList" class="list-group-item active">
                        IP Whitelist
                    </a>
                    <a href="#sectionWithdrawAddressWhiteList" class="list-group-item">
                        Withdrawal Whitelist
                    </a>
                    <a href="#sectionEnableAccount" class="list-group-item">
                        Enable Account
                    </a>
                    <hr>
                    <a href="#sectionUi" class="list-group-item">
                        UI Settings
                    </a>
                    <a href="#sectionNotifications" class="list-group-item">
                        Notifications
                    </a>
                </div>
            </div>
            <div class="col-md-9 col-xs-12" id="parentSection">
                <div class="manage-frame loaded" id="sectionSummary" style="display: none;">



<div id="divManageSummary">
    <div class="col-md-10 load-wrapper" data-bind="visible: isLoading()" style="display: none;">
        <div class="loading">
            <i class="fa fa-spinner fa-pulse fa-5x"></i>
        </div>
    </div>

    <form id="formManageSummary" class="form-horizontal" style="margin-bottom: 24px;" data-bind="visible: !isLoading()">
        <div class="row">
            <div class="col-md-12">
                <h1 class="section-header">Account Information</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-12">
                <h2 class="identity-name">User Name</h2>
            </div>
            <div class="col-lg-6 col-md-12">
                <h2 class="identity-name"><small data-bind="text: email" style="margin-left:0px">mistinolle@gmail.com</small></h2>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-12">
                <h2 class="identity-name">
                    Account Type
                    <span class="label label-info" data-bind="text: accountType" style="padding-top:0px; padding-bottom:0px">NEW</span>
                    <a href="https://bittrex.zendesk.com/hc/en-us/articles/215282838" target="_blank"><span class="fa fa-question-circle"></span></a>
                </h2>
            </div>
            <div class="col-lg-6 col-md-12">
                <div style="margin-top:12px">
                    <div data-bind="template: { name: accountUpgradeTemplate }">
    <button data-bind="click: clickBasic" class="btn btn-success"><span class="fa fa-angle-double-up fa-fw"></span>&nbsp; Upgrade to Basic</button>
    <div class="row" style="margin-top: 10px">
        <div class="col-xs-12">
            New accounts must verify before withdrawing from Bittrex.
        </div>
    </div>
</div>
                </div>
            </div>
        </div>

        <div class="identity-divider"></div>

        <div class="row">
            <div class="col-md-6 col-xs-8">
                <h2 class="identity-attribute"><span class="fa fa-btc fa-fw"></span>&nbsp; Digital Token Trading</h2>
            </div>
            <div class="col-md-6 col-xs-4">
                <div style="margin-top:12px">
                    <div data-bind="template: { name: canCryptoTradeTemplate }">
    <span class="fa fa-square-o fa-2x" style="margin-top:2px"></span>
</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-8">
                <h2 class="identity-attribute"><span class="fa fa-usd fa-fw"></span>&nbsp; Fiat Trading</h2>
            </div>
            <div class="col-md-6 col-xs-4">
                <div style="margin-top:12px">
                    <div data-bind="template: { name: canFiatTradeTemplate }">
    <span class="fa fa-square-o fa-2x" style="margin-top:2px"></span>
</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-8">
                <h2 class="identity-attribute"><span class="fa fa-bank fa-fw"></span>&nbsp; Margin Trading</h2>
            </div>
            <div class="col-md-6 col-xs-4">
                <div style="margin-top:12px">
                    <div data-bind="template: { name: canMarginTradeTemplate }">
    <span class="fa fa-square-o fa-2x" style="margin-top:2px"></span>
</div>
                </div>
            </div>
        </div>

        <div class="identity-divider"></div>

        <div class="row">
            <div class="col-md-6 col-xs-8">
                <h2 class="identity-attribute"><span class="fa fa-cloud-upload fa-fw"></span>&nbsp; Daily Withdrawal Limit</h2>
            </div>
            <div class="col-md-6 col-xs-4">
                <h2 class="section-header" style="margin-bottom:12px; margin-top: 12px; padding-left: 0px; font-weight:500; font-size:18px" data-bind="text: dailyWithdrawalLimit() + ' BTC'">0 BTC</h2>
                <p data-bind="visible: !has2Fa()">To increase your limit on a Basic or Enhanced account, enable Two-Factor authentication <a href="/manage#section2Fa">here</a></p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <h2 class="identity-attribute">User Activity (Last 100 Entries)</h2>
            </div>
            <div class="col-xs-12">
                <div data-bind="if: activities.items().length > 0">
                    <div data-bind="template: { name: 'koTable-template', data: activities }">
    <table class="table table-striped table-hover table-condensed table-bordered">
        <thead>
            <tr data-bind="foreach: columns">
                <th data-bind="css: headerClass" class="col-header">
                    <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                        <span data-bind="html: header">Time Stamp</span>&nbsp;<i data-bind="css: state" class="fa fa-arrow-down"></i>
                    </div>
                </th>
            
                <th data-bind="css: headerClass" class="col-header">
                    <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                        <span data-bind="html: header">Address</span>&nbsp;<i data-bind="css: state"></i>
                    </div>
                </th>
            
                <th data-bind="css: headerClass" class="col-header">
                    <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                        <span data-bind="html: header">User Agent</span>&nbsp;<i data-bind="css: state"></i>
                    </div>
                </th>
            
                <th data-bind="css: headerClass" class="col-header">
                    <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                        <span data-bind="html: header">Activity</span>&nbsp;<i data-bind="css: state"></i>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody data-bind="foreach: { data: visibleItems, as: 'item' }">
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 12:24:14</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 12:24:05</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGOFF</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 11:00:51</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 10:59:53</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 UBrowser/7.0.69.1021 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 10:59:09</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 UBrowser/7.0.69.1021 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">VERIFY_NEW_IP</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 10:51:45</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">UNKNOWN_IP_LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 10:50:59</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">UNKNOWN_IP_LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 10:22:24</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">UNKNOWN_IP_LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">01/30/18 08:10:30</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.45</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36 OPR/50.0.2762.67 (Edition Campaign 34)</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">01/30/18 08:03:46</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.45</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36 OPR/50.0.2762.67 (Edition Campaign 34)</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">VERIFY_NEW_IP</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">01/30/18 08:03:18</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.45</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36 OPR/50.0.2762.67 (Edition Campaign 34)</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">UNKNOWN_IP_LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">01/22/18 14:21:38</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.167.163</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36 OPR/49.0.2725.64 (Edition Campaign 34)</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">01/22/18 14:20:40</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.167.163</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36 OPR/49.0.2725.64 (Edition Campaign 34)</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">VERIFY_NEW_IP</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">01/22/18 14:16:05</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.167.163</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">UNKNOWN_IP_LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">11/08/17 12:21:13</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.168.175</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">11/08/17 12:21:03</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.168.175</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">VERIFY_NEW_IP</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">11/08/17 12:20:14</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.168.175</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">UNKNOWN_IP_LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">10/18/17 10:55:44</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.167.223</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">IMAGE_INITIATE_NETVERIFY</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">10/18/17 10:52:03</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.167.223</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">10/18/17 10:51:52</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.167.223</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">VERIFY_NEW_IP</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        </tbody>

    </table>

    <div class="row">
        <div class="col-xs-12">
            <div style="float:left;margin-left:10px">
                <input type="text" class="form-control" data-bind="value: filter">
            </div>
            <div style="float:right">
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }" class="disabled"><a href="#" data-bind="click: firstPage">First</a></li></ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }" class="disabled"><a href="#" data-bind="click: previousPage">Prev</a></li></ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px" data-bind="foreach: allPages">
                    <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }" class="active"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">1</a></li>
                
                    <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">2</a></li>
                
                    <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">3</a></li>
                
                    <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">4</a></li>
                
                    <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">5</a></li>
                </ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }"><a href="#" data-bind="click: nextPage">Next</a></li></ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }"><a href="#" data-bind="click: lastPage">Last</a></li></ul>
            </div>
        </div>
    </div>
</div>
                </div>
            </div>
        </div>

    </form>

</div>

<script type="text/html" id="new-account-template">
    <button data-bind="click: clickBasic" class="btn btn-success" ><span class="fa fa-angle-double-up fa-fw" ></span>&nbsp; Upgrade to Basic</button>
    <div class="row" style="margin-top: 10px">
        <div class="col-xs-12">
            New accounts must verify before withdrawing from Bittrex.
        </div>
    </div>
</script>

<script type="text/html" id="basic-account-template">
    <button data-bind="click: clickEnhanced" class="btn btn-success" ><span class="fa fa-angle-double-up fa-fw"></span>&nbsp; Upgrade to Enhanced</button>
</script>

<script type="text/html" id="enhanced-account-template">
</script>

<script type="text/html" id="checked-template">
    <span class="fa fa-check-square-o fa-2x" style="margin-top:2px"></span>
</script>

<script type="text/html" id="unchecked-template">
    <span class="fa fa-square-o fa-2x" style="margin-top:2px"></span>
</script>

<!-- Knockout Table -->
<script type="text/html" id="koTable-template">
    <table class="table table-striped table-hover table-condensed table-bordered">
        <thead>
            <tr data-bind="foreach: columns">
                <th data-bind="css: headerClass">
                    <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                        <span data-bind="html: header"></span>&nbsp;<i data-bind="css: state"></i>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody data-bind="foreach: { data: visibleItems, as: 'item' }">
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]"></span>
                    </div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')"></span>
                    </div>
                    <div data-bind="if: type == 'number'">
                        <span data-bind="text: item[column.property].toFixed(8)"></span>
                    </div>
                    <div data-bind="if: type == 'percent'">
                        <span data-bind="text: item[column.property].toFixed(2) + '%'"></span>
                    </div>
                    <div data-bind="if: type == 'template'">
                        <div data-bind="template: { name: column.template, data: item }"></div>
                    </div>
                </td>
            </tr>
        </tbody>

    </table>

    <div class="row">
        <div class="col-xs-12">
            <div style="float:left;margin-left:10px">
                <input type="text" class="form-control" data-bind="value: filter">
            </div>
            <div style="float:right">
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }"><a href="#" data-bind="click: firstPage">First</a></li></ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }"><a href="#" data-bind="click: previousPage">Prev</a></li></ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px" data-bind="foreach: allPages">
                    <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }"></a></li>
                </ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }"><a href="#" data-bind="click: nextPage">Next</a></li></ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }"><a href="#" data-bind="click: lastPage">Last</a></li></ul>
            </div>
        </div>
    </div>
</script>


</div>
                <div class="manage-frame" id="sectionBasicVerification" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame loaded" id="sectionEnhancedVerification" hidden="" style="display: none;">



<form id="formManageEnhanced" class="form-horizontal">
    <div class="panel panel-default col-md-10">
        <div class="panel-body">
            Enhanced Accounts may withdraw greater than 100 BTC or equivalent per day.
        </div>
    </div>
    <div class="row" data-bind="if: verificationState() != null">

        <div class="col-xs-12" data-bind="visible: stateUiVisible">
            <h3><span data-bind="css: verificationState().style" class="fa fa-warning"></span> <span data-bind="text: verificationState().title">Start enhanced verification</span></h3>
        </div>
        <div class="col-xs-12">
            <p data-bind="html: verificationState().message, visible: stateUiVisible ">Please upload your documents and selfie.</p>
        </div>

        <div class="col-xs-12">
            <h2>Tips for successful verification:</h2>
            <ul>
                <li>Take a fresh selfie, ensure nothing is blocking a full view of your face </li>
                <li>Do not use the same image from your identification documents </li>
                <li>Do not use a professional profile picture </li>
                <li>Do not manipulate the image of your identification in anyway. This includes adding watermarks or blacking out certain information </li>
                <li>Do not crop the image of your ID such that any portion of the ID is removed including edges </li>
            </ul>
        </div>

        <div class="col-xs-12" data-bind="visible: verificationState().showButton">
            <button class="btn btn-success" data-bind="click: requestJumio, visible: stateUiVisible">Start Enhanced Verification</button>
        </div>
        <div id="JUMIOIFRAME" data-bind="visible: verificationState().showButton"></div>

    </div>

</form>



</div>
                <div class="manage-frame loaded" id="sectionPassword" hidden="" style="display: none;">



<form action="/Manage/ManagePassword" class="form-horizontal" id="formManagePassword" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="9o1NhJe8pzlzp1Uc5L1LxJ2ydvUQ4fXCKdKT6M2wqSt2tyZ5LlHO5Vi1-d88Y-qHldB-CUkAssWheObCVknm3YPadgTKyuNL837l8eo6Qb8Yoq4cFoU6KyHuBFZMJBTVK9jzXQ2">    <div class="panel panel-default col-md-10">
        <div class="panel-body">
            You're logged in as <strong>mistinolle@gmail.com</strong>.
        </div>
    </div>
    <div class="row">
        <h2 class="col-md-12 section-header">Change Password</h2>
        <p class="text-success"></p>
    </div>
    <div class="row">
        <div class="validation-summary-valid" data-valmsg-summary="true"><ul><li style="display:none"></li>
</ul></div>
    </div>
    <div class="form-group">
        <label for="currentPassword" class="col-md-3 control-label">Current Password</label>
        <div class="col-md-7">
            <input autocomplete="off" class="form-control" data-val="true" data-val-required="The Current password field is required." id="OldPassword" name="OldPassword" placeholder="Current Password" type="password">
        </div>
    </div>
    <div class="form-group">
        <label for="newPassword" class="col-md-3 control-label">New Password</label>
        <div class="col-md-7">
            <input autocomplete="off" class="form-control" data-val="true" data-val-length="The New password must be at least 6 characters long." data-val-length-max="100" data-val-length-min="6" data-val-required="The New password field is required." id="NewPassword" name="NewPassword" placeholder="New Password" type="password">
        </div>
    </div>
    <div class="form-group">
        <label for="confirmPassword" class="col-md-3 control-label">Confirm Password</label>
        <div class="col-md-7">
            <input autocomplete="off" class="form-control" data-val="true" data-val-equalto="The new password and confirmation password do not match." data-val-equalto-other="*.NewPassword" id="ConfirmPassword" name="ConfirmPassword" placeholder="Confirm New Password" type="password">
        </div>
    </div>
        <div class="form-group" data-bind="visible: has2Fa" style="display: none;">
        <label for="authenticatorCode" class="col-md-3 control-label">Authenticator Code</label>
        <div class="col-md-7">
            <input autocomplete="off" class="form-control" id="AuthenticatorCode" name="AuthenticatorCode" placeholder="Input your 6 digit authenticator code" type="text" value="">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-3">
            <button class="btn btn-primary has-spinner" data-bind="css: { active: isSaving }, click: submitInformation">
                <span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>
                Update Password
            </button>
        </div>
    </div>
</form>


<script data-cfasync="false" src="/cdn-cgi/scripts/d07b1474/cloudflare-static/email-decode.min.js"></script></div>
                <div class="manage-frame loaded" id="section2Fa" hidden="" style="display: none;">


<form action="/Manage/Manage2Fa" class="form-horizontal" id="formManage2Fa" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="NYWK_VmM5I2CF6KzTcX4smK6gjftpyulPq9fcEpcl-5-afi7nvK1Guh7nQkY0B_Ck3ovwEGo2h7SP9Z-nj1E4GEi4LwTQ0nugtz_4zTJooTfDjQvN_64J_3gNQq6y7UQcqK1sA2">    <div class="panel panel-default col-md-10">
        <div class="panel-body">
            <h4 style="margin-bottom:5px">Bittrex encourages the use of two-factor authentication</h4>
            Two-factor authentication (2fa) greatly increases security by requiring both your password and another form
            of authentication. Bittrex implements 2fa utilizing <a target="_blank" href="http://en.wikipedia.org/wiki/Google_Authenticator" rel="noreferrer">Google Authenticator</a>.
            To enable this feature simply download Google Authenticator on your mobile device and scan the QRCode.<br>
            <br>
            Once you have linked the Authenticator with Bittrex, enter the 6 digit code provided.
            <br><br>
            <strong>Please back up your secret key. </strong> Resetting your two-factor authentication requires opening a support ticket
            and may take up to 7 days to address.
        </div>
    </div>
    <div class="row">
        <h2 class="col-md-12 section-header">
            Two-Factor Authentication
            <label>
                            <span class="label label-warning">Disabled</span>
            </label>
        </h2>
        <p class="text-success"></p>
    </div>
    <div class="row">
        <div class="validation-summary-valid" data-valmsg-summary="true"><ul><li style="display:none"></li>
</ul></div>
        <input data-val="true" data-val-required="The 2Fa State field is required." id="State2Fa" name="State2Fa" type="hidden" value="Disabled">
    </div>
<input id="QRCodeString" name="QRCodeString" type="hidden" value="otpauth://totp/Bittrex?secret=P7JMMZV4L3OGXCI2"><input id="Secret2FA" name="Secret2FA" type="hidden" value="P7JMMZV4L3OGXCI2">                    <div class="form-group">
                        <label for="Secret2FA" class="col-md-3 control-label" style="font-size: 20px"><span class="fa fa-lock"></span>&nbsp; Secret Key*</label>
                        <div class="col-md-7">
                            <p class="form-control-static" style="font-size: 20px"><strong>P7JMMZV4L3OGXCI2</strong> </p>
                        </div>
                        <div class="col-xs-10">
                            <strong style="color: red">* Please back up your secret key. </strong> Resetting your two-factor authentication requires opening a support ticket
                            and may take up to 7 days to address
                        </div>
                    </div>
                    <img src="/Balance/BarcodeImage?barcodeText=otpauth://totp/Bittrex?secret=P7JMMZV4L3OGXCI2">
        <div class="form-group">
            <label for="authenticatorCode" class="col-md-3 control-label">Authenticator Code</label>
            <div class="col-md-7">
                <input autocomplete="off" class="form-control" data-val="true" data-val-regex="Your authenticator code is a 6 digit number" data-val-regex-pattern="^[0-9]{6}$" id="AuthenticatorCode" name="AuthenticatorCode" placeholder="Input your 6-digit authenticator code" type="text" value="">
            </div>
        </div>
    <div class="form-group">
        <div class="col-md-3">
                    <button class="btn btn-primary has-spinner" data-bind="css: { active: isSaving }, click: submitInformation">
                        <span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>
                        Enable 2FA
                    </button>
        </div>
    </div>
</form>

</div>
                <div class="manage-frame loaded" id="sectionApi" hidden="" style="display: none;">


    <div class="panel panel-default col-md-10">
        <div class="panel-body">
            <h6>To use API Keys, you must have Two-Factor Authentication enabled.</h6>
            <a href="#section2Fa" class="forgot">Two-Factor Authentication settings</a>
        </div>
    </div>
    <script>
        var loadSection2Fa = function loadSection2Fa() {
            if (!$('#section2Fa').hasClass('loaded')) {
                $('#section2Fa').addClass('loaded');
                $('#section2Fa').load("/Manage/Manage2FA", function () {
                    self.manage2Fa = new manage2Fa();
                    if (document.getElementById('formManage2Fa') != null) {
                        ko.applyBindings(self.manage2Fa, document.getElementById('formManage2Fa'));
                    }
                });
            }
        }

        $('.forgot').click(function () {

            var allHeaders = $('.list-group-item');
            var allFrames = $('.manage-frame');
            var href = $(this).attr('href');
            var frame = $(href);

            if (!frame.hasClass('active')) {
                allHeaders.removeClass('active');
                $(this).addClass('active');

                allFrames.removeClass('active').hide();
                frame.addClass('active').show();

                switch (href) {
                    case '#section2Fa':
                        loadSection2Fa();
                        break;
                }
            }
        });

    </script>
</div>
                <div class="manage-frame" id="sectionUi" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionNotifications" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame active loaded" id="sectionIpAddressWhiteList" hidden="" style="display: block;">



<div id="divManageIpAddress">
    <div class="col-md-10 load-wrapper" data-bind="visible: isLoading()" style="display: none;">
        <div class="loading">
            <i class="fa fa-spinner fa-pulse fa-5x"></i>
        </div>
    </div>

    <div class="row" style="margin-bottom: 24px;" data-bind="visible: !isLoading()">
        <div class="col-md-12">
            <h1 class="section-header">IP Addresses</h1>
        </div>
    </div>

    <div class="panel panel-warning">
        <div class="panel-heading">
            <span class="fa fa-warning">&nbsp;IP Address Whitelisting</span>
        </div>
        <div class="panel-body">
            <p class="text-center">
                By adding one or more IP address to this list, you will only be able to place orders or withdraw funds from those addresses.
                To add or remove an IP address, you must have two-factor authentication enabled and provide your authenticator code before submitting
                any changes. 
            </p>
        </div>
    </div>

    <form class="form-horizontal" data-bind="visible: !isLoading()" style="">
        <div class="form-group">
            <label for="newAddress" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-2">
                <input type="number" class="form-control" id="byte1" placeholder="000" data-bind="value: byte1">
            </div>
            <div class="col-sm-2">
                <input type="number" class="form-control" id="byte2" placeholder="000" data-bind="value: byte2">
            </div>
            <div class="col-sm-2">
                <input type="number" class="form-control" id="byte3" placeholder="000" data-bind="value: byte3">
            </div>
            <div class="col-sm-2">
                <input type="number" class="form-control" id="byte4" placeholder="000" data-bind="value: byte4">
            </div>
        </div>
    </form>

    <form class="form-horizontal" data-bind="visible: !isLoading()" style="">
        <div class="form-group">
            <label for="newAuthenticator" class="col-sm-2 control-label">Authenticator</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="newAuthenticator" placeholder="012345" data-bind="value: authenticatorCode">
            </div>
            <button class="btn btn-primary" data-bind="click: createIpAddress">Set Address</button>
        </div>
    </form>

    <div class="dataTables_scrollBody" style="overflow: hidden; width: 100%; margin-top: 5px; margin-bottom: 5px;" data-bind="visible: !isLoading()">
        <table class="table table-striped table-hover table-condensed" id="ipAddressTable">
            <thead>
                <tr>
                    <th>Address</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody data-bind="foreach: ipAddressWhiteList"></tbody>
        </table>
    </div>

</div>



</div>
                <div class="manage-frame" id="sectionWithdrawAddressWhiteList" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionEnableAccount" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>










        









      <!-- END Navigation FOOTER --> 

        <div class="hidden-lg" style="margin-bottom: 47px"></div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" aria-labelledby="resfreshModalLabel" aria-hidden="true" id="refreshModal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="resfreshModalLabel">
                        Bittrex has updated, please refresh your browser
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="window.location.reload(true); return false;"><span class="glyphicon glyphicon-refresh"></span>&nbsp;Refresh</button>
                </div>
            </div>
        </div>
    </div>


<footer id="footer-navigation" class="navbar navbar-inverse navbar-fixed-bottom">
        <div class="container-fluid container-footer">
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a>
                            <span>© 2018 Bittrex, INC</span>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a>
                            <span data-bind="text:menu.displayTotalBtc">Total BTC Volume = 41013.2925</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <span data-bind="text:menu.displayTotalEth">Total ETH Volume = 18819.5684</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <span data-bind="text:navigation.displayBitcoinUsd">1 BTC = $10990.4388</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <i class="fa fa-wifi socket-status-warning" data-bind="css: { 'socket-status-normal': socket.connectionStatus() == 'Connected', 'socket-status-warning': socket.connectionStatus() == 'Slow' || socket.connectionStatus() == 'Reconnecting', 'socket-status-error': socket.connectionStatus() == 'Disconnected' }"></i>
                            <span data-bind="text:socket.displayStatus">Socket Status = Slow</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
</body>
</html>