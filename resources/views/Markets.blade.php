<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />


    <!-- ****** faviconit.com favicons ****** -->
    <link rel="shortcut icon" href="/Content/img/logos/bittrex.ico">
    <link rel="icon" sizes="16x16 32x32 64x64" href="/Content/img/logos/bittrex.ico">
    <link rel="icon" type="image/png" sizes="196x196" href="/Content/img/logos/bittrex-192.png">
    <link rel="icon" type="image/png" sizes="160x160" href="/Content/img/logos/bittrex-160.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/Content/img/logos/bittrex-96.png">
    <link rel="icon" type="image/png" sizes="64x64" href="/Content/img/logos/bittrex-64.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/Content/img/logos/bittrex-32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/Content/img/logos/bittrex-16.png">
    <link rel="apple-touch-icon" href="/Content/img/logos/bittrex-57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/Content/img/logos/bittrex-114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/Content/img/logos/bittrex-72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/Content/img/logos/bittrex-144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/Content/img/logos/bittrex-60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/Content/img/logos/bittrex-120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/Content/img/logos/bittrex-76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/Content/img/logos/bittrex-152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/Content/img/logos/bittrex-180.png">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="/Content/img/logos/bittrex-144.png">
    <meta name="msapplication-config" content="/Content/img/logos/browserconfig.xml">
    <!-- ****** faviconit.com favicons ****** -->


	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/style.css') }} ">

	<!-- One Page Module Specific Stylesheet -->
	<link rel="stylesheet" href="{{ asset('assets/css/onepage.css') }}">
	<!-- / -->

	<link rel="stylesheet" href="{{ asset('assets/css/dark.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/font-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/et-line.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">

	<link rel="stylesheet" href="{{ asset('assets/css/fonts.css') }}">

	<link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
<title> Bittrex.com - The Next Generation Crypto-Currency Exchange</title>


</head>

<body class="stretched side-push-panel">

	<div class="body-overlay"></div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
					<!-- #primary-menu end -->

				</div>

			</div>

		</header>
		<!-- #header end -->
		
		<div id="header-navigation" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid container-header">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="Markets">
               <img src="{{ asset('assets/images/bittrex/bittrex-logo.png')}}" alt='Bittrex.com' height="24" />
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li class="dropdown" id="dash_menu_btc2">
                        <a href="#dash_menu_btc" class="dropdown-toggle" data-toggle="collapse">
                            <i class="fa fa-btc"></i>
                            <span>Markets</span>
                            <b class="caret"></b>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-left">
                    <li class="dropdown" id="dash_menu_fiat2">
                        <a href="#dash_menu_fiat" class="dropdown-toggle" data-toggle="collapse">
                            <i class="fa fa-usd"></i>
                            <span>Markets</span>
                            <b class="caret"></b>
                        </a>
                    </li>
                </ul>

                    <ul class="nav navbar-nav navbar-right">
        <li >
            <a href="New-Account">
                <i class="fa fa-users"></i>
                <span >&nbsp; Register</span>
            </a>
        </li>
        <li >
            <a href="login">
                <i class="fa fa-sign-in"></i>
                <span >&nbsp; Login</span>
            </a>
        </li>
    </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-question-circle"></i><span class="hidden-sm">&nbsp; Help</span>  <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="dash_menu_news">
                                <a href="/News">
                                    <i class="fa fa-bullhorn"></i>
                                    <span>&nbsp; News</span>
                                </a>
                            </li>
                            <li id="dash_menu_status">
                                <a href="/Status">
                                    <i class="fa fa-lightbulb-o"></i>
                                    <span>&nbsp; Site Status</span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="/Home/About">About</a></li>
                            <li><a href="/Privacy">Your Privacy and Security</a></li>
                            <li><a href="/Home/Cookies">Cookie Policy</a></li>
                            <li><a href="/Home/Terms">Terms and Conditions</a></li>
                            <li><a href="/Home/Api">API Documentation</a></li>
                            <li><a href="/Fees">Fees</a></li>
                            <li><a href="https://bittrex.zendesk.com/hc/en-us" rel="noreferrer">Support</a></li>
                            <li><a href="/Home/Contact">Contact Us</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>

		<!-- Content
		============================================= -->
	<section id="content">
	<div id="home-wrapper">
    <div class="container carousel">
        
        <div class="hidden-xs">
            <div class="connected-carousels">
    <div class="stage">
    <div class="carousel carousel-stage">
      
 <!-- START THE FEATURETTES -->
<div class="row featurette">
            <div class="col-md-12" data-bind="with: btcTable">
                
<div class="row" style="margin-bottom:5px">
    <div class="col-xs-6">
        <h2 class="featurette-heading top-header" style="padding: 0px">
            <span class="hidden-xs" data-bind="text: title">Bitcoin</span><span style="float: left; padding-top:4px"><i data-bind="css: icon" class="visible-xs cc BTC"></i></span> <span>Markets</span>
        </h2>
    </div>
    <div class="col-xs-6">
        <form class="form-inline" style="float:right">
            <div class="form-group">
                <input type="text" class="form-control" data-bind="textInput: filter">
            </div>
        </form>
    </div>
</div>


<table class="table table-striped table-hover table-condensed table-bordered" style="table-layout: fixed; margin-left: 0px; width: 100%;">
    <thead>
        <tr data-bind="foreach: columns">
            <th data-bind="css: headerClass" class="col-header">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Market</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Currency</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Volume</span>&nbsp;<i data-bind="css: state" class="fa fa-arrow-down"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">% Change</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Last Price</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg visible-md">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">24Hr High</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg visible-md">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">24Hr Low</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">% Spread</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Added</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        </tr>
    </thead>
    <tbody data-bind="foreach: visibleSummaries">
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-RDD" title="ReddCoin">
                    <span data-bind="text: marketName">BTC-RDD</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">ReddCoin</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">5198.816</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">14.6%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00000094</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00000109</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00000074</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.1%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">02/25/2014</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-DOGE" title="Dogecoin">
                    <span data-bind="text: marketName">BTC-DOGE</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Dogecoin</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">2558.526</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">1.6%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00000064</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00000074</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00000060</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.5%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">02/13/2014</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-ETC" title="Ethereum Classic">
                    <span data-bind="text: marketName">BTC-ETC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Ethereum Classic</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">2030.470</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-7.9%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00331500</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00361890</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00316400</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.2%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/26/2016</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-ETH" title="Ethereum">
                    <span data-bind="text: marketName">BTC-ETH</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Ethereum</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1986.635</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-2.2%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.08049511</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.08234613</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.07713042</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.5%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">08/14/2015</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-LSK" title="Lisk">
                    <span data-bind="text: marketName">BTC-LSK</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Lisk</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1897.065</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-9.9%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00222440</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00272679</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00216942</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.1%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">05/24/2016</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-SC" title="Siacoin">
                    <span data-bind="text: marketName">BTC-SC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Siacoin</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1875.495</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">6.7%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00000256</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00000268</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00000216</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.4%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">05/22/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-ZCL" title="Zclassic">
                    <span data-bind="text: marketName">BTC-ZCL</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Zclassic</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1712.979</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-10.5%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.01426030</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.01649200</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.01320000</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.3%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">11/15/2016</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-ADA" title="Ada">
                    <span data-bind="text: marketName">BTC-ADA</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Ada</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1618.781</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-5.1%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00003091</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00003299</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00003033</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.2%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">09/29/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-XVG" title="Verge">
                    <span data-bind="text: marketName">BTC-XVG</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Verge</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1495.323</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-9.7%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00000632</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00000721</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00000608</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.2%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">02/17/2016</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-XRP" title="Ripple">
                    <span data-bind="text: marketName">BTC-XRP</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Ripple</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1400.746</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-4.6%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00009197</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00009652</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00008890</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.2%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">12/22/2014</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-LTC" title="Litecoin">
                    <span data-bind="text: marketName">BTC-LTC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Litecoin</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1367.061</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-6.1%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.01990201</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.02167032</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.01980000</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.5%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">02/13/2014</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-NBT" title="Nubits">
                    <span data-bind="text: marketName">BTC-NBT</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Nubits</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1188.828</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-0.4%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00008889</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00009178</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00008345</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.4%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">04/08/2015</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-XEM" title="NewEconomyMovement">
                    <span data-bind="text: marketName">BTC-XEM</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">NewEconomyMovement</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1071.343</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-2.2%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00004275</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00004491</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00003865</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.4%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">04/10/2015</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-XLM" title="Lumen">
                    <span data-bind="text: marketName">BTC-XLM</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Lumen</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1067.166</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-5.5%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00003559</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00003769</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00003334</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.3%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">11/17/2015</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-OMG" title="OmiseGO">
                    <span data-bind="text: marketName">BTC-OMG</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">OmiseGO</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">954.319</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">9.5%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00169417</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00171696</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00154654</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.3%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/16/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-NEO" title="Neo">
                    <span data-bind="text: marketName">BTC-NEO</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Neo</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">938.546</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-4.1%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.01137367</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.01191997</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.01090000</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.6%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">10/26/2016</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-BCC" title="Bitcoin Cash">
                    <span data-bind="text: marketName">BTC-BCC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Bitcoin Cash</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">792.932</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-5.8%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.12437500</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.13266999</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.12136340</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.1%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">08/01/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-BITB" title="BitBean">
                    <span data-bind="text: marketName">BTC-BITB</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">BitBean</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">752.112</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">3.7%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00000167</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00000191</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00000140</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.6%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">02/16/2015</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-SRN" title="Sirin Token">
                    <span data-bind="text: marketName">BTC-SRN</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Sirin Token</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">578.600</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">23.1%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00008371</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00010959</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00006262</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.1%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">02/13/2018</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=BTC-WAVES" title="Waves">
                    <span data-bind="text: marketName">BTC-WAVES</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Waves</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">573.453</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">2.5%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00072498</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00075994</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00067277</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.2%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">06/19/2016</td>
        </tr>
    </tbody>

</table>

<div class="row">
    <div class="col-xs-12">
        <div style="float:right">
            <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }" class="disabled"><a href="#" data-bind="click: firstPage">First</a></li></ul>
            <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }" class="disabled"><a href="#" data-bind="click: previousPage">Prev</a></li></ul>
            <ul class="pagination pagination-sm" style="margin: 5px 0px" data-bind="foreach: allPages">
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }" class="active"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">1</a></li>
            
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">2</a></li>
            
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">3</a></li>
            
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">4</a></li>
            
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">5</a></li>
            </ul>
            <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }"><a href="#" data-bind="click: nextPage">Next</a></li></ul>
            <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }"><a href="#" data-bind="click: lastPage">Last</a></li></ul>
        </div>
    </div>
</div>

            </div>

            <div class="col-md-12" data-bind="with: ethTable">
                
<div class="row" style="margin-bottom:5px">
    <div class="col-xs-6">
        <h2 class="featurette-heading top-header" style="padding: 0px">
            <span class="hidden-xs" data-bind="text: title">Ethereum</span><span style="float: left; padding-top:4px"><i data-bind="css: icon" class="visible-xs cc ETH"></i></span> <span>Markets</span>
        </h2>
    </div>
    <div class="col-xs-6">
        <form class="form-inline" style="float:right">
            <div class="form-group">
                <input type="text" class="form-control" data-bind="textInput: filter">
            </div>
        </form>
    </div>
</div>


<table class="table table-striped table-hover table-condensed table-bordered" style="table-layout: fixed; margin-left: 0px; width: 100%;">
    <thead>
        <tr data-bind="foreach: columns">
            <th data-bind="css: headerClass" class="col-header">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Market</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Currency</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Volume</span>&nbsp;<i data-bind="css: state" class="fa fa-arrow-down"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">% Change</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Last Price</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg visible-md">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">24Hr High</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg visible-md">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">24Hr Low</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">% Spread</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Added</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        </tr>
    </thead>
    <tbody data-bind="foreach: visibleSummaries">
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-OMG" title="OmiseGO">
                    <span data-bind="text: marketName">ETH-OMG</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">OmiseGO</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">2860.125</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">12.3%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.02127964</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.02190000</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.01889392</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.2%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/16/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-SRN" title="Sirin Token">
                    <span data-bind="text: marketName">ETH-SRN</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Sirin Token</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">2178.692</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">34.1%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00114000</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00135000</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00077592</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">10.6%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">02/13/2018</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-ETC" title="Ethereum Classic">
                    <span data-bind="text: marketName">ETH-ETC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Ethereum Classic</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1857.502</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-3.8%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.04156000</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.04399641</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.04016000</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.5%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/28/2016</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-LTC" title="Litecoin">
                    <span data-bind="text: marketName">ETH-LTC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Litecoin</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1400.455</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-3.2%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.24880000</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.27256410</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.24832792</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.5%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">06/25/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-NEO" title="Neo">
                    <span data-bind="text: marketName">ETH-NEO</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Neo</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1359.719</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-2.0%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.14162498</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.14567890</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.13845416</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.6%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/21/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-XRP" title="Ripple">
                    <span data-bind="text: marketName">ETH-XRP</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Ripple</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1183.306</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-2.4%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00114007</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00117614</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00112656</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.6%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">06/25/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-SC" title="Siacoin">
                    <span data-bind="text: marketName">ETH-SC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Siacoin</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1182.090</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">8.8%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00003186</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00003330</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00002784</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.5%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/14/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-ADA" title="Ada">
                    <span data-bind="text: marketName">ETH-ADA</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Ada</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">892.681</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-2.6%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00038600</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00040251</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00037900</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.8%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">11/28/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-XLM" title="Lumen">
                    <span data-bind="text: marketName">ETH-XLM</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Lumen</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">766.233</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-2.7%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00044483</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00045995</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00042400</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.8%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/21/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-BCC" title="Bitcoin Cash">
                    <span data-bind="text: marketName">ETH-BCC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Bitcoin Cash</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">561.468</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-2.0%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">1.55987513</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">1.64799219</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">1.52987751</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.1%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">08/01/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-XEM" title="NewEconomyMovement">
                    <span data-bind="text: marketName">ETH-XEM</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">NewEconomyMovement</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">502.253</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">1.7%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00053388</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00055300</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00048511</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.8%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/21/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-VEE" title="BLOCKv">
                    <span data-bind="text: marketName">ETH-VEE</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">BLOCKv</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">339.603</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-8.0%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00008923</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00010800</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00008501</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">2.7%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">02/16/2018</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-XMR" title="Monero">
                    <span data-bind="text: marketName">ETH-XMR</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Monero</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">269.364</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">10.2%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.36308357</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.36355146</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.32242516</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.9%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/21/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-ZEC" title="ZCash">
                    <span data-bind="text: marketName">ETH-ZEC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">ZCash</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">267.399</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-0.3%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.49364331</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.50765600</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.47091654</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.2%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/14/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-WAX" title="Worldwide Asset Exchange">
                    <span data-bind="text: marketName">ETH-WAX</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Worldwide Asset Exchange</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">250.077</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">0.0%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00040002</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00043500</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00036991</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">2.9%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">02/15/2018</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-DASH" title="Dash">
                    <span data-bind="text: marketName">ETH-DASH</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Dash</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">232.877</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">0.3%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.78159983</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.80999999</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.75462487</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.8%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/14/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-CVC" title="Civic">
                    <span data-bind="text: marketName">ETH-CVC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Civic</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">220.508</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-3.0%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00046100</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00053738</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00044000</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.3%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/17/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-WAVES" title="Waves">
                    <span data-bind="text: marketName">ETH-WAVES</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Waves</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">213.530</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">6.2%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00903278</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.00950000</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00840253</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">2.4%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">08/04/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-QTUM" title="Qtum">
                    <span data-bind="text: marketName">ETH-QTUM</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Qtum</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">197.219</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-1.5%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.03222103</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.03314190</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.03116903</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.2%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/20/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=ETH-STRAT" title="Stratis">
                    <span data-bind="text: marketName">ETH-STRAT</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Stratis</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">190.520</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-8.7%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.00955019</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.01046068</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.00926697</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">2.3%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">08/04/2017</td>
        </tr>
    </tbody>

</table>

<div class="row">
    <div class="col-xs-12">
        <div style="float:right">
            <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }" class="disabled"><a href="#" data-bind="click: firstPage">First</a></li></ul>
            <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }" class="disabled"><a href="#" data-bind="click: previousPage">Prev</a></li></ul>
            <ul class="pagination pagination-sm" style="margin: 5px 0px" data-bind="foreach: allPages">
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }" class="active"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">1</a></li>
            
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">2</a></li>
            
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">3</a></li>
            
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">4</a></li>
            
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">5</a></li>
            </ul>
            <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }"><a href="#" data-bind="click: nextPage">Next</a></li></ul>
            <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }"><a href="#" data-bind="click: lastPage">Last</a></li></ul>
        </div>
    </div>
</div>

            </div>

            <div class="col-md-12" data-bind="with: fiatTable">
                
<div class="row" style="margin-bottom:5px">
    <div class="col-xs-6">
        <h2 class="featurette-heading top-header" style="padding: 0px">
            <span class="hidden-xs" data-bind="text: title">USDT</span><span style="float: left; padding-top:4px"><i data-bind="css: icon" class="visible-xs cc USDT"></i></span> <span>Markets</span>
        </h2>
    </div>
    <div class="col-xs-6">
        <form class="form-inline" style="float:right">
            <div class="form-group">
                <input type="text" class="form-control" data-bind="textInput: filter">
            </div>
        </form>
    </div>
</div>


<table class="table table-striped table-hover table-condensed table-bordered" style="table-layout: fixed; margin-left: 0px; width: 100%;">
    <thead>
        <tr data-bind="foreach: columns">
            <th data-bind="css: headerClass" class="col-header">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Market</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Currency</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Volume</span>&nbsp;<i data-bind="css: state" class="fa fa-arrow-down"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">% Change</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Last Price</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg visible-md">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">24Hr High</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg visible-md">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">24Hr Low</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">% Spread</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        
            <th data-bind="css: headerClass" class="col-header visible-lg">
                <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                    <span data-bind="html: header">Added</span>&nbsp;<i data-bind="css: state"></i>
                </div>
            </th>
        </tr>
    </thead>
    <tbody data-bind="foreach: visibleSummaries">
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-BTC" title="Bitcoin">
                    <span data-bind="text: marketName">USDT-BTC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Bitcoin</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">61789869.011</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-2.7%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">11014.00</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">11768.46</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">10671.00</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.0%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">12/11/2015</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-LTC" title="Litecoin">
                    <span data-bind="text: marketName">USDT-LTC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Litecoin</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">9214437.651</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-8.2%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">220.20</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">249.40</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">209.92</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.5%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/14/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-ETC" title="Ethereum Classic">
                    <span data-bind="text: marketName">USDT-ETC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Ethereum Classic</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">7477545.833</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-10.0%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">36.54</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">41.46</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">34.59</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.9%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/14/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-XRP" title="Ripple">
                    <span data-bind="text: marketName">USDT-XRP</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Ripple</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">7152649.958</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-7.5%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">1.01</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">1.11</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.98</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.2%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/14/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-ETH" title="Ethereum">
                    <span data-bind="text: marketName">USDT-ETH</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Ethereum</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">6742035.204</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-5.4%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">883.87</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">947.00</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">856.00</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.2%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">04/20/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-BCC" title="Bitcoin Cash">
                    <span data-bind="text: marketName">USDT-BCC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Bitcoin Cash</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">4396634.534</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-6.9%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">1387.00</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">1523.00</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">1318.00</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.0%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">08/01/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-NEO" title="Neo">
                    <span data-bind="text: marketName">USDT-NEO</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Neo</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">3837378.522</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-6.9%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">124.57</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">137.58</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">118.38</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.3%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">08/04/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-XVG" title="Verge">
                    <span data-bind="text: marketName">USDT-XVG</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Verge</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">2648625.042</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-10.7%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.07</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.08</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.07</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.3%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">12/29/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-OMG" title="OmiseGo">
                    <span data-bind="text: marketName">USDT-OMG</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">OmiseGo</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">2619501.126</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">5.3%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">18.56</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">19.82</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">17.00</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.0%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">09/16/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-ADA" title="Ada">
                    <span data-bind="text: marketName">USDT-ADA</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Ada</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">2602784.722</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-7.5%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.34</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.38</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.33</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.0%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">12/29/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-ZEC" title="ZCash">
                    <span data-bind="text: marketName">USDT-ZEC</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">ZCash</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">1283336.315</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-6.6%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">430.91</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">467.40</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">410.00</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.2%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/14/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-XMR" title="Monero">
                    <span data-bind="text: marketName">USDT-XMR</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Monero</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">985934.518</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-up">3.4%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">320.00</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">320.57</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">279.50</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">0.2%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/21/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-BTG" title="Bitcoin Gold">
                    <span data-bind="text: marketName">USDT-BTG</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Bitcoin Gold</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">772074.288</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-4.6%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">127.70</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">137.80</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">120.50</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.3%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">11/21/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-DASH" title="Dash">
                    <span data-bind="text: marketName">USDT-DASH</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">Dash</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">424690.255</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-3.9%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">694.50</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">735.00</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">660.98</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.4%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">07/21/2017</td>
        </tr>
    
        <tr>
            <td class="name">
                <a data-bind="attr: { href: '/Market/Index?MarketName=' + marketName, title: marketCurrencyName }" href="/Market/Index?MarketName=USDT-NXT" title="NXT">
                    <span data-bind="text: marketName">USDT-NXT</span>
                </a>
            </td>
            <td data-bind="text: marketCurrencyName" class="name visible-lg">NXT</td>
            <td data-bind="text: baseVolume().toFixed(3)" class="number">404237.245</td>
            <td class="number">
                <span data-bind="text: displayChange(), css: { 'dyn-td-up': change() > 0, 'dyn-td-down': change() < 0, 'dyn-td-none': change() == 0 }" class="dyn-td-down">-9.6%</span>
            </td>
            <td data-bind="text: displayLast()" class="number">0.23</td>
            <td data-bind="text: displayHigh()" class="number visible-lg visible-md">0.26</td>
            <td data-bind="text: displayLow()" class="number visible-lg visible-md">0.21</td>
            <td data-bind="text: displaySpread()" class="number visible-lg">1.6%</td>
            <td data-bind="text: created().format('MM/DD/YYYY')" class="date visible-lg">12/29/2017</td>
        </tr>
    </tbody>

</table>

<div class="row">
    <div class="col-xs-12">
        <div style="float:right">
            <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }" class="disabled"><a href="#" data-bind="click: firstPage">First</a></li></ul>
            <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }" class="disabled"><a href="#" data-bind="click: previousPage">Prev</a></li></ul>
            <ul class="pagination pagination-sm" style="margin: 5px 0px" data-bind="foreach: allPages">
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }" class="active"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">1</a></li>
            
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">2</a></li>
            
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">3</a></li>
            
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">4</a></li>
            
                <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">5</a></li>
            </ul>
            <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }" class="disabled"><a href="#" data-bind="click: nextPage">Next</a></li></ul>
            <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }" class="disabled"><a href="#" data-bind="click: lastPage">Last</a></li></ul>
        </div>
    </div>
</div>

            </div>

        </div>
 <!-- /END THE FEATURETTES -->
    </div>

</div>


        </div>

        <!-- START Navigation FOOTER -->
        <!--
        <div id="footmainbkg" style="width:100%; height: 78px; background-color: #999; margin-top:40px; margin-bottom: 47px;">
            <div class="footmain" style="width:1140px; margin: 0 auto; background-color: #999;">
                <div class="footermlinkl">
                    <li><a href="https://bittrex.com/Home/About" style="text-decoration: underline;">About</a></li>
                    <li><a href="https://bittrex.com/Privacy" style="text-decoration: underline;">Privacy</a></li>
                </div>
                <div class="footermlinkl">
                    <li><a href="https://bittrex.com/Status" style="text-decoration: underline;">Website Status</a></li>
                    <li><a href="https://bittrex.com/Home/Api" style="text-decoration: underline;">API Documentation</a></li>
                </div>
                <div class="footermlinkl">
                    <li><a href="https://bittrex.com/Home/Cookies" style="text-decoration: underline;">Cookies</a></li>
                    <li><a href="https://bittrex.com/Home/Terms" style="text-decoration: underline;">Terms</a></li>
                </div>
                <div class="footermlinkl">
                    <li><a href="https://bittrex.com/Fees" style="text-decoration: underline;">Fees</a></li>
                    <li><a href="https://bittrex.com/Home/Contact" style="text-decoration: underline;">Press</a></li>
                </div>

                <div class="footermlinkr" style="float: right;">
                    <li style="float: right;"><a href="https://twitter.com/bittrexexchange" style="text-decoration: underline;"><i class="fa fa-twitter" style="padding-right: 6px;"></i>Follow Us on Twitter</a></li>
                    <li style="float: right;"><a href="https://www.facebook.com/bittrex" style="text-decoration: underline;"><i class="fa fa-facebook" style="padding-right: 6px;"></i>Like Us on Facebook</a></li>
                </div>
                <div class="footermlinkr" style="float: right;">
                    <li ><a href="https://bittrex.zendesk.com/hc/en-us" rel="noreferrer" style="text-decoration: underline;"><div>Support</div></a></li>
                    <li ><a href="https://bittrex.com/Home/Contact" style="text-decoration: underline;"><div>Contact</div></a></li>
                </div>

            </div>
        </div>
    -->
        <!-- END Navigation FOOTER --> 

        <div class="hidden-lg" style="margin-bottom: 47px"></div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" aria-labelledby="resfreshModalLabel" aria-hidden="true" id="refreshModal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="resfreshModalLabel">
                        Bittrex has updated, please refresh your browser
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="window.location.reload(true); return false;"><span class="glyphicon glyphicon-refresh"></span>&nbsp;Refresh</button>
                </div>
            </div>
        </div>
    </div>


    <footer id="footer-navigation" class="navbar navbar-inverse navbar-fixed-bottom">
        <div class="container-fluid container-footer">
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a>
                            <span>&copy; 2018 Bittrex, INC</span>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a>
                            <span data-bind="text:menu.displayTotalBtc"></span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <span data-bind="text:menu.displayTotalEth"></span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <span data-bind="text:navigation.displayBitcoinUsd"></span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <i class="fa fa-wifi" data-bind="css: { 'socket-status-normal': socket.connectionStatus() == 'Connected', 'socket-status-warning': socket.connectionStatus() == 'Slow' || socket.connectionStatus() == 'Reconnecting', 'socket-status-error': socket.connectionStatus() == 'Disconnected' }"></i>
                            <span data-bind="text:socket.displayStatus"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>

    <script>
        var bittrex = bittrex || {};
    </script>


    <!-- Core Packages  -->
    <script src="/bundles/tpcore?v=mFMGI5b0zWcBIxRIcWepn_4CsIjZ9sHStdwUn338-Vw1"></script>

    <script src="/bundles/datatables?v=_nmR9l62IHo9mIP5ztLVEfKRGl1vQJnAPVqfG-k1Anc1"></script>


    <!-- Utility Packages -->
    <script src="/bundles/utility?v=DM2widmV938pSRg0UCBUnqTbqenw3dHQpif1Ncy7J5c1"></script>


    <!-- This is created dynamically and can't be bundled -->
    

    <!-- Layout DataProvider Packages -->
    <!-- TODO: Need to add the legacy fallback code -->
    <script src="/bundles/bittrexDataProvider?v=W4wUs1toJUNMzVPticPXD8xcafgDNRMz_6TNVCDiUa81"></script>

    <script src="/bundles/layoutDataProvider?v=aJ_Nt8ssNsUZF3F_NLS-rVnFAg7jUefv1E3SN-OWW1o1"></script>


    <!-- Page DataProvider Packages -->
    
    <script>
        var bittrex = bittrex || {};
        //bittrex.fetchSummary = true;
    </script>



    <!-- Layout View Packages -->
    <script src="/bundles/bittrexViewModel?v=6jppaCnvNr3ReJqemqWyfuX5a6vLxegOU3Bdo4x4Tv81"></script>

    <script src="/bundles/layoutViewModel?v=lI4iUNgUH4UJYsCqK74f5hq9ZR7lT9RTzIuJkn3Yg3s1"></script>


    <!-- Page View Packages -->
    
    <!-- TODO: Bundle/Minify this -->
    <script src="/Scripts/bittrex2/vendor/jquery.jcarousel.min.js"></script>
    <script src="/bundles/amcharts?v=K0Hd1nD9Q3MNnDw7uK_yta_Z_7qFcwZI8ZifuOIj0t01"></script>

    <script src="/bundles/homeViewModel2?v=7AmCS2NmOx6N1GjQDZnmL2Z6edyesCa2EX8jh5AalJo1"></script>


			

		</section><!-- #content end -->

		

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<!-- <div id="gotoTop" class="icon-angle-up"></div> -->

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/plugins.js"></script>

	<!-- Google Map JavaScripts
	============================================= -->
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=YOUR_API_KEY"></script>
	<script type="text/javascript" src="js/jquery.gmap.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>

	<script type="text/javascript">

		jQuery(window).load(function(){

			// Google Map
			jQuery('#headquarters-map').gMap({
				address: 'Melbourne, Australia',
				maptype: 'ROADMAP',
				zoom: 14,
				markers: [
					{
						address: "Melbourne, Australia",
						html: "Melbourne, Australia",
						icon: {
							image: "images/icons/map-icon-red.png",
							iconsize: [32, 32],
							iconanchor: [14,44]
						}
					}
				],
				doubleclickzoom: false,
				controls: {
					panControl: false,
					zoomControl: false,
					mapTypeControl: false,
					scaleControl: false,
					streetViewControl: false,
					overviewMapControl: false
				},
				styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"labels.text","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":"-100"},{"lightness":"30"}]},{"featureType":"administrative.neighborhood","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"simplified"},{"gamma":"0.00"},{"lightness":"74"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"lightness":"3"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
			});

		});

	</script>

</body>
</html>