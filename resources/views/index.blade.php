<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/style.css') }} ">

	<!-- One Page Module Specific Stylesheet -->
	<link rel="stylesheet" href="{{ asset('assets/css/onepage.css') }}">
	<!-- / -->

	<link rel="stylesheet" href="{{ asset('assets/css/dark.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/font-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/et-line.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">

	<link rel="stylesheet" href="{{ asset('assets/css/fonts.css') }}">

	<link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!-- Document Title
	============================================= -->
	<title>Bittrex.com - Bittrex, The Next Generation Generation Digital Currency Exchange</title>
</head>
<body class="stretched side-push-panel">
	<div class="body-overlay"></div>

	<div id="side-panel" class="dark">

		<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>

		<div class="side-panel-wrap">

			<div class="widget widget_links clearfix">

				<h4>About Canvas</h4>

				<div style="font-size: 14px; line-height: 1.7;">
					<address style="line-height: 1.7;">
						795 Folsom Ave, Suite 600<br>
						San Francisco, CA 94107<br>
					</address>

					<div class="clear topmargin-sm"></div>

					<abbr title="Phone Number">Phone:</abbr> (91) 8547 632521<br>
					<abbr title="Fax">Fax:</abbr> (91) 11 4752 1433<br>
					<abbr title="Email Address">Email:</abbr> info@canvas.com
				</div>

			</div>

		</div>

	</div>
		<!-- Document Wrapper
	============================================= -->


		<!-- Header
		============================================= -->
		<header id="header" class="full-header transparent-header border-full-header dark static-sticky" data-sticky-class="not-dark" data-sticky-offset="full" data-sticky-offset-negative="100">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="/" class="standard-logo" data-dark-logo="{{ asset('assets/images/bittrex-logo.png')}}"><img src="{{ asset('assets/images/bittrex-logo.png')}}" alt="Bittrex Logo">
						</a>
						<!-- <a href="index.html" class="retina-logo" data-dark-logo="images/canvasone-dark@2x.png"><img src="images/canvasone@2x.png" alt="Canvas Logo"></a> -->
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">

						<ul class="one-page-menu" data-easing="easeInOutExpo" data-speed="1250" data-offset="65">
							<li><a href="#" data-href="#section-about"><div>About</div>
							</a></li>
							<li><a href="#" data-href="#section-works"><div>Currencies</div></a></li>
							<li><a href="#" data-href="#section-contact"><div>Contact</div></a></li>
							<li><a href="Markets" data-href=""><div>Markets</div></a></li>
							<li><a href="login" data-href=""><div>Login</div></a></li>
							
						</ul>

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->

			<!-- Slider
		============================================= -->
		<section id="slider" class="slider-parallax full-screen force-full-screen">

			<div class="slider-parallax-inner">

				<div class="full-screen force-full-screen dark section nopadding nomargin noborder ohidden" style="background-image: url('images/page/main.jpg'); background-size: cover; background-position: center center;">

					<div class="container center">
						<div class="vertical-middle">
							<div class="emphasis-title">
								<h1>
									<span class="text-rotater nocolor" data-separator="|" data-rotate="fadeIn" data-speed="6000">
										<span class="t-rotate t700 font-body opm-large-word"> BITTREX |THE NEXT GENERATION|DIGITAL CURRENCY EXCHANGE</span>
									</span>
								</h1>
							</div>

						</div>
					</div>

					<div class="video-wrap">
						<div class="video-overlay" style="background-color: rgba(0,0,0,0.55);"></div>
					</div>

					<a href="#" data-scrollto="#section-about" data-easing="easeInOutExpo" data-speed="1250" data-offset="65" class="one-page-arrow dark"><i class="icon-angle-down infinite animated fadeInDown"></i></a> 

				</div>

			</div>

		</section><!-- #slider end -->
		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap nopadding">

				<div id="section-about" class="center page-section">

					<div class="container clearfix">

						<h2 class="divcenter bottommargin font-body" style="max-width: 700px; font-size: 40px;">Trade Confidently</h2>

						<p class="lead divcenter bottommargin" style="max-width: 800px;"> We provide individuals and businesses a world class experience to buy and sell cutting-edge cryptocurrencies
                        and digital tokens. Based and fully regulated in the USA, Bittrex is the go-to spot for traders who demand lightning fast trade execution,
                        stable wallets, and industry-best security practices. Whether you are new to trading and cryptocurrencies, or a veteran to both, Bittrex.com
                        was created for you!
                        </p>

						<p class="bottommargin" style="font-size: 16px;"><a href="New-Account" data-scrollto="#section-services" data-easing="easeInOutExpo" data-speed="1250" data-offset="70" class="btn border-button">GET STARTED NOW<i class="fa fa-sign-in"></i></a></p>

						<div class="clear"></div>

						 <!-- call-to-action -->
   				  <div class="call-to-action">
      	 		  <div class="container-fluid">
          		  <div class="col-lg-5 col-lg-offset-1">
              	  <div id="carousel-call-to-action" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-call-to-action" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-call-to-action" data-slide-to="1"></li>
                        <li data-target="#carousel-call-to-action" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="carousel-caption">
                             <h2 class="heading"><i class="fa fa-flag"></i> USA Based</h2>
                                <p>
                                    Proudly based in the United States, we collaborate with experts in USA financial law to ensure that we remain in compliance with the evolving legal landscape
                                    and always available to our worldwide community. Our firm dedication to being legally compliant and fully
                                    regulated means that we were one of the first companies to apply for New York’s Bitlicense, allowing us to proudly serve New York customers.
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="carousel-caption">
                                <h2 class="heading"><i class="fa fa-lock"></i> Security First</h2>
                                <p>
                                    With over 50 years of security experience on our founder's resumes, security is our number one concern and priority. Our
                                    systems are constantly upgraded and tested to ensure that we are exceeding industry-best standards.  To protect users we require two-factor
                                    authentication for all withdrawals and API usage. The entirety of Bittrex.com is protected by SSL, so you can rest easy about the
                                    safety of your funds and personal information.
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="carousel-caption">
                                <h2 class="heading"><i class="fa fa-code"></i> Support for Algorithmic Trading</h2>
                                <p>
                                    While the Bittrex.com trading interface is designed to provide an intuitive and efficient trading experience, some of our
                                    traders enjoy trading through third-party platforms or designing their own algorithmic trading bots.
                                    With our dedication to providing the best trading experience possible, we designed our APIs to allow for high-frequency trading bots,
                                    and to allow mining pools to be build upon our platform.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-lg-offset-1 text-center wow fadeIn">
                <h2 class="market-header"><i class="fa fa-line-chart"></i> Active Markets</h2>
                <table id="market-summary-table" class="table_air" style="width:100%">
                    <thead>
                        <tr>
                            <th>Market</th>
                            <th style="text-align:right">Last Price</th>
                            <th style="text-align:right">Change</th>
                            <th style="text-align:right">24 Hour Volume</th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: filteredSummaries">
                        <tr>
                            <td data-bind="text: marketName"></td>
                            <td style="text-align:right" data-bind="text: last().toFixed(8), css: { 'flashed': last.absoluteChange() > 0 }"></td>
                            <td style="text-align:right" data-bind="text: change().toFixed(2) + '%'"></td>
                            <td style="text-align:right" data-bind="text: baseVolume().toFixed(2)"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    			</div> 

				</div>

				<div id="section-works" class="page-section notoppadding">

					<div class="section nomargin">
						  <div class="divcenter center" style="max-width: 900px;">
                    <h2 class="section-heading"><i class="fa fa-cog fa-spin"></i>Extensive Digital Currency Support</h2>
                    <h3>With blockchain technology continuing to innovate, Bittrex supports established and emerging currencies</h3>
                    <p>
                        The cryptocurrency landscape is in constant state of expansion as brand new cryptocurrencies innovate on blockchain technology
                        and develop business opportunities with the power of the blockchain. At Bittrex.com, we take pride in supporting both new and established
                        cryptocurrencies, providing you with an ever-growing selection of trading and investing opportunities. We conduct compliance audits on all new
                        coin launches, ensuring that our users have the information they need to make informed trades.
                    </p>
                    <p>
                        Below you will find a selection of the 190+ cryptocurrencies we support.
                    </p>
                </div>

					</div>

					<div id="portfolio" class="portfolio grid-container portfolio-nomargin portfolio-full portfolio-masonry mixed-masonry clearfix">

						<article class="portfolio-item pf-uielements pf-icons">
							<div class="portfolio-image">
							<img src="{{ asset('assets/images/bittrex/Card_BTC.jpg')}}" class="image responsive" alt="Open Imagination">
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
									<h2><a data-target="#portfolioModal1" class="portfolio-link" data-toggle="modal"> Bitcoin BTC</a></h2>
								</div>
							</div>
						</div>
						</article>


						<article class="portfolio-item pf-uielements pf-icons">
							<div class="portfolio-image">
							<img src="{{ asset('assets/images/bittrex/Card_GRC.jpg')}}" class="image responsive" alt="Locked Steel Gate">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
										<h2><a data-target="#portfolioModal8" class="portfolio-link" data-toggle="modal"> Gridcoin (GRC) </a></h2>
									</div>
								</div>
							</div>
						</article>


						<article class="portfolio-item pf-uielements pf-icons">
						<div class="portfolio-image">
						<img src="{{ asset('assets/images/bittrex/Card_LTC.jpg')}}" class="image responsive" alt="Mac Sunglasses">
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
	     <h2><a data-target="#portfolioModal3" class="portfolio-link" data-toggle="modal"> Litecoin (LTC) </a></h2>
									</div>
								</div>
							</div>
						</article>


						<article class="portfolio-item pf-uielements pf-icons">
							<div class="portfolio-image">
								<a href="#">
									<img src="{{ asset('assets/images/bittrex/Card_BLK.jpg')}}" class="image responsive"  alt="Open Imagination">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
				<h2><a data-target="#portfolioModal4" class="portfolio-link" data-toggle="modal"> Blackcoin (BLK) </a></h2>	
									</div>
								</div>
							</div>  
						</article>


						<article class="portfolio-item pf-uielements pf-icons">
							<div class="portfolio-image">
								<a href="#">
									<img src="{{ asset('assets/images/bittrex/Card_DASH.jpg')}}" class="image responsive" alt="Console Activity">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
			<h2><a data-target="#portfolioModal5" class="portfolio-link" data-toggle="modal"> Dash (DASH) </a></h2>					
									</div>  

								</div>
							</div>
						</article>


						<article class="portfolio-item pf-uielements pf-icons">
							<div class="portfolio-image">
								<a href="#">
									<img src="{{ asset('assets/images/bittrex/Card_ETH.jpg')}}" class="image responsive" alt="Open Imagination">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
					<h2><a data-target="#portfolioModal6" class="portfolio-link" data-toggle="modal"> Ethereum (ETH)</a></h2>					
									</div>
								</div>
							</div>  
						</article>


						<article class="portfolio-item pf-uielements pf-icons">
							<div class="portfolio-image">
								<a href="#">
									<img src="{{ asset('assets/images/bittrex/Card_GAM.jpg')}}" class="image responsive"  alt="Backpack Contents">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
		<h2><a data-target="#portfolioModal7" class="portfolio-link" data-toggle="modal"> Gambit (GAM) </a></h2>	
									</div>
								</div>

							</div>
						</article>
					<article class="portfolio-item pf-uielements pf-icons">
							<div class="portfolio-image">
								<a href="#">
									<img src="{{ asset('assets/images/bittrex/Card_UBQ.jpg')}}" class="image responsive" alt="Backpack Contents">
								</a>
								<div class="portfolio-overlay">
									<div class="portfolio-desc">
									<h2><a data-target="#portfolioModal2" class="portfolio-link" data-toggle="modal"> Ubiq (UBQ) </a></h2>
									</div>
									</div>
								</div>
							</div>
						</article>
					</div> 
					<!-- #portfolio end -->	

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <i class="fa fa-times fa-3x fa-fw"></i>
            </div>
            <div class="modal-body">
                <h2>Bitcoin (BTC)</h2>
                <hr>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe width="854" height="480" src="https://www.youtube.com/embed/Gc2en3nHxA4" frameborder="0" allowfullscreen></iframe>
                </div>
                <p><a href="https://en.wikipedia.org/wiki/Bitcoin">From Wikipedia:</a></p>
                <p>
                    Bitcoin is a digital asset and a payment system invented by Satoshi Nakamoto who published the invention in 2008 and released it as open-source software in 2009. The system is peer-to-peer;
                    users can transact directly without an intermediary. Transactions are verified by network nodes and recorded in a public distributed ledger called the block chain. The ledger uses bitcoin as
                    its unit of account. The system works without a central repository or single administrator, which has led the U.S. Treasury to categorize bitcoin as a decentralized virtual currency.
                    Bitcoin is often called the first cryptocurrency, although prior systems existed. Bitcoin is more correctly described as the first decentralized digital currency.
                    It is the largest of its kind in terms of total market value.
                </p>
                <p>
                    Bitcoins are created as a reward for payment processing work in which users offer their computing power to verify and record payments into a public ledger. This activity is called mining and
                    miners are rewarded with transaction fees and newly created bitcoins. Besides being obtained by mining, bitcoins can be exchanged for other currencies,
                    products, and services. Users can send and receive bitcoins for an optional transaction fee.
                </p>
                <ul class="list-inline item-details">
                    <li>Website: <strong><a href="https://bitcoin.org/">Bitcoin.org</a></strong></li>
                    <li>USD Trading: <strong><a href="/Market/Index?MarketName=USDT-BTC">USD</a></strong></li>
                    <li>CNY Trading: <strong><a href="/Market/Index?MarketName=BITCNY-BTC">CNY</a></strong></li>
                </ul>
                <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>

    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <i class="fa fa-times fa-3x fa-fw"></i>
            </div>
            <div class="modal-body">
                <h2>Jumbucks (JBS)</h2>
                <hr>
                <img src="{{ asset('assets/images/bittrex/Card_JBS.jpg')}}" class="img-responsive img-centered" alt="">
                <p><a href="http://getjumbucks.com/">From GetJumbucks.com:</a></p>
                <p>
                    The core idea behind Jumbucks is to create a coin for everyone to use. Jumbucks is not only meant to be fun and experimental, but it is designed
                    to be a beacon towards which all cryptocurrencies should be headed. Jumbucks' use cases will fund honest developers for future coin development,
                    and encourage people to keep their wallets open (and staking!) by offering unique wallet features. Compelling and self-sustaining use cases are planned
                    for Jumbucks, many of which have never yet been seen in the world of cryptocurrency. Jumbucks’ core is a fork of ShadowCoin, so it will immediately offer
                    great features such as P2P encrypted messaging and anonymous dual-key stealth addresses. Between its core functionalities, its sustainable use cases, and
                    its unique wallet features, Jumbucks is a coin designed for the whole cryptocurrency community.
                </p>
                <ul class="list-inline item-details">
                    <li>Website: <strong><a href="http://getjumbucks.com/">GetJumbucks.com</a></strong></li>
                    <li>BTC Trading: <strong><a href="/Market/Index?MarketName=BTC-JBS">BTC</a></strong></li>
                </ul>
                <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>

    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <i class="fa fa-times fa-3x fa-fw"></i>
            </div>
            <div class="modal-body">
                <h2>Litecoin (LTC)</h2>
                <hr>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe width="854" height="480" src="https://www.youtube.com/embed/VOVKowJkk2A" frameborder="0" allowfullscreen></iframe>
                </div>
                <p><a href="https://litecoin.org/">From Litecoin.org:</a></p>
                <p>
                    Litecoin is a peer-to-peer Internet currency that enables instant, near-zero cost payments to anyone in the world. Litecoin is an open source, global payment
                    network that is fully decentralized without any central authorities. Mathematics secures the network and empowers individuals to control their own finances.
                    Litecoin features faster transaction confirmation times and improved storage efficiency than the leading math-based currency. With substantial industry support,
                    trade volume and liquidity, Litecoin is a proven medium of commerce complementary to Bitcoin.
                </p>
                <ul class="list-inline item-details">
                    <li>Website: <strong><a href="https://litecoin.org/">Litecoin.org</a></strong></li>
                    <li>BTC Trading: <strong><a href="/Market/Index?MarketName=BTC-LTC">BTC</a></strong></li>
                </ul>
                <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>

    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <i class="fa fa-times fa-3x fa-fw"></i>
            </div>
            <div class="modal-body">
                <h2>Blackcoin (BLK)</h2>
                <hr>
                <img src="{{ asset('assets/images/bittrex/Card_BLK.jpg')}}" class="img-responsive img-centered" alt="">
                <p><a href="http://blackcoin.co/">From Blackcoin.co:</a></p>
                <p>
                    BlackCoin is a peer-to-peer digital currency with a distributed, decentralized public ledger; that unlike ones held at traditional banks, are viewable and easily audited by the people.
                    The ability to manage transactions and issue additional BlackCoins is all handled by the network of users utilizing BlackCoin. Because the BlackCoin network is run by the people,
                    holders of BlackCoin receive a 1% yearly interest through a process called staking.
                </p>
                <ul class="list-inline item-details">
                    <li>Website: <strong><a href="http://blackcoin.co/">Blackcoin.co</a></strong></li>
                    <li>BTC Trading: <strong><a href="/Market/Index?MarketName=BTC-BLK">BTC</a></strong></li>
                </ul>
                <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>

    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <i class="fa fa-times fa-3x fa-fw"></i>
            </div>
            <div class="modal-body">
                <h2>Dash (DASH)</h2>
                <hr>
                <img src="{{ asset('assets/images/bittrex/Card_DASH.jpg')}}" class="img-responsive img-centered" alt="">
                <p><a href="http://www.dash.org/what-is-dash/">From Dash.org:</a></p>
                <p>
                    Dash (DASH) is a privacy-centric digital currency with instant transactions. It is based on the Bitcoin 
                    software, but it has a two tier network that improves it. Dash allows you to remain anonymous while you make transactions, similar to cash.
                </p>
                <p>
                    With Bitcoin, transactions are published to the blockchain and you can prove who made them or to whom, 
                    but with Dash the anonymization technology makes it impossible to trace them. This is important because 
                    the blockchain is accessible to anyone with an internet connection – a significant drawback for those don’t 
                    wish their transaction history and balances to be publicly available. Dash does this through a mixing 
                    protocol utilizing an innovative decentralized network of servers called Masternodes, avoiding the need for 
                    a trusted third party that could compromise the integrity of the system.
                </p>
                <p>
                    Dash transactions are almost instantly confirmed by the Masternodes network. This is a great improvement on 
                    Bitcoin’s system, where confirmations take much longer because all the work is done by the miners.
                </p>
                <ul class="list-inline item-details">
                    <li>Website: <strong><a href="http://dash.org">Dash.org</a></strong></li>
                    <li>BTC Trading: <strong><a href="/Market/Index?MarketName=BTC-DASH">BTC</a></strong></li>
                </ul>
                <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>

    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <i class="fa fa-times fa-3x fa-fw"></i>
            </div>
            <div class="modal-body">
                <h2>Ethereum (ETH)</h2>
                <hr>
                <img src="{{ asset('assets/images/bittrex/Card_ETH.jpg')}}" class="img-responsive img-centered" alt="">
                <p><a href="https://www.ethereum.org/">From Ethereum.org:</a></p>
                <p>
                    Ethereum is a decentralized platform that runs smart contracts: applications that run exactly as programmed without any possibility of downtime, censorship, fraud or third party interference.
                </p>
                <p>
                    Ethereum is how the Internet was supposed to work.
                </p>
                <p>
                    Ethereum was crowdfunded during August 2014 by fans all around the world. It is developed by ETHDEV with contributions from great minds across the globe.
                </p>
                <ul class="list-inline item-details">
                    <li>Website: <strong><a href="https://www.ethereum.org/">Ethereum.org</a></strong></li>
                    <li>BTC Trading: <strong><a href="/Market/Index?MarketName=BTC-ETH">BTC</a></strong></li>
                </ul>
                <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>

    <div class="portfolio-modal modal fade" id="portfolioModal7" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <i class="fa fa-times fa-3x fa-fw"></i>
            </div>
            <div class="modal-body">
                <h2>Gambit (GAM)</h2>
                <hr>
                <img src="{{ asset('assets/images/bittrex/Card_GAM.jpg')}}" class="img-responsive img-centered" alt="">
                <p><a href="http://gambitcrypto.com/">From GambitCrypto.com:</a></p>
                <p>
                    Gambit (GAM) is an experimental trading token that combines cryptocurrency trading, hedging, and asset acquisitions. It is a fully proof-of-stake (PoS) coin with an initial distribution
                    via InitialCoinOffering.com. Gambit features a flat yearly staking interest of 5% to keep inflation low, but at the same time provides Gambit holders the ability to earn tokens for
                    helping to secure the network.
                </p>
                <p>
                    Gambit features a large BTC trading pool that is managed by day traders as a side pot to their regular positions. These traders are in constant competition to outperform eachother on the
                    trader spreadsheet. Seventy-five percent (75%) of all trading profits from these allotments will go directly into buy support on the GAM/BTC markets and/or towards refilling the fund.
                    Every single GAM that is purchased this way will be permanently burned, steadily reducing the total supply of Gambit so long as trading remains profitable.*
                </p>
                <p>
                    * Owning any cryptocurrency (such as Bitcoin or Gambit) is a high-risk activity with potentially volatile price movements. We strongly advise you to carefully consider your risk tolerance before joining the Gambit community.
                    The risks of loss in digital currencies are substantial. You may sustain a total loss of funds. Digital currencies are experimental and may succeed or may fail.
                </p>
                <ul class="list-inline item-details">
                    <li>Website: <strong><a href="http://gambitcrypto.com/">GambitCrypto.com</a></strong></li>
                    <li>BTC Trading: <strong><a href="/Market/Index?MarketName=BTC-GAM">BTC</a></strong></li>
                </ul>
                <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>

    <div class="portfolio-modal modal fade" id="portfolioModal8" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <i class="fa fa-times fa-3x fa-fw"></i>
            </div>
            <div class="modal-body">
                <h2>Gridcoin (GRC)</h2>
                <hr>
                <img src="{{ asset('assets/images/bittrex/Card_GRC.jpg')}}" class="img-responsive img-centered" alt="">
                <p><a href="http://www.gridcoin.us/">From Gridcoin.us:</a></p>
                <p>
                    Gridcoin (Ticker: GRC) is a decentralized, open source math-based digital asset (crypto-currency). It performs transactions peer-to-peer cryptographically without
                    the need for a central issuing authority. It is the first block chain protocol that delivered a working algorithm that equally rewards and cryptographically proves
                    solving BOINC (Berkley Open Infrastructure for Network Computing) hosted work, which can be virtually any kind of distributed computing process (ASIC/GPU/CPU/Sensor/Etc).
                </p>
                <p>
                    Gridcoin provides benefits to humanity through contributions to scientific research. It is the only crypto-currency that rewards individuals for BOINC
                    contributions without the need for a central authority to distribute rewards.
                </p>

                <ul class="list-inline item-details">
                    <li>Website: <strong><a href="http://www.gridcoin.us/">Gridcoin.us</a></strong></li>
                    <li>BTC Trading: <strong><a href="/Market/Index?MarketName=BTC-GRC">BTC</a></strong></li>
                </ul>
                <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
 
						<h1 align="center">Contact Us</h1>
					<div id="section-contact" class="page-section notoppadding">
					<div class="container clearfix">

						<div class="divcenter topmargin" style="max-width: 850px;">

							<div class="contact-widget">

								<div class="contact-form-result"></div>

								<form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">

									<div class="form-process"></div>

									<div class="col_half">
										<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control border-form-control required" placeholder="Name" />
									</div>
									<div class="col_half col_last">
										<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control border-form-control" placeholder="Email Address" />
									</div>

									<div class="clear"></div>

									<div class="col_one_third">
										<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control border-form-control" placeholder="Phone" />
									</div>

									<div class="col_two_third col_last">
										<input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control border-form-control" placeholder="Subject" />
									</div>

									<div class="clear"></div>

									<div class="col_full">
										<textarea class="required sm-form-control border-form-control" id="template-contactform-message" name="template-contactform-message" rows="7" cols="30" placeholder="Your Message"></textarea>
									</div>

									<div class="col_full center">
										<button class="button button-border button-circle t500 noleftmargin topmargin-sm" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
										<br>
										<small style="display: block; font-size: 13px; margin-top: 15px;">We'll do our best to get back to you within 6-8 working hours.</small>
									</div>

									<div class="clear"></div>

									<div class="col_full hidden">
										<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
									</div>

								</form> 

							</div>

						</div>

					</div>  

				</div>

			</div> 

		</section><!-- #content end -->
		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark noborder">

			

			<div id="copyrights">
				<div class="container center clearfix">
					 <p>&copy; 2018 Bittrex, INC</p>
					 <img class="img-responsive center-block" src="{{ asset('assets/images/bittrex/bittrex-logo.png')}}" />
				</div>				
			</div>

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/plugins.js"></script>

	<!-- Google Map JavaScripts
	============================================= -->
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=YOUR_API_KEY"></script>
	<script type="text/javascript" src="js/jquery.gmap.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="js/functions.js"></script>

	<script type="text/javascript">

		jQuery(window).load(function(){

			// Google Map
			jQuery('#headquarters-map').gMap({
				address: 'Melbourne, Australia',
				maptype: 'ROADMAP',
				zoom: 14,
				markers: [
					{
						address: "Melbourne, Australia",
						html: "Melbourne, Australia",
						icon: {
							image: "images/icons/map-icon-red.png",
							iconsize: [32, 32],
							iconanchor: [14,44]
						}
					}
				],
				doubleclickzoom: false,
				controls: {
					panControl: false,
					zoomControl: false,
					mapTypeControl: false,
					scaleControl: false,
					streetViewControl: false,
					overviewMapControl: false
				},
				styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"labels.text","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":"-100"},{"lightness":"30"}]},{"featureType":"administrative.neighborhood","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"simplified"},{"gamma":"0.00"},{"lightness":"74"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"lightness":"3"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
			});

		});

	</script>

</body>
</html>

