 <!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }} ">

    <!-- One Page Module Specific Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/onepage.css') }}">
    <!-- / -->

    <link rel="stylesheet" href="{{ asset('assets/css/dark.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/et-line.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/fonts.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Document Title
    ============================================= -->
    <title>One Page Module | Canvas</title>

</head>

 <h2>Register</h2>
    <form method="POST" action="/register">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
 
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" name="email">
        </div>
 
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>
 
        <div class="form-group">
            <label for="password_confirmation">Password Confirmation:</label>
            <input type="password" class="form-control" id="password_confirmation"
                   name="password_confirmation">
        </div>
 
        <div class="form-group">
            <button style="cursor:pointer" type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>

    <footer id="footer" class="dark noborder">

            <div class="container center">
                <div class="footer-widgets-wrap">

                    <div class="row divcenter clearfix">

                        <div class="col-md-4">

                            <div class="widget clearfix">
                                <h4>Site Links</h4>

                                <ul class="list-unstyled footer-site-links nobottommargin">
                                    <li><a href="#" data-scrollto="#wrapper" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Top</a></li>
                                    <li><a href="#" data-scrollto="#section-about" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">About</a></li>
                                    <li><a href="#" data-scrollto="#section-works" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Works</a></li>
                                    <li><a href="#" data-scrollto="#section-services" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Services</a></li>
                                    <li><a href="#" data-scrollto="#section-blog" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Blog</a></li>
                                    <li><a href="#" data-scrollto="#section-contact" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">Contact</a></li>
                                </ul>
                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="widget subscribe-widget clearfix" data-loader="button">
                                <h4>Subscribe</h4>

                                <div class="widget-subscribe-form-result"></div>
                                <form id="widget-subscribe-form" action="../include/subscribe.php" role="form" method="post" class="nobottommargin">
                                    <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control input-lg not-dark required email" placeholder="Your Email Address">
                                    <button class="button button-border button-circle button-light topmargin-sm" type="submit">Subscribe Now</button>
                                </form>
                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="widget clearfix">
                                <h4>Contact</h4>

                                <p class="lead">795 Folsom Ave, Suite 600<br>San Francisco, CA 94107</p>

                                <div class="center topmargin-sm">
                                    <a href="#" class="social-icon inline-block noborder si-small si-facebook" title="Facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon inline-block noborder si-small si-twitter" title="Twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon inline-block noborder si-small si-github" title="Github">
                                        <i class="icon-github"></i>
                                        <i class="icon-github"></i>
                                    </a>
                                    <a href="#" class="social-icon inline-block noborder si-small si-pinterest" title="Pinterest">
                                        <i class="icon-pinterest"></i>
                                        <i class="icon-pinterest"></i>
                                    </a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

            <div id="copyrights">
                <div class="container center clearfix">
                    Copyright Canvas 2015 | All Rights Reserved
                </div>
            </div>

        </footer><!-- #footer end -->
