<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


    <!-- General meta information -->
    <title>Login Form</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="robots" content="index, follow" /> 
    <meta charset="utf-8" />
    <!-- // General meta information -->
    
    
    <!-- Load Javascript -->
    <script type="text/javascript" src="{{ asset('assets/js/jquery1.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.query-2.1.7.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/rainbows.js')}}"></script>
    <!-- // Load Javascipt -->

    <!-- Load stylesheets -->
    <link rel="stylesheet" href="{{ asset('assets/css/style-login.css') }}">

    <!-- // Load stylesheets -->
    
<script>


    $(document).ready(function(){
 
    $("#submit1").hover(
    function() {
    $(this).animate({"opacity": "0"}, "slow");
    },
    function() {
    $(this).animate({"opacity": "1"}, "slow");
    });
    });


</script>
    
</head>
<body>
     <!--  <div id="wrapper">
        <div id="wrappertop"></div>
     <form method="POST" action="{{ route('login') }}">
        <div id="wrappermiddle">

            <h2>Login</h2>

            <div id="username_input">

                <div id="username_inputleft"></div>

                <div id="username_inputmiddle">
                <form>
                    <input type="text" name="link" id="url" value="E-mail Address" onclick="this.value = ''">
                    <img id="url_user" src="./images/mailicon.png" alt="">
                </form>
                </div>

                <div id="username_inputright"></div>

            </div>

            <div id="password_input">

                <div id="password_inputleft"></div>

                <div id="password_inputmiddle">
                <form>
                    <input type="password" name="link" id="url" value="Password" onclick="this.value = ''">
                    <img id="url_password" src="./images/passicon.png" alt="">
                </form>
                </div>

                <div id="password_inputright"></div>

            </div>

            <div id="submit">
                <form>
                <input type="image" src="./images/submit_hover.png" id="submit1" value="Sign In">
                <input type="image" src="./images/submit.png" id="submit2" value="Sign In">
                </form>
            </div>


            <div id="links_left">

            <a href="#">Forgot your Password?</a>

            </div>

            <div id="links_right"><a href="#">Not a Member Yet? Sign UP</a></div>

        </div>
    </form>

        <div id="wrapperbottom"></div>
        
        <div id="powered">
        <p>Powered by <a href="http://www.premiumfreebies.eu">Premiumfreebies Control Panel</a></p>
        </div> 
    </div> -->
 


  <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                            <div id="links_right"><a href="register">Not a Member Yet? Sign UP</a></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html> 
               
