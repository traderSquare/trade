<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Roboto:300,400,500,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />

    <!-- One Page Module Specific Stylesheet -->
    <link rel="stylesheet" href="css/onepage.css" type="text/css" />
    <!-- / -->
     <!-- Slider Syle -->
    <link rel="stylesheet" href="css/slider.css" type="text/css" />
    <!-- / -->

    <link rel="stylesheet" href="css/dark.css" type="text/css" />
    <link rel="stylesheet" href="css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="css/et-line.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="css/fonts.css" type="text/css" />

    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title
    ============================================= -->
    <title>Bittrex.com - Manage Settings</title>
</head>

<body class="stretched side-push-panel">

    <div class="body-overlay"></div>

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Header
        ============================================= -->

                </div>


        <header>
        <!-- #header Start-->
        
     <div id="header-navigation" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid container-header">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/Home/Markets">
                    <img src="{{ asset('assets/images/bittrex/bittrex-logo.png')}}" alt='Bittrex.com' height="24" />
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li class="dropdown" id="dash_menu_btc2">
                        <a href="#dash_menu_btc" class="dropdown-toggle" data-toggle="collapse">
                            <i class="fa fa-btc"></i>
                            <span>Markets</span>
                            <b class="caret"></b>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-left">
                    <li class="dropdown" id="dash_menu_fiat2">
                        <a href="#dash_menu_fiat" class="dropdown-toggle" data-toggle="collapse">
                            <i class="fa fa-usd"></i>
                            <span>Markets</span>
                            <b class="caret"></b>
                        </a>
                    </li>
                </ul>


        <ul class="nav navbar-nav navbar-right">

                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-flask"></i><span class="hidden-sm">&nbsp; Lab</span>  <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/Lab/Any2Any">
                                        <i class="fa fa-random"></i>
                                        <span>&nbsp; Any 2 Any</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/Lab/AutoSell">
                                        <i class="fa fa-recycle"></i>
                                        <span>&nbsp; Auto-Sell</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/Lab/ProfitLoss">
                                        <i class="fa fa-line-chart"></i>
                                        <span>&nbsp; Profit-Loss</span>
                                    </a>
                                </li>
                                </ul>

                                  <li id="dash_menu_history">
                            <a href="/History">
                                <i class="fa fa-calendar"></i>
                                <span class="hidden-sm">&nbsp; Orders</span>
                            </a>
                        </li>

                            <li id="dash_menu_wallet">
                            <a href="/Balance">
                                <i class="fa fa-btc"></i>
                                <span class="hidden-sm">&nbsp; Wallets</span>
                            </a>
                        </li>

                          <li id="dash_menu_settings">
                            <a href="/Manage">
                                <i class="fa fa-cog"></i>
                                <span class="hidden-sm">&nbsp; Settings</span>
                            </a>
                        </li>

                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-question-circle"></i><span class="hidden-sm">&nbsp; Help</span>  <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="dash_menu_news">
                                <a href="/News">
                                    <i class="fa fa-bullhorn"></i>
                                    <span>&nbsp; News</span>
                                </a>
                            </li>
                            <li id="dash_menu_status">
                                <a href="/Status">
                                    <i class="fa fa-lightbulb-o"></i>
                                    <span>&nbsp; Site Status</span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="/Home/About">About</a></li>
                            <li><a href="/Privacy">Your Privacy and Security</a></li>
                            <li><a href="/Home/Cookies">Cookie Policy</a></li>
                            <li><a href="/Home/Terms">Terms and Conditions</a></li>
                            <li><a href="/Home/Api">API Documentation</a></li>
                            <li><a href="/Fees">Fees</a></li>
                            <li><a href="https://bittrex.zendesk.com/hc/en-us" rel="noreferrer">Support</a></li>
                            <li><a href="/Home/Contact">Contact Us</a></li>
                        </ul>
                    </li>
        <li >
            <a href="/Account/Login">
                <i class="fa fa-sign-out"></i>
                <span >&nbsp; Logout </span>
            </a>
        </li>
    </ul>      
            </div>
        </div>
    </div> -->
</header>  
<!-- ##Header End -->
<br><br>
    <div id="media-width-detection-element"></div>
    <div id="layout-navigation" class="col-sm-12 menu-wrapper">
        <div id="dash_menu_btc" class="collapse menu-content" style="z-index:300">
            <p class="menu-sort">
                Sort By:
                <select data-bind="options: menu.availableSort, optionsText: 'sortName', value: menu.selectedSort, optionsCaption: 'Choose...'"></select>
                Max Rows:
                <select data-bind="options: menu.availableRows, optionsText: 'sortName', value: menu.selectedRows, optionsCaption: 'Choose...'"></select>
                Search:
                <input data-bind="value: menu.filter, valueUpdate: 'afterkeydown'" />
            </p>
            <table data-bind="foreach: menu.marketsBtcRows" class="menu-table">
                <tr data-bind="foreach: $data">
                    <td data-bind="css: { 'even': isEven(), 'odd': !isEven() }">
                        <div>
                            <div class="col-sm-2 name">
                                <a data-bind="attr: { href: urlPath, title:marketCurrencyName }, text:marketCurrency">Link</a>
                            </div>
                            <div class="col-sm-6 price"> <div data-bind="text: displayLast()"></div></div>
                            <div class="col-sm-4 delta"> <div data-bind="text: displayChange(), css: { 'dyn-div-up': change() > 0, 'dyn-div-down': change() < 0, 'dyn-div-none': change() == 0 }"></div></div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="dash_menu_fiat" class="collapse menu-content" style="z-index:300">
            <p class="menu-sort">
                Sort By:
                <select data-bind="options: menu.availableSort, optionsText: 'sortName', value: menu.selectedSort, optionsCaption: 'Choose...'"></select>
                Max Rows:
                <select data-bind="options: menu.availableRows, optionsText: 'sortName', value: menu.selectedRows, optionsCaption: 'Choose...'"></select>
                Search:
                <input data-bind="value: menu.filter, valueUpdate: 'afterkeydown'" />
            </p>
            <table data-bind="foreach: menu.marketsFiatRows" class="menu-table">
                <tr data-bind="foreach: $data">
                    <td data-bind="css: { 'even': isEven(), 'odd': !isEven() }">
                        <div>
                            <div class="col-sm-2 name">
                                <a data-bind="attr: { href: urlPath, title:marketCurrencyName }, text:marketCurrency">Link</a>
                            </div>
                            <div class="col-sm-6 price"> <div data-bind="text: displayLast()"></div></div>
                            <div class="col-sm-4 delta"> <div data-bind="text: displayChange(), css: { 'dyn-div-up': change() > 0, 'dyn-div-down': change() < 0, 'dyn-div-none': change() == 0 }"></div></div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="alert-news" class="alert alert-info alert-override" style="display:none; margin-top:48px; margin-bottom:0px">
        <i class="fa fa-exclamation-sign"></i>
        <span id="alert-news-text"></span>
        <a href="/News">&nbsp;For more information, see the News page.</a>
    </div>


<!-- <div id="pad-wrapper">
    <input name="__RequestVerificationToken" type="hidden" value="XPutwGf6rNU0_EgUiL_qu7S8hhgvdcaPQpPCB8TBuUvRedWYPGMnFYA3uBqny02rTJonqRJ1autgqWhgeMEOJ-09KobKD3-o6dHvS9f2bN3yenJRtZg5z7UGIQ0oAeWoSQ-rZg2" /> 
<div id="body-container">
        <div id="event-store"></div>
        <div class="content"></div>
<div class="container" id="manage-body">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="list-group" id="sidebar">
                    <a href="#sectionSummary" class="list-group-item active">
                        Summary
                    </a>
                    <hr />
                    <a href="#sectionBasicVerification" class="list-group-item" id="basicVerificationBtn">
                        Basic Verification
                    </a>
                    <a href="#sectionEnhancedVerification" class="list-group-item" id="enhancedVerificationBtn">
                        Enhanced Verification
                    </a>
                    <hr />
                    <a href="#sectionPassword" class="list-group-item" >
                        Password
                    </a>
                    <a href="#section2Fa" class="list-group-item" >
                        Two-Factor Authentication
                    </a>
                    <a href="#sectionApi" class="list-group-item">
                        API Keys
                    </a>
                    <a href="#sectionIpAddressWhiteList" class="list-group-item">
                        IP Whitelist
                    </a>
                    <a href="#sectionWithdrawAddressWhiteList" class="list-group-item">
                        Withdrawal Whitelist
                    </a>
                    <a href="#sectionEnableAccount" class="list-group-item">
                        Enable Account
                    </a>
                    <hr />
                    <a href="#sectionUi" class="list-group-item" >
                        UI Settings
                    </a>
                    <a href="#sectionNotifications" class="list-group-item" >
                        Notifications
                    </a>
                </div>
            </div>

<div class="col-md-9 col-xs-12" id="parentSection">
                <div class="manage-frame active" id="sectionSummary" >
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionBasicVerification" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionEnhancedVerification" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionPassword" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="section2Fa" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionApi" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionUi" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionNotifications" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionIpAddressWhiteList" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionWithdrawAddressWhiteList" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionEnableAccount" hidden>
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div> -->


<div class="content">
            





<div id="pad-wrapper">
    <input name="__RequestVerificationToken" type="hidden" value="RiAQIDnYQHGGkLk6xrClIcsGwWNTW225zfGf7wcZTMDiEefA1lcveRi3mINMlR7i9NR-LBDPrJ06mSF0szXdyPLomqBjG-a4QGKX_Ts3pKRV08t5DvLXL__8kOnT8igvD9OdKA2">
    <div class="container" id="manage-body">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="list-group affix" id="sidebar">
                    <a href="#sectionSummary" class="list-group-item active">
                        Summary
                    </a>
                    <hr>
                    <a href="#sectionBasicVerification" class="list-group-item" id="basicVerificationBtn">
                        Basic Verification
                    </a>
                    <a href="#sectionEnhancedVerification" class="list-group-item" id="enhancedVerificationBtn">
                        Enhanced Verification
                    </a>
                    <hr>
                    <a href="#sectionPassword" class="list-group-item">
                        Password
                    </a>
                    <a href="#section2Fa" class="list-group-item">
                        Two-Factor Authentication
                    </a>
                    <a href="#sectionApi" class="list-group-item">
                        API Keys
                    </a>
                    <a href="#sectionIpAddressWhiteList" class="list-group-item">
                        IP Whitelist
                    </a>
                    <a href="#sectionWithdrawAddressWhiteList" class="list-group-item">
                        Withdrawal Whitelist
                    </a>
                    <a href="#sectionEnableAccount" class="list-group-item">
                        Enable Account
                    </a>
                    <hr>
                    <a href="#sectionUi" class="list-group-item">
                        UI Settings
                    </a>
                    <a href="#sectionNotifications" class="list-group-item">
                        Notifications
                    </a>
                </div>
            </div>
            <div class="col-md-9 col-xs-12" id="parentSection">
                <div class="manage-frame loaded active" id="sectionSummary" style="display: block;"></div></div>





<div id="pad-wrapper">
    <input name="__RequestVerificationToken" type="hidden" value="fUf2dE2xvNAmq_znz9Gq-No4Hc7Ue1549oFw_pV8Rb-vDRwaOqgcMSzjaWtfvmu1Vbz03Ce8dd5I5FFf0rOqbNY4RGqxhwVPU7UMz7Abh4ilmMvVIUBU83CQdhHBVS6uFPTxSw2">
    <div class="container" id="manage-body">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="list-group affix" id="sidebar">
                    <a href="#sectionSummary" class="list-group-item">
                        Summary
                    </a>
                    <hr>
                    <a href="#sectionBasicVerification" class="list-group-item active" id="basicVerificationBtn">
                        Basic Verification
                    </a>
                    <a href="#sectionEnhancedVerification" class="list-group-item" id="enhancedVerificationBtn">
                        Enhanced Verification
                    </a>
                    <hr>
                    <a href="#sectionPassword" class="list-group-item">
                        Password
                    </a>
                    <a href="#section2Fa" class="list-group-item">
                        Two-Factor Authentication
                    </a>
                    <a href="#sectionApi" class="list-group-item">
                        API Keys
                    </a>
                    <a href="#sectionIpAddressWhiteList" class="list-group-item">
                        IP Whitelist
                    </a>
                    <a href="#sectionWithdrawAddressWhiteList" class="list-group-item">
                        Withdrawal Whitelist
                    </a>
                    <a href="#sectionEnableAccount" class="list-group-item">
                        Enable Account
                    </a>
                    <hr>
                    <a href="#sectionUi" class="list-group-item">
                        UI Settings
                    </a>
                    <a href="#sectionNotifications" class="list-group-item">
                        Notifications
                    </a>
                </div>
            </div>
            <div class="col-md-9 col-xs-12" id="parentSection">
                <div class="manage-frame loaded" id="sectionSummary" style="display: none;">



<div id="divManageSummary">
    <div class="col-md-10 load-wrapper" data-bind="visible: isLoading()" style="display: none;">
        <div class="loading">
            <i class="fa fa-spinner fa-pulse fa-5x"></i>
        </div>
    </div>

    <form id="formManageSummary" class="form-horizontal" style="margin-bottom: 24px;" data-bind="visible: !isLoading()">
        <div class="row">
            <div class="col-md-12">
                <h1 class="section-header">Account Information</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-12">
                <h2 class="identity-name">User Name</h2>
            </div>
            <div class="col-lg-6 col-md-12">
                <h2 class="identity-name"><small data-bind="text: email" style="margin-left:0px">mistinolle@gmail.com</small></h2>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-12">
                <h2 class="identity-name">
                    Account Type
                    <span class="label label-info" data-bind="text: accountType" style="padding-top:0px; padding-bottom:0px">NEW</span>
                    <a href="https://bittrex.zendesk.com/hc/en-us/articles/215282838" target="_blank"><span class="fa fa-question-circle"></span></a>
                </h2>
            </div>
            <div class="col-lg-6 col-md-12">
                <div style="margin-top:12px">
                    <div data-bind="template: { name: accountUpgradeTemplate }">
    <button data-bind="click: clickBasic" class="btn btn-success"><span class="fa fa-angle-double-up fa-fw"></span>&nbsp; Upgrade to Basic</button>
    <div class="row" style="margin-top: 10px">
        <div class="col-xs-12">
            New accounts must verify before withdrawing from Bittrex.
        </div>
    </div>
</div>
                </div>
            </div>
        </div>

        <div class="identity-divider"></div>

        <div class="row">
            <div class="col-md-6 col-xs-8">
                <h2 class="identity-attribute"><span class="fa fa-btc fa-fw"></span>&nbsp; Digital Token Trading</h2>
            </div>
            <div class="col-md-6 col-xs-4">
                <div style="margin-top:12px">
                    <div data-bind="template: { name: canCryptoTradeTemplate }">
    <span class="fa fa-square-o fa-2x" style="margin-top:2px"></span>
</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-8">
                <h2 class="identity-attribute"><span class="fa fa-usd fa-fw"></span>&nbsp; Fiat Trading</h2>
            </div>
            <div class="col-md-6 col-xs-4">
                <div style="margin-top:12px">
                    <div data-bind="template: { name: canFiatTradeTemplate }">
    <span class="fa fa-square-o fa-2x" style="margin-top:2px"></span>
</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-8">
                <h2 class="identity-attribute"><span class="fa fa-bank fa-fw"></span>&nbsp; Margin Trading</h2>
            </div>
            <div class="col-md-6 col-xs-4">
                <div style="margin-top:12px">
                    <div data-bind="template: { name: canMarginTradeTemplate }">
    <span class="fa fa-square-o fa-2x" style="margin-top:2px"></span>
</div>
                </div>
            </div>
        </div>

        <div class="identity-divider"></div>

        <div class="row">
            <div class="col-md-6 col-xs-8">
                <h2 class="identity-attribute"><span class="fa fa-cloud-upload fa-fw"></span>&nbsp; Daily Withdrawal Limit</h2>
            </div>
            <div class="col-md-6 col-xs-4">
                <h2 class="section-header" style="margin-bottom:12px; margin-top: 12px; padding-left: 0px; font-weight:500; font-size:18px" data-bind="text: dailyWithdrawalLimit() + ' BTC'">0 BTC</h2>
                <p data-bind="visible: !has2Fa()">To increase your limit on a Basic or Enhanced account, enable Two-Factor authentication <a href="/manage#section2Fa">here</a></p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <h2 class="identity-attribute">User Activity (Last 100 Entries)</h2>
            </div>
            <div class="col-xs-12">
                <div data-bind="if: activities.items().length > 0">
                    <div data-bind="template: { name: 'koTable-template', data: activities }">
    <table class="table table-striped table-hover table-condensed table-bordered">
        <thead>
            <tr data-bind="foreach: columns">
                <th data-bind="css: headerClass" class="col-header">
                    <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                        <span data-bind="html: header">Time Stamp</span>&nbsp;<i data-bind="css: state" class="fa fa-arrow-down"></i>
                    </div>
                </th>
            
                <th data-bind="css: headerClass" class="col-header">
                    <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                        <span data-bind="html: header">Address</span>&nbsp;<i data-bind="css: state"></i>
                    </div>
                </th>
            
                <th data-bind="css: headerClass" class="col-header">
                    <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                        <span data-bind="html: header">User Agent</span>&nbsp;<i data-bind="css: state"></i>
                    </div>
                </th>
            
                <th data-bind="css: headerClass" class="col-header">
                    <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                        <span data-bind="html: header">Activity</span>&nbsp;<i data-bind="css: state"></i>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody data-bind="foreach: { data: visibleItems, as: 'item' }">
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 12:24:14</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 12:24:05</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGOFF</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 11:00:51</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 10:59:53</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 UBrowser/7.0.69.1021 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 10:59:09</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 UBrowser/7.0.69.1021 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">VERIFY_NEW_IP</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 10:51:45</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">UNKNOWN_IP_LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 10:50:59</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">UNKNOWN_IP_LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">02/21/18 10:22:24</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.14</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">UNKNOWN_IP_LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">01/30/18 08:10:30</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.45</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36 OPR/50.0.2762.67 (Edition Campaign 34)</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">01/30/18 08:03:46</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.45</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36 OPR/50.0.2762.67 (Edition Campaign 34)</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">VERIFY_NEW_IP</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">01/30/18 08:03:18</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.150.45</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36 OPR/50.0.2762.67 (Edition Campaign 34)</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">UNKNOWN_IP_LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">01/22/18 14:21:38</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.167.163</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36 OPR/49.0.2725.64 (Edition Campaign 34)</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">01/22/18 14:20:40</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.167.163</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36 OPR/49.0.2725.64 (Edition Campaign 34)</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">VERIFY_NEW_IP</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">01/22/18 14:16:05</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.167.163</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">UNKNOWN_IP_LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">11/08/17 12:21:13</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.168.175</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">11/08/17 12:21:03</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.168.175</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">VERIFY_NEW_IP</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">11/08/17 12:20:14</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.168.175</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">UNKNOWN_IP_LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">10/18/17 10:55:44</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.167.223</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">IMAGE_INITIATE_NETVERIFY</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">10/18/17 10:52:03</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.167.223</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">LOGIN</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'"></div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')">10/18/17 10:51:52</span>
                    </div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">154.72.167.223</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]">VERIFY_NEW_IP</span>
                    </div>
                    <div data-bind="if: type == 'date'"></div>
                    <div data-bind="if: type == 'number'"></div>
                    <div data-bind="if: type == 'percent'"></div>
                    <div data-bind="if: type == 'template'"></div>
                </td>
            </tr>
        </tbody>

    </table>

    <div class="row">
        <div class="col-xs-12">
            <div style="float:left;margin-left:10px">
                <input type="text" class="form-control" data-bind="value: filter">
            </div>
            <div style="float:right">
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }" class="disabled"><a href="#" data-bind="click: firstPage">First</a></li></ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }" class="disabled"><a href="#" data-bind="click: previousPage">Prev</a></li></ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px" data-bind="foreach: allPages">
                    <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }" class="active"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">1</a></li>
                
                    <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">2</a></li>
                
                    <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">3</a></li>
                
                    <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">4</a></li>
                
                    <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }">5</a></li>
                </ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }"><a href="#" data-bind="click: nextPage">Next</a></li></ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }"><a href="#" data-bind="click: lastPage">Last</a></li></ul>
            </div>
        </div>
    </div>
</div>
                </div>
            </div>
        </div>

    </form>

</div>

<script type="text/html" id="new-account-template">
    <button data-bind="click: clickBasic" class="btn btn-success" ><span class="fa fa-angle-double-up fa-fw" ></span>&nbsp; Upgrade to Basic</button>
    <div class="row" style="margin-top: 10px">
        <div class="col-xs-12">
            New accounts must verify before withdrawing from Bittrex.
        </div>
    </div>
</script>

<script type="text/html" id="basic-account-template">
    <button data-bind="click: clickEnhanced" class="btn btn-success" ><span class="fa fa-angle-double-up fa-fw"></span>&nbsp; Upgrade to Enhanced</button>
</script>

<script type="text/html" id="enhanced-account-template">
</script>

<script type="text/html" id="checked-template">
    <span class="fa fa-check-square-o fa-2x" style="margin-top:2px"></span>
</script>

<script type="text/html" id="unchecked-template">
    <span class="fa fa-square-o fa-2x" style="margin-top:2px"></span>
</script>

<!-- Knockout Table -->
<script type="text/html" id="koTable-template">
    <table class="table table-striped table-hover table-condensed table-bordered">
        <thead>
            <tr data-bind="foreach: columns">
                <th data-bind="css: headerClass">
                    <div class="sortable" data-bind="click: $parent.sortClick.bind(property)">
                        <span data-bind="html: header"></span>&nbsp;<i data-bind="css: state"></i>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody data-bind="foreach: { data: visibleItems, as: 'item' }">
            <tr data-bind="foreach: { data: $parent.columns, as: 'column' } ">
                <td data-bind="css: cellClass">
                    <div data-bind="if: type == 'string'">
                        <span data-bind="text: item[column.property]"></span>
                    </div>
                    <div data-bind="if: type == 'date'">
                        <span data-bind="text: item[column.property].format('MM/DD/YY HH:mm:ss')"></span>
                    </div>
                    <div data-bind="if: type == 'number'">
                        <span data-bind="text: item[column.property].toFixed(8)"></span>
                    </div>
                    <div data-bind="if: type == 'percent'">
                        <span data-bind="text: item[column.property].toFixed(2) + '%'"></span>
                    </div>
                    <div data-bind="if: type == 'template'">
                        <div data-bind="template: { name: column.template, data: item }"></div>
                    </div>
                </td>
            </tr>
        </tbody>

    </table>

    <div class="row">
        <div class="col-xs-12">
            <div style="float:left;margin-left:10px">
                <input type="text" class="form-control" data-bind="value: filter">
            </div>
            <div style="float:right">
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }"><a href="#" data-bind="click: firstPage">First</a></li></ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === 0 }"><a href="#" data-bind="click: previousPage">Prev</a></li></ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px" data-bind="foreach: allPages">
                    <li data-bind="css: { active: $data.pageNumber === ($parent.pageIndex() + 1) }"><a href="#" data-bind="text: $data.pageNumber, click: function() { $parent.moveToPage($data.pageNumber-1); }"></a></li>
                </ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }"><a href="#" data-bind="click: nextPage">Next</a></li></ul>
                <ul class="pagination pagination-sm" style="margin: 5px 0px"><li data-bind="css: { disabled: pageIndex() === maxPageIndex() }"><a href="#" data-bind="click: lastPage">Last</a></li></ul>
            </div>
        </div>
    </div>
</script>


</div>
                <div class="manage-frame active loaded" id="sectionBasicVerification" hidden="" style="display: block;">



<div id="divManageBasic">
    <div class="col-md-10 load-wrapper" data-bind="visible: isLoading()" style="display: none;">
        <div class="loading">
            <i class="fa fa-spinner fa-pulse fa-5x"></i>
        </div>
    </div>

    <form id="formManageBasic" class="form-horizontal" style="margin-bottom: 24px;" data-bind="visible: !isLoading()" accept-charset="ISO-8859-1">

        <div class="panel panel-danger" data-bind="visible: stage() != 'VERIFIED'">
            <div class="panel-heading">
                <span class="fa fa-warning"></span>&nbsp;Warning about Deposits
            </div>
            <div class="panel-body">
                <p class="text-center">
                    Unverified accounts cannot withdraw from Bittrex.
                    Please see this <a href="https://support.bittrex.com/hc/en-us/articles/231701788">link</a> for more information.                   
                </p>
            </div>
        </div>

        <div data-bind="template: { name: formTemplate }">
    <div class="row">
        <div class="col-md-12">
            <h1 class="section-header">Basic Information</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div data-bind="template: { name: verificationStageTemplate }">
    <div class="panel panel-danger">
        <div class="panel-heading">
            <span class="fa fa-warning"></span>&nbsp;
            No public record match
        </div>
        <div class="panel-body">
            <p class="text-center">
                Thank you for providing your basic information.  To complete the verification process, you must now upload your identification documents.
            </p>

            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <h2 class="identity-name">Enhance</h2>
                </div>
                <div class="col-lg-6 col-md-12">
                    <button data-bind="click: clickEnhanced" class="btn btn-success identity"><span class="fa fa-angle-double-up fa-fw"></span>&nbsp; Supply Identification Documents</button>
                </div>
            </div>

        </div>
    </div>
</div>
        </div>
    </div>

    <div class="identity-divider"></div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Name</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name"><small data-bind="text: fullName" style="margin-left:0px">mekemjio mistinolle</small></h2>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Birth Date</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name"><small data-bind="text: birthDate().format('MMM DD, YYYY')" style="margin-left:0px">Oct 09, 1991</small></h2>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name compressed first">Address</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name compressed first"><small data-bind="text: address1" style="margin-left:0px">dschang</small></h2>
        </div>
        <div class="col-lg-offset-6 col-lg-6 col-md-12">
            <h2 class="identity-name compressed"><small data-bind="text: address2" style="margin-left:0px"></small></h2>
        </div>
        <div class="col-lg-offset-6 col-lg-6 col-md-12">
            <h2 class="identity-name compressed"><small data-bind="text: cityState" style="margin-left:0px">dschang, West</small></h2>
        </div>
        <div class="col-lg-offset-6 col-lg-6 col-md-12">
            <h2 class="identity-name compressed"><small data-bind="text: postalCode" style="margin-left:0px">173</small></h2>
        </div>
        <div class="col-lg-offset-6 col-lg-6 col-md-12">
            <h2 class="identity-name compressed"><small data-bind="text: fullCountry" style="margin-left:0px">Cameroon</small></h2>
        </div>
    </div>
</div>
    </form>

</div>

<script type="text/html" id="new-basic-template">
    <div class="row">
        <div class="col-md-12">
            <h1 class="section-header">Basic Information</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div data-bind='template: { name: "stage-new-template" }'></div>
        </div>
    </div>

    <div class="identity-divider"></div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">First Name</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <input data-bind="textInput: firstName" type="text" class="form-control identity" placeholder="First Name">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Middle Name</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <input data-bind="textInput: middleName" type="text" class="form-control identity" placeholder="Middle Name">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Last Name</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <input data-bind="textInput: lastName" type="text" class="form-control identity" placeholder="Last Name">
        </div>
    </div>

    <div class="identity-divider"></div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Birth Month</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <select data-bind="options: availableMonths, optionsCaption: 'Choose your Birth Month ...', value: birthMonth, optionsText: 'name', optionsValue: 'number'" class="form-control identity"></select>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Birth Day</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <select class="form-control identity" data-bind="options: availableDates, optionsCaption: 'Choose your Birth Day ...', value: birthDay"></select>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Birth Year</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <select class="form-control identity" data-bind="options: availableYears, optionsCaption: 'Choose your Birth Year ...', value: birthYear"><</select>
        </div>
    </div>

    <div class="identity-divider"></div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Country</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <select data-bind="options: availableCountries, optionsCaption: 'Choose your country ...', value: country, optionsText: 'Name', optionsValue: 'ISO3'" class="form-control identity"></select>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Street Address</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <input data-bind="textInput: address1" type="text" class="form-control identity" placeholder="Street Address">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Apartment or Unit</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <input data-bind="textInput: address2" type="text" class="form-control identity" placeholder="Apartment or Unit">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">City</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <input data-bind="textInput: city" type="text" class="form-control identity" placeholder="City">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">State or Province</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <select class="form-control identity" data-bind="options: availableStates, optionsCaption: 'Choose your state or province ...', value: state, optionsText: 'Name', optionsValue: 'Abbr2'"></select>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Postal or Zipcode</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <input data-bind="textInput: postalCode" type="text" class="form-control identity" placeholder="Postal Code">
        </div>
    </div>

    <div class="identity-divider"></div>

    <div class="row">
        <div data-bind='template: { name: idTemplate }'></div>
    </div>

    <div class="identity-divider"></div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
        </div>
        <div class="col-lg-6 col-md-12">
            <button type="submit" class="btn btn-success" data-bind="click: submitInformation">Submit <i data-bind="visible: isSaving" class="fa fa-circle-o-notch fa-spin fa-fw"></i></button>        
        </div>
    </div>
</script>

<script type="text/html" id="ssn-template">
    <div class="col-lg-6 col-md-12">
        <h2 class="identity-name">Social Security Number (Last 4 Digits)</h2>
    </div>
    <div class="col-lg-6 col-md-12">
        <input data-bind="textInput: ssn" type="text" class="form-control identity" placeholder="Social Security Number">
    </div>
</script>

<script type="text/html" id="passport-template">
    <div class="col-lg-6 col-md-12">
        <h2 class="identity-name">Passport Number (Optional)</h2>
    </div>
    <div class="col-lg-6 col-md-12">
        <input data-bind="textInput: passport" type="text" class="form-control identity" placeholder="Passport Number">
    </div>
</script>

<script type="text/html" id="submitted-basic-template">
    <div class="row">
        <div class="col-md-12">
            <h1 class="section-header">Basic Information</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div data-bind='template: { name: verificationStageTemplate }'></div>
        </div>
    </div>

    <div class="identity-divider"></div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Name</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name"><small data-bind="text: fullName" style="margin-left:0px"></small></h2>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name">Birth Date</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name"><small data-bind="text: birthDate().format('MMM DD, YYYY')" style="margin-left:0px"></small></h2>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name compressed first">Address</h2>
        </div>
        <div class="col-lg-6 col-md-12">
            <h2 class="identity-name compressed first"><small data-bind="text: address1" style="margin-left:0px"></small></h2>
        </div>
        <div class="col-lg-offset-6 col-lg-6 col-md-12">
            <h2 class="identity-name compressed"><small data-bind="text: address2" style="margin-left:0px"></small></h2>
        </div>
        <div class="col-lg-offset-6 col-lg-6 col-md-12">
            <h2 class="identity-name compressed"><small data-bind="text: cityState" style="margin-left:0px"></small></h2>
        </div>
        <div class="col-lg-offset-6 col-lg-6 col-md-12">
            <h2 class="identity-name compressed"><small data-bind="text: postalCode" style="margin-left:0px"></small></h2>
        </div>
        <div class="col-lg-offset-6 col-lg-6 col-md-12">
            <h2 class="identity-name compressed"><small data-bind="text: fullCountry" style="margin-left:0px"></small></h2>
        </div>
    </div>
</script>

<script type="text/html" id="stage-verified-template">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span class="fa fa-check-circle-o fa-fw"></span>&nbsp;
            Basic Account Approved
        </div>
        <div class="panel-body">
            <p class="text-center">  
                Your information has been processed and approved. 
            </p>
        </div>
    </div>
</script>

<script type="text/html" id="stage-no-identity-template">
    <div class="panel panel-danger">
        <div class="panel-heading">
            <span class="fa fa-warning"></span>&nbsp;
            No public record match
        </div>
        <div class="panel-body">
            <p class="text-center">
                Thank you for providing your basic information.  To complete the verification process, you must now upload your identification documents.
            </p>

            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <h2 class="identity-name">Enhance</h2>
                </div>
                <div class="col-lg-6 col-md-12">
                    <button data-bind="click: clickEnhanced" class="btn btn-success identity"><span class="fa fa-angle-double-up fa-fw"></span>&nbsp; Supply Identification Documents</button>
                </div>
            </div>

        </div>
    </div>
</script>

<script type="text/html" id="stage-new-template">
    <div class="panel panel-info">
        <div class="panel-heading">
            <span class="fa fa-info-circle fa-fw"></span>&nbsp;
            New Account
        </div>
        <div class="panel-body">
            <p class="text-center">
                Please submit the information below to apply for a basic account.  Please use your full legal name as it exists on your government identification.
            </p>
        </div>
    </div>
</script>

<script type="text/html" id="stage-processing-template">
    <div class="panel panel-warning">
        <div class="panel-heading">
            <span class="fa fa-refresh fa-spin fa-fw"></span>&nbsp;
            Processing information
        </div>
        <div class="panel-body">
            <p class="text-center">
                Thank you for providing your information.
            </p>
        </div>
   </div>
</script>

</div>
                <div class="manage-frame" id="sectionEnhancedVerification" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionPassword" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="section2Fa" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionApi" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionUi" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionNotifications" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionIpAddressWhiteList" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionWithdrawAddressWhiteList" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
                <div class="manage-frame" id="sectionEnableAccount" hidden="" style="display: none;">
                    <div class="col-md-10 load-wrapper">
                        <div class="loading">
                            <i class="fa fa-spinner fa-pulse fa-5x"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>











        









      <!-- END Navigation FOOTER --> 

        <div class="hidden-lg" style="margin-bottom: 47px"></div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" aria-labelledby="resfreshModalLabel" aria-hidden="true" id="refreshModal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="resfreshModalLabel">
                        Bittrex has updated, please refresh your browser
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="window.location.reload(true); return false;"><span class="glyphicon glyphicon-refresh"></span>&nbsp;Refresh</button>
                </div>
            </div>
        </div>
    </div>


<footer id="footer-navigation" class="navbar navbar-inverse navbar-fixed-bottom">
        <div class="container-fluid container-footer">
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a>
                            <span>© 2018 Bittrex, INC</span>
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a>
                            <span data-bind="text:menu.displayTotalBtc">Total BTC Volume = 41013.2925</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <span data-bind="text:menu.displayTotalEth">Total ETH Volume = 18819.5684</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <span data-bind="text:navigation.displayBitcoinUsd">1 BTC = $10990.4388</span>
                        </a>
                    </li>
                    <li>
                        <a>
                            <i class="fa fa-wifi socket-status-warning" data-bind="css: { 'socket-status-normal': socket.connectionStatus() == 'Connected', 'socket-status-warning': socket.connectionStatus() == 'Slow' || socket.connectionStatus() == 'Reconnecting', 'socket-status-error': socket.connectionStatus() == 'Disconnected' }"></i>
                            <span data-bind="text:socket.displayStatus">Socket Status = Slow</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
</body>
</html>