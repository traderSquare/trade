
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
  <meta charset="utf-8">
  <!-- v13576 -->
  <title>Bittrex Support</title>

  <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="VbGiokazZO6X3qOhflW09KPreNyZRlDIpMqA3coLWod/IYciAQWezVNnZd3d0K9PIUcf5bxvMCkNkInChs2t1Q==" />
  <link rel="canonical" href="https://support.bittrex.com/hc/en-us" />
<link rel="alternate" hreflang="en" href="https://support.bittrex.com/hc/en-us" />

  <!-- Entypo pictograms by Daniel Bruce — www.entypo.com -->
  <link rel="stylesheet" media="all" href="//p13.zdassets.com/hc/assets/application-3b0b6df180f05e3fa954d2e4d90e4600.css" id="stylesheet" />
  <link rel="stylesheet" type="text/css" href="//p13.zdassets.com/hc/themes/478848/115000640712/style-badae25e3dedd74860d69cd8fe0c5fcc.css?brand_id=149544&amp;locale=en-us" />

  <link rel="shortcut icon" type="image/x-icon" href="//p13.zdassets.com/hc/settings_assets/478848/200016824/fLQwWyw6nq0FP0k0jDd9Hw-bittrex-favicon-32x32.png" />

  <!--[if lt IE 9]>
  <script>
    //Enable HTML5 elements for <IE9
    'abbr article aside audio bdi canvas data datalist details dialog \
    figcaption figure footer header hgroup main mark meter nav output \
    progress section summary template time video'.replace(/\w+/g,function(n){document.createElement(n)});
  </script>
<![endif]-->

  <script src="//p13.zdassets.com/hc/assets/jquery-b60ddb79ff2563b75442a6bac88b00b5.js"></script>
  
  
  

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,300,300italic"  type="text/css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/highlight.js/9.10.0/styles/github.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.magnific-popup/1.0.0/magnific-popup.css" />
<style>
.hero-bg {
  background-image: url(//p13.zdassets.com/hc/theme_assets/478848/200016824/cover-1.3.jpg);
}
</style>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script src="https://assets.zendesk.com/assets/apps/conditional_fields/latest/helpcenter.js"></script>
<script>var cfaRules = [{"fieldType":"tagger","field":360000516212,"value":"need_new_account_created_yes","select":[360000543171,360000518832],"formId":114093958552,"requireds":[360000518832,360000543171]},{"fieldType":"tagger","field":360000517092,"value":"outside_united_states","select":[360000543651],"formId":114093958552,"requireds":[360000543651]},{"fieldType":"tagger","field":360000542871,"value":"corporate_account","select":[360000544311,360000544331,114096792652,360000517092],"formId":114093958552,"requireds":[360000544311,360000544331,114096792652,360000517092]}];</script>
<script src="//cdn.jsdelivr.net/jquery.magnific-popup/1.0.0/jquery.magnific-popup.min.js"></script>
<script src="//cdn.jsdelivr.net/highlight.js/9.10.0/highlight.min.js"></script>

  <script type="text/javascript" src="//p13.zdassets.com/hc/themes/478848/115000640712/script-badae25e3dedd74860d69cd8fe0c5fcc.js?brand_id=149544&amp;locale=en-us"></script>
</head>
<body class="">
  
  


  


  <div class="layout">
  <header class="topbar container" data-topbar>
    <div class="container-inner">
      <div class="topbar__inner">
        <div class="topbar__col clearfix">
          <div class="logo-wrapper">
            <div class="logo">
              <a title="Home" href="Bittrex-Support">
                <img src="//p13.zdassets.com/hc/settings_assets/478848/200016824/xSaHUeYiwl0eOCb0Fou1tg-bittrex-logo.svg" alt="Logo">
              </a> 
            </div>
          </div>
          <!-- <p class="help-center-name">Bittrex Support</p> -->
          <button type="button" role="button" aria-label="Toggle Navigation" class="lines-button x" data-toggle-menu> <span class="lines"></span> </button>
        </div>
        <div class="topbar__col topbar__menu">
          <div class="topbar__collapse" data-menu>
            <div class="topbar__controls">
               
              <a class="btn btn--topbar submit-a-request" href="Submit-Request">Submit a request</a> 
                <a class="login" role="button" href="login">Sign in</a>

            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  

  <main role="main">
    <div class="hero-unit" data-hero-unit>
  <div class="hero-bg" data-hero-bg></div>
  <div class="container-inner search-box search-box--hero-unit" data-search-box>
    <div class="intro-title">How can we help?</div>
    <form role="search" class="search" data-search="" data-instant="true" autocomplete="off" action="/hc/en-us/search" accept-charset="UTF-8" method="get"><input name="utf8" type="hidden" value="&#x2713;" /><input type="search" name="query" id="query" placeholder="Search" autocomplete="off" aria-label="Search" />
<input type="submit" name="commit" value="Search" /></form>
  </div>
</div>


<div class="container">
  <div class="container-inner">
    <div class="row custom-blocks clearfix" id="custom-blocks">
      <div class="column column--xs-4 custom-block">
        <a class="custom-block__link" href="https://www.bittrex.com/Manage" target="_blank" Alt="Bittrex User Settings">
          <div class="custom-block__icon">
            <svg width="66" height="66" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
              <g fill="#231F20">
                <path d="M65.7927221,29.335539 C66.0737133,31.8382706 66.0691546,34.3624114 65.7781243,36.8408649 C65.6052905,38.3073561 64.2910407,39.4083694 62.8854563,39.2930155 L62.516505,39.2829446 C60.0898776,39.2829446 57.8237363,40.7847028 56.8923788,43.0079737 C55.9002342,45.3720161 56.519829,48.1291462 58.4347595,49.8623696 C59.524792,50.8505376 59.6554773,52.516525 58.7336222,53.6618469 C57.1625657,55.6181713 55.3669683,57.3937158 53.388841,58.945079 C52.2400962,59.8330624 50.6091519,59.6982408 49.6230867,58.6249495 C47.9935812,56.836304 45.0498635,56.1624341 42.8159114,57.0746265 C40.4796631,58.0311083 38.9656765,60.3898464 39.0575253,62.9393554 C39.1068296,64.3928682 38.0309239,65.6402357 36.5849322,65.799815 C35.3817554,65.9334643 34.1742617,66 32.9618053,66 C31.6857272,66 30.4170418,65.9265026 29.1610603,65.7795128 C27.6980433,65.6122482 26.6194059,64.3283952 26.7066681,62.8598896 C26.8525255,60.2831361 25.358312,57.885251 22.9878932,56.8935333 C20.7508015,55.9544259 17.7755139,56.6269937 16.1379702,58.4373094 C15.1469974,59.529812 13.481169,59.6609231 12.3343555,58.737536 C10.3993684,57.1795343 8.64055803,55.4008082 7.11588116,53.4574253 C6.21305863,52.3166037 6.34658462,50.6734947 7.41825175,49.6894096 C9.29906427,47.9646889 9.91748053,45.2309558 8.95883604,42.8933238 C8.00364749,40.570958 5.7240027,39.0668887 3.29905373,39.1284608 C1.84898786,39.1284608 0.364914037,38.0801841 0.206420426,36.6649618 C-0.0734017087,34.161995 -0.0689175019,31.6357438 0.221452364,29.1575869 C0.398014736,27.6975674 1.58420568,26.6579403 3.06300667,26.7057196 C5.79990159,26.7057196 8.16001296,25.2401143 9.10314713,22.9937737 C10.0959818,20.626044 9.47848829,17.8701443 7.56326063,16.1356257 C6.47718382,15.1478401 6.34489194,13.4841734 7.2649863,12.3391844 C8.82648252,10.3908869 10.6247521,8.61323195 12.6069214,7.05787633 C13.7530632,6.16386909 15.3953843,6.2999154 16.3769119,7.37618204 C17.999671,9.1615821 20.9560202,9.83601798 23.1795533,8.9278037 C25.517621,7.9700446 27.0305385,5.61216536 26.9416918,3.05972675 C26.8924012,1.60661887 27.9690632,0.36015714 29.4143791,0.200447254 C31.8813036,-0.0733429131 34.3682213,-0.0664112199 36.8402182,0.219807146 C38.3048712,0.390338645 39.3805547,1.67038532 39.2937162,3.1396047 C39.1469745,5.71314912 40.6404049,8.11318973 43.0021402,9.10707643 C45.2496208,10.0447053 48.2269148,9.37196496 49.856362,7.56679489 C50.8422846,6.47557768 52.5126528,6.34302985 53.6559781,7.25991015 C55.5794459,8.80037021 57.3386583,10.5768549 58.8855168,12.5408265 C59.7871644,13.6839108 59.6546333,15.3296332 58.5811291,16.3107129 C56.704284,18.0325636 56.0857737,20.7680106 57.04084,23.1068762 C57.9776308,25.3923582 60.2124259,26.8709189 62.7421339,26.8709189 C64.1782931,26.8709189 65.6343239,27.9272252 65.7927221,29.335539 Z M63.8052316,29.5588807 C63.7715528,29.2594441 63.2360031,28.8709189 62.7421339,28.8709189 C59.4058189,28.8709189 56.4368097,26.9065954 55.1897633,23.8641828 C53.9172916,20.748025 54.7368628,17.1233586 57.2304872,14.8356662 C57.5310088,14.5610176 57.568129,14.100073 57.3147797,13.7788832 C55.8614446,11.9336541 54.2098228,10.2658164 52.4052478,8.82057456 C52.0868964,8.56527534 51.6158802,8.60265163 51.3406739,8.90724927 C49.144086,11.340722 45.2635025,12.2175669 42.2292323,10.9516912 C39.0836358,9.62793991 37.1020843,6.44345679 37.2970799,3.02367707 C37.3214255,2.61176344 37.0210975,2.25437779 36.6095523,2.20646083 C34.2876076,1.93761634 31.9518624,1.931106 29.6345195,2.18829483 C29.2288069,2.2331268 28.9266683,2.5829155 28.9405117,2.99103784 C29.0583652,6.37678669 27.0527826,9.50248381 23.9367481,10.7789263 C20.9332856,12.0057078 17.083863,11.1275339 14.8980253,8.72261867 C14.6228229,8.42085377 14.1604752,8.38255389 13.8392768,8.63308803 C11.9797399,10.0922223 10.2909148,11.7616873 8.82480104,13.5909737 C8.5668722,13.9119473 8.60398714,14.3786948 8.90737162,14.6546256 C11.4488862,16.9563346 12.2688888,20.6160411 10.9473835,23.7675901 C9.68240162,26.7805025 6.58216252,28.7057196 3.03105891,28.7052091 C2.5650564,28.6903138 2.26022261,28.9574828 2.20742499,29.3940165 C1.93526826,31.7167891 1.93105498,34.0904104 2.19401648,36.4425615 C2.22675539,36.734894 2.78393868,37.1284608 3.27353662,37.1287865 C6.50662031,37.0462606 9.53953935,39.0473269 10.8088874,42.1335152 C12.0866931,45.2494125 11.2667784,48.8738773 8.77048139,51.163002 C8.46984471,51.4390692 8.43252666,51.8982878 8.68679841,52.2196041 C10.1200114,54.0463919 11.7716519,55.7167345 13.5886552,57.1797388 C13.9103868,57.4387894 14.3767919,57.4020804 14.6556747,57.0946262 C16.8600625,54.6576585 20.7396568,53.780671 23.7609161,55.0489645 C26.9135043,56.3679192 28.8971558,59.5512451 28.7033092,62.9757194 C28.6788893,63.3866928 28.9806438,63.7458575 29.3908846,63.7927633 C30.5720966,63.9309993 31.7631616,64 32.9618053,64 C34.1006689,64 35.2344498,63.937526 36.3648369,63.8119625 C36.7706602,63.7671759 37.0725818,63.4171386 37.0587484,63.0092602 C36.9368648,59.6261333 38.944339,56.4985586 42.0589956,55.2233897 C45.0729179,53.9927104 48.9084123,54.8707242 51.0987064,57.2749388 C51.3777852,57.5787009 51.8371443,57.6166737 52.160136,57.3670312 C54.0123296,55.9144033 55.6988434,54.2467242 57.1749123,52.408682 C57.4334861,52.0874271 57.3968982,51.621002 57.0920632,51.3446536 C54.5497492,49.0435795 53.7269542,45.3822311 55.0479523,42.2346108 C56.2910329,39.2672156 59.2852822,37.2829446 62.5428507,37.2832917 L62.9925467,37.2966277 C63.3700381,37.327208 63.7438743,37.0140266 63.7918214,36.6071968 C64.0649441,34.2812468 64.0692262,31.9102272 63.8052316,29.5588807 Z M33.0502113,42.7419655 C38.3843923,42.7419655 42.7241226,38.4020007 42.7241226,33.0682997 C42.7241226,27.7343428 38.384434,23.3943363 33.0502113,23.3943363 C27.7167563,23.3943363 23.377193,27.7344681 23.377193,33.0682997 C23.377193,38.4018754 27.716798,42.7419655 33.0502113,42.7419655 Z M33.0502113,44.7419655 C26.6121785,44.7419655 21.377193,39.5063949 21.377193,33.0682997 C21.377193,26.6299573 26.6121281,21.3943363 33.0502113,21.3943363 C39.4890363,21.3943363 44.7241226,26.6298061 44.7241226,33.0682997 C44.7241226,39.5065461 39.4889859,44.7419655 33.0502113,44.7419655 Z" />
              </g>
            </svg>
          </div>
          <h4 class="custom-block__title">Bittrex User Settings</h4> </a>
      </div>
      <div class="column column--xs-4 custom-block">
        <a class="custom-block__link" href="https://www.bittrex.com/Home/Markets">
          <div class="custom-block__icon">
            <svg width="66" height="66" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
              <g fill="#231F20">
                <path d="M3.1403,41.5307 C2.3533,41.5307 1.5893,41.2857 0.9753,40.7217 C-0.5707,39.2067 -0.1267,36.6117 1.2443,35.1267 L10.7463,24.9857 C12.3213,23.2787 15.2873,22.5817 17.4963,23.4047 L32.2103,28.8897 C33.7153,29.4517 35.9333,28.9777 37.0533,27.8577 L52.1113,12.7997 L48.7753,9.4657 C47.5173,8.2087 47.6443,7.1497 47.8323,6.6307 C48.0213,6.1097 48.6043,5.2097 50.3973,5.0477 L61.7443,4.0167 C62.9573,3.9107 64.0983,4.3027 64.8973,5.1017 C65.7073,5.9117 66.0923,7.0307 65.9813,8.2527 L64.9513,19.6007 C64.7873,21.3927 63.8863,21.9777 63.3643,22.1657 C62.8473,22.3527 61.7873,22.4807 60.5313,21.2217 L57.1973,17.8857 L39.7213,35.3587 C38.0843,36.9957 35.0973,37.6397 32.9143,36.8257 L18.3003,31.3767 C16.8243,30.8247 14.6623,31.3457 13.5853,32.5127 L6.5473,39.9847 C5.6843,40.9197 4.3833,41.5307 3.1403,41.5307 Z M15.4803,24.9447 C14.2333,24.9447 12.8943,25.4387 12.1273,26.2707 L2.6253,36.4107 C1.7943,37.3117 1.6243,38.7167 2.2733,39.3537 C2.9003,39.9287 4.3163,39.6207 5.1683,38.6987 L12.2063,31.2267 C13.7963,29.5047 16.7663,28.7927 18.9593,29.6097 L33.5733,35.0577 C35.0683,35.6147 37.2703,35.1417 38.3873,34.0257 L57.1973,15.2177 L61.8653,19.8887 C62.3163,20.3407 62.6393,20.4247 62.7233,20.3927 C62.8113,20.3607 63.0133,20.0827 63.0723,19.4287 L64.1033,8.0817 C64.1623,7.4267 63.9713,6.8427 63.5643,6.4357 C63.1553,6.0277 62.5663,5.8337 61.9153,5.8957 L50.5673,6.9257 C49.9143,6.9847 49.6373,7.1847 49.6053,7.2727 C49.5743,7.3577 49.6563,7.6807 50.1083,8.1317 L54.7783,12.7997 L38.3863,29.1917 C36.7483,30.8307 33.7473,31.4747 31.5513,30.6567 L16.8363,25.1717 C16.4233,25.0177 15.9583,24.9447 15.4803,24.9447 Z M41.2894,62.3962 L39.4034,62.3962 L39.4034,43.2912 C37.7224,44.8032 34.8554,45.3752 32.7454,44.5922 L27.1444,42.5122 L27.1444,62.3962 L25.2584,62.3962 L25.2584,39.8002 L33.4024,42.8242 C34.9074,43.3842 37.1344,42.9032 38.2624,41.7762 L41.2894,38.7492 L41.2894,62.3962 Z M62.5116,62.3944 L60.6256,62.3944 L60.6256,30.5824 L56.3736,26.3334 L48.3626,34.3424 L48.3626,62.3944 L46.4766,62.3944 L46.4766,33.5604 L56.3736,23.6674 L62.5116,29.8004 L62.5116,62.3944 Z M20.058,62.3953 L18.172,62.3953 L18.172,39.2313 C16.69,38.9513 14.937,39.4623 14.012,40.4653 L5.913,49.2433 L5.913,62.3943 L4.027,62.3943 L4.027,48.5073 L12.625,39.1863 C14.201,37.4783 17.16,36.7813 19.36,37.6023 L19.974,37.8303 L20.058,38.5173 L20.058,62.3953 Z"
                />
              </g>
            </svg>
          </div>
          <h4 class="custom-block__title">Bittrex Exchange</h4> </a>
      </div>
      <div class="column column--xs-4 custom-block">
        <a class="custom-block__link" href="https://support.bittrex.com/hc/en-us/requests">
          <div class="custom-block__icon">
            <svg width="66px" height="66px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
        <g fill="#231F20">
            <path d="M49.5756,21.4295 C50.1536,24.0885 48.6626,26.1035 45.1856,27.4365 C43.8586,32.7745 41.1896,36.1745 38.1016,38.0805 L38.1016,41.4665 C38.1016,41.6325 38.0576,41.7955 37.9746,41.9395 C37.8926,42.0815 35.9286,45.3995 31.0366,45.3995 L30.8866,45.3995 C27.4656,45.3995 25.4806,43.7795 24.5656,42.7555 C19.7826,46.0015 19.7326,46.0255 19.6156,46.0735 L18.7506,46.4255 C14.1096,48.3075 8.3776,50.6305 7.9286,58.0405 L37.3846,58.0405 L37.3846,59.9405 L6.9506,59.9405 C6.4256,59.9405 5.9996,59.5155 5.9996,58.9905 C5.9996,49.5445 12.9566,46.7245 18.0366,44.6655 L18.8176,44.3475 C19.2596,44.0565 21.8686,42.2895 23.8226,40.9635 L23.8226,38.0005 C20.6016,35.9365 18.3256,32.3545 17.1696,27.5155 C13.5466,26.1835 11.9946,24.1405 12.5836,21.4295 C12.9586,19.7045 15.0886,18.9645 16.3026,18.6795 C16.3146,18.0305 16.3376,17.3995 16.3846,16.6625 C17.3646,2.2485 25.9446,-0.0005 31.1846,-0.0005 C35.5986,-0.0005 45.7746,1.8545 46.0656,18.7325 C47.3006,19.0495 49.2206,19.7975 49.5756,21.4295 Z M18.1896,19.4675 C18.1896,31.3375 22.9256,38.1455 31.1846,38.1455 C40.7756,38.1455 44.1796,28.0835 44.1796,19.4675 C44.1796,18.8815 44.1596,18.3165 44.1346,17.7575 C36.7256,17.0315 32.6756,14.9555 30.8126,13.6565 C30.2446,14.5055 29.1326,15.2605 27.4426,15.9495 C23.6286,17.5025 19.9376,17.7365 18.2326,17.7505 C18.2086,18.3125 18.1896,18.8785 18.1896,19.4675 Z M36.2006,41.1785 L36.2006,39.0505 C34.5466,39.7315 32.8326,40.0455 31.1846,40.0455 C29.1806,40.0455 27.3616,39.6705 25.7226,38.9845 L25.7226,41.1785 C26.1526,41.7625 27.6996,43.4995 30.8866,43.4995 L31.0366,43.4995 C34.2226,43.4995 35.7726,41.7595 36.2006,41.1785 Z M43.9946,15.8285 C43.0786,6.8165 38.6166,1.9005 31.1846,1.9005 C23.6286,1.9005 19.2476,6.7095 18.3646,15.8445 C19.9746,15.8155 23.3296,15.5725 26.7256,14.1895 C28.9236,13.2935 29.3486,12.5215 29.3516,12.3305 C29.1666,11.9255 29.2876,11.4395 29.6536,11.1705 C30.0496,10.8815 30.6006,10.9395 30.9266,11.3065 C30.9546,11.3375 34.2566,14.8025 43.9946,15.8285 Z M16.3216,20.6405 C15.4206,20.9065 14.5476,21.3405 14.4406,21.8335 C14.2956,22.4995 13.9836,23.9555 16.7326,25.2825 C16.5046,23.8285 16.3666,22.2795 16.3216,20.6405 Z M47.7186,21.8335 C47.6206,21.3815 46.8766,20.9785 46.0586,20.7095 C46.0106,22.3115 45.8676,23.7945 45.6456,25.1705 C48.1536,23.8745 47.8596,22.4815 47.7186,21.8335 Z M41.143,41.9334 L59.531,41.9334 C60.056,41.9334 60.481,42.3584 60.481,42.8834 L60.481,64.9504 C60.481,65.4754 60.056,65.9004 59.531,65.9004 L41.143,65.9004 C40.618,65.9004 40.192,65.4754 40.192,64.9504 L40.192,42.8834 C40.192,42.3584 40.618,41.9334 41.143,41.9334 Z M42.093,64.0004 L58.581,64.0004 L58.581,43.8334 L42.093,43.8334 L42.093,64.0004 Z M54.9337,48.4307 L45.7397,48.4307 C45.2147,48.4307 44.7897,48.0057 44.7897,47.4807 C44.7897,46.9557 45.2147,46.5307 45.7397,46.5307 L54.9337,46.5307 C55.4587,46.5307 55.8847,46.9557 55.8847,47.4807 C55.8847,48.0057 55.4587,48.4307 54.9337,48.4307 Z M54.9337,53.9473 L45.7397,53.9473 C45.2147,53.9473 44.7897,53.5223 44.7897,52.9973 C44.7897,52.4723 45.2147,52.0473 45.7397,52.0473 L54.9337,52.0473 C55.4587,52.0473 55.8847,52.4723 55.8847,52.9973 C55.8847,53.5223 55.4587,53.9473 54.9337,53.9473 Z M51.2567,59.4639 L45.7397,59.4639 C45.2147,59.4639 44.7897,59.0389 44.7897,58.5139 C44.7897,57.9889 45.2147,57.5639 45.7397,57.5639 L51.2567,57.5639 C51.7817,57.5639 52.2067,57.9889 52.2067,58.5139 C52.2067,59.0389 51.7817,59.4639 51.2567,59.4639 Z"
                />
              </g>
            </svg>
          </div>
          <h3 class="custom-block__title">My Support Tickets</h3>
      </div>
    </div>

    
    <div class="knowledge-base">
      <ul class="row category-list clearfix" id="category-list">
        
          <li class="column column--sm-4 category-list-item">
            <a class="category-list-item__link" href="/hc/en-us/categories/200049234-FAQ-Frequently-Asked-Questions">
              <h2 class="h5 category-list-item__title">FAQ - Frequently Asked Questions</h2>
            </a>
          </li>
        
          <li class="column column--sm-4 category-list-item">
            <a class="category-list-item__link" href="/hc/en-us/categories/200236600-News-and-Announcements">
              <h2 class="h5 category-list-item__title">News and Announcements</h2>
            </a>
          </li>
        
          <li class="column column--sm-4 category-list-item">
            <a class="category-list-item__link" href="/hc/en-us/categories/202219128-Coin-Information">
              <h2 class="h5 category-list-item__title">Coin Information</h2>
            </a>
          </li>
        
      </ul>

      <section class="promoted-articles">
        <h4 class="promoted-articles__title">Promoted articles</h4>
        <ul class="promoted-articles__list row">
          
            <li class="promoted-articles-item column column--xs-12 column--sm-6 column--md-4">
            <a class="promoted-articles-item__title" href="/hc/en-us/articles/115000198612-Troubleshooting-Two-Factor-Authentication-2FA-">Troubleshooting Two-Factor Authentication (2FA) </a>
            <p class="meta"> 
Two-Factor Troubleshooting Tip
The typical re...</p>
          
            <li class="promoted-articles-item column column--xs-12 column--sm-6 column--md-4">
            <a class="promoted-articles-item__title" href="/hc/en-us/articles/231701788-Withdraw-Limits-and-Troubleshooting">Withdraw Limits and Troubleshooting</a>
            <p class="meta">Bittrex enforces withdrawal limits to protect c...</p>
          
            <li class="promoted-articles-item column column--xs-12 column--sm-6 column--md-4">
            <a class="promoted-articles-item__title" href="/hc/en-us/articles/115000235552-How-do-I-perform-a-trade-">How do I perform a trade?</a>
            <p class="meta">This video shows you how to deposit and buy coi...</p>
          
            <li class="promoted-articles-item column column--xs-12 column--sm-6 column--md-4">
            <a class="promoted-articles-item__title" href="/hc/en-us/articles/115000233331-Where-is-my-withdrawal-">Where is my withdrawal?</a>
            <p class="meta">Here are some common issues seen with withdrawa...</p>
          
            <li class="promoted-articles-item column column--xs-12 column--sm-6 column--md-4">
            <a class="promoted-articles-item__title" href="/hc/en-us/articles/115000223912-Where-is-my-deposit-">Where is my deposit?</a>
            <p class="meta">We cannot credit transactions until they have t...</p>
          
            <li class="promoted-articles-item column column--xs-12 column--sm-6 column--md-4">
            <a class="promoted-articles-item__title" href="/hc/en-us/articles/115001970131-Set-up-Two-Factor-2FA-">Set up Two-Factor (2FA)</a>
            <p class="meta">Two-factor authentication is an extra layer of ...</p>
          
            <li class="promoted-articles-item column column--xs-12 column--sm-6 column--md-4">
            <a class="promoted-articles-item__title" href="/hc/en-us/articles/360000474232-Bittrex-Market-Token-Removal-Policy">Bittrex Market &amp; Token Removal Policy</a>
            <p class="meta">To help drive innovation in the blockchain indu...</p>
          
            <li class="promoted-articles-item column column--xs-12 column--sm-6 column--md-4">
            <a class="promoted-articles-item__title" href="/hc/en-us/articles/360000475411-How-do-I-submit-a-token-to-Bittrex-for-listing-">How do I submit a token to Bittrex for listing?</a>
            <p class="meta">To help drive innovation in the blockchain indu...</p>
          
            <li class="promoted-articles-item column column--xs-12 column--sm-6 column--md-4">
            <a class="promoted-articles-item__title" href="/hc/en-us/articles/115000961172-Bittrex-s-Crosschain-Recovery-Policy">Bittrex&#39;s Crosschain Recovery Policy</a>
            <p class="meta">Bittrex's cross chain recovery policy is that w...</p>
          
          </li>
        </ul>
     </section>

      <div class="category-tree" id="category-tree">
        
          <section class="category-tree-item">
            
              <h2 class="category-tree-item__title">
                <a class="category-tree-item__title-link" href="/hc/en-us/categories/200049234-FAQ-Frequently-Asked-Questions">FAQ - Frequently Asked Questions</a>
              </h2>
            

            <div class="category-tree-item__sections row clearfix">
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/115000043771-Common-Errors-and-Troubleshooting-Tips">Common Errors and Troubleshooting Tips</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item article-list-item--is-promoted" >
                          
                            <span class="fa fa-star" title="Promoted article"></span>
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000198612-Troubleshooting-Two-Factor-Authentication-2FA-">Troubleshooting Two-Factor Authentication (2FA) </a>
                        </li>
                      
                        <li class="article-list-item article-list-item--is-promoted" >
                          
                            <span class="fa fa-star" title="Promoted article"></span>
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/231701788-Withdraw-Limits-and-Troubleshooting">Withdraw Limits and Troubleshooting</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115002224092-How-do-I-get-invited-to-the-Bittrex-Slack-channel-">How do I get invited to the Bittrex Slack channel?</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115001324851-Not-receiving-email-from-Bittrex">Not receiving email from Bittrex</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000240791-Error-Codes-Troubleshooting-common-error-codes">Error Codes - Troubleshooting common error codes</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000220872-Logon-issues-Captcha-500-uh-oh-something-went-wrong">Logon issues Captcha, 500 uh oh something went wrong</a>
                        </li>
                      
                    </ul>
                    
                  
                </section>
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/200692094-Trading">Trading</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item article-list-item--is-promoted" >
                          
                            <span class="fa fa-star" title="Promoted article"></span>
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000235552-How-do-I-perform-a-trade-">How do I perform a trade?</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115003004171-What-are-my-trade-limits-">What are my trade limits?</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000762772-Margin-Trading-Availability">Margin Trading Availability</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000199651-What-fees-does-Bittrex-charge-">What fees does Bittrex charge?</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/235780368-Why-is-the-total-price-an-estimate-in-the-trade-confirmation-dialog-">Why is the total price an estimate in the trade confirmation dialog?</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/202227464-What-is-Time-in-Force-">What is &quot;Time in Force?&quot;</a>
                        </li>
                      
                    </ul>
                    
                      <a href="/hc/en-us/sections/200692094-Trading" class="btn btn--default see-all-articles">
                        See all 7 articles
                      </a>
                    
                  
                </section>
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/200710250-Wallets">Wallets</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item article-list-item--is-promoted" >
                          
                            <span class="fa fa-star" title="Promoted article"></span>
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000233331-Where-is-my-withdrawal-">Where is my withdrawal?</a>
                        </li>
                      
                        <li class="article-list-item article-list-item--is-promoted" >
                          
                            <span class="fa fa-star" title="Promoted article"></span>
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000223912-Where-is-my-deposit-">Where is my deposit?</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115001919932-Claims-Stakes-Promotions-Giveaways-Forks-and-Airdrops">Claims, Stakes, Promotions, Giveaways, Forks and Airdrops</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115001293992-Purchase-USDT-via-Wire-Transfer">Purchase USDT via Wire Transfer</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000233911-What-does-it-mean-when-a-wallet-is-in-maintenance-">What does it mean when a wallet is in maintenance?</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000199631-How-do-I-deposit-coins-to-Bittrex-">How do I deposit coins to Bittrex?</a>
                        </li>
                      
                    </ul>
                    
                      <a href="/hc/en-us/sections/200710250-Wallets" class="btn btn--default see-all-articles">
                        See all 10 articles
                      </a>
                    
                  
                </section>
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/203645818-User-Accounts">User Accounts</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item article-list-item--is-promoted" >
                          
                            <span class="fa fa-star" title="Promoted article"></span>
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115001970131-Set-up-Two-Factor-2FA-">Set up Two-Factor (2FA)</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115001352132-Why-is-my-account-disabled-">Why is my account disabled?</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115001262712-Multiple-Account-Policy">Multiple Account Policy</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115005329167-Creating-a-Bittrex-Account-and-Performing-Verification">Creating a Bittrex Account and Performing Verification</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/215282838-Frequently-asked-questions-about-account-verification">Frequently asked questions about account verification</a>
                        </li>
                      
                    </ul>
                    
                  
                </section>
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/200142404-Security">Security</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000233391-Account-Hacked-Unauthorized-Login">Account Hacked/Unauthorized Login</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115005595768-IP-and-Crypto-Address-Whitelisting">IP and Crypto Address Whitelisting</a>
                        </li>
                      
                    </ul>
                    
                  
                </section>
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/115000090271-ETH-ETH-Tokens-Common-Issues">ETH/ETH Tokens Common Issues</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000393752-Blockchain-Congestion-Detected">Blockchain Congestion Detected</a>
                        </li>
                      
                    </ul>
                    
                  
                </section>
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/200710240-Coin-Developers">Coin Developers</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item article-list-item--is-promoted" >
                          
                            <span class="fa fa-star" title="Promoted article"></span>
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/360000474232-Bittrex-Market-Token-Removal-Policy">Bittrex Market &amp; Token Removal Policy</a>
                        </li>
                      
                        <li class="article-list-item article-list-item--is-promoted" >
                          
                            <span class="fa fa-star" title="Promoted article"></span>
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/360000475411-How-do-I-submit-a-token-to-Bittrex-for-listing-">How do I submit a token to Bittrex for listing?</a>
                        </li>
                      
                    </ul>
                    
                  
                </section>
              
            </div>

            
          </section>
        
          <section class="category-tree-item">
            
              <h2 class="category-tree-item__title">
                <a class="category-tree-item__title-link" href="/hc/en-us/categories/200236600-News-and-Announcements">News and Announcements</a>
              </h2>
            

            <div class="category-tree-item__sections row clearfix">
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/200142394-Announcements">Announcements</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item article-list-item--is-promoted" >
                          
                            <span class="fa fa-star" title="Promoted article"></span>
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000961172-Bittrex-s-Crosschain-Recovery-Policy">Bittrex&#39;s Crosschain Recovery Policy</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/360000857772-ETH-ETH-Token-Single-Address">ETH/ETH Token Single Address</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115003463331-New-Account-Sign-Ups">New Account Sign Ups</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115003209552-Important-Information-About-Identity-Verification">Important Information About Identity Verification</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115002187632-Statement-on-disabled-accounts">Statement on disabled accounts</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115001499832-Self-Service-Account-Recovery">Self-Service Account Recovery</a>
                        </li>
                      
                    </ul>
                    
                      <a href="/hc/en-us/sections/200142394-Announcements" class="btn btn--default see-all-articles">
                        See all 23 articles
                      </a>
                    
                  
                </section>
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/200560334-Coin-Removals">Coin Removals</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/360000985452-Pending-Market-Removals-3-2-2018">Pending Market Removals 3/2/2018</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/360000560051-Pending-Market-Removals-2-9-2018">Pending Market Removals 2/9/2018</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115003977472-Pending-Market-Removals-1-26-2018">Pending Market Removals 1/26/2018</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115003816652-Pending-Market-Removals-1-12-2018">Pending Market Removals 1/12/2018</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115003472251-Pending-Market-Removals-12-22-2017">Pending Market Removals 12/22/2017</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115003346031-Pending-Market-Removals-12-15-2017">Pending Market Removals 12/15/2017</a>
                        </li>
                      
                    </ul>
                    
                      <a href="/hc/en-us/sections/200560334-Coin-Removals" class="btn btn--default see-all-articles">
                        See all 100 articles
                      </a>
                    
                  
                </section>
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/200567324-Changelist">Changelist</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115000152972-Changelist-05-09-17">Changelist 05/09/17</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/203302154-Changelist-10-28-14">Changelist 10/28/14</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/202998344-Changelist-09-16-14">Changelist 09/16/14</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/203185350-Changelist-09-09-14">Changelist 09/09/14</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/203150570-Changelist-09-02-14">Changelist 09/02/14</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/203147780-Changelist-08-31-14">Changelist 08/31/14</a>
                        </li>
                      
                    </ul>
                    
                      <a href="/hc/en-us/sections/200567324-Changelist" class="btn btn--default see-all-articles">
                        See all 11 articles
                      </a>
                    
                  
                </section>
              
            </div>

            
          </section>
        
          <section class="category-tree-item">
            
              <h2 class="category-tree-item__title">
                <a class="category-tree-item__title-link" href="/hc/en-us/categories/202219128-Coin-Information">Coin Information</a>
              </h2>
            

            <div class="category-tree-item__sections row clearfix">
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/360000045871-Status">Status</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/360000203372-NEO-update-01-22-2018">NEO update - 01/22/2018</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/360000203352-IGNIS-and-SWIFT-update-01-22-2018">IGNIS and SWIFT update - 01/22/2018</a>
                        </li>
                      
                    </ul>
                    
                  
                </section>
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/203283828-Information">Information</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115003571832-Statement-on-Ignis-IGNIS-airdrop-to-Nxt-NXT-holders">Statement on Ignis [IGNIS] airdrop to Nxt [NXT] holders</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115003506331-Support-for-the-Bitswift-SWIFT-blockchain-upgrade">Support for the Bitswift [SWIFT] blockchain upgrade</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115003175391-Support-for-the-Ardor-ARDR-blockchain-upgrade">Support for the Ardor [ARDR] blockchain upgrade</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115003177652-Support-for-the-GeoCoin-GEO-blockchain-upgrade">Support for the GeoCoin [GEO] blockchain upgrade</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115002603211-Statement-on-Bitcoin-Segwit2x-hard-fork">Statement on Bitcoin Segwit2x hard fork</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/115002320451-Statement-on-Bitcoin-Gold-BTG-">Statement on Bitcoin Gold [BTG]</a>
                        </li>
                      
                    </ul>
                    
                      <a href="/hc/en-us/sections/203283828-Information" class="btn btn--default see-all-articles">
                        See all 22 articles
                      </a>
                    
                  
                </section>
              
                <section class="column column--sm-6 column--md-4 section">
                  <h3 class="section__title">
                    
                    <a class="section__title-link" href="/hc/en-us/sections/203283818-Crowd-Fund-Campaigns">Crowd Fund Campaigns</a>
                  </h3>
                  
                    <ul class="article-list">
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/227537268-eBoost-EBST-Crowdfund-Campaign">eBoost (EBST) Crowdfund Campaign</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/226915648-Blocksafe-Foundation-Crowdfund-Campaign">Blocksafe Foundation Crowdfund Campaign</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/218843307-EDON-Crowdfund-Campaign">EDON Crowdfund Campaign</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/207166757-R3D-CrowdFund-Campaign">R3D CrowdFund Campaign</a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/204504474-Gambit-GAM-Crowdfund-Campaign-">Gambit GAM Crowdfund Campaign </a>
                        </li>
                      
                        <li class="article-list-item " >
                          
                          <a class="article-list-item__link" href="/hc/en-us/articles/204259220-Social-Media-Advertisement-Coin-SMAC-Crowdfund-Campaign">Social Media Advertisement Coin (SMAC) Crowdfund Campaign</a>
                        </li>
                      
                    </ul>
                    
                      <a href="/hc/en-us/sections/203283818-Crowd-Fund-Campaigns" class="btn btn--default see-all-articles">
                        See all 10 articles
                      </a>
                    
                  
                </section>
              
            </div>

            
          </section>
        
      </div>
    </div>
    

    

    <section class="footer-submit-ticket">
      <h2 class="footer-submit-ticket__title">Can't find what you're looking for?</h2>
      <h3 class="footer-submit-ticket__subtitle">Let us help you!</h3>
      <p class="footer-submit-ticket__btn">
        <a class="btn btn--success" href="/hc/en-us/requests/new">Submit a request</a>
      </p>
    </section>
  </div>
</div>

  </main>

  </div>
<!-- /.layout -->
<footer class="footer container">
  <div class="container-inner footer__inner">
    <div class="footer__col copyright">
      <p>&copy; Bittrex Support</p>
    </div>
    <div class="footer__col footer__col--social-links">
      <a href="https://www.facebook.com/bittrex" target="_blank" class="footer-social-link fa fa-facebook"></a>
      <a href="https://twitter.com/BittrexExchange" target="_blank" class="footer-social-link fa fa-twitter"></a>
    </div>
  </div>
</footer>
<a href="#" class="scroll-to-top fa fa-angle-up" data-scroll-to-top></a>



  <!-- / -->

  <script type="text/javascript" src="//p13.zdassets.com/hc/assets/locales/en-us-e9636f54e909a07cbf366cd8414b9cff.js"></script>
  <script src="https://bittrex.zendesk.com/auth/v2/host.js" data-brand-id="149544" data-return-to="https://support.bittrex.com/hc/en-us" data-theme="hc" data-locale="1" data-auth-origin="149544,true,true"></script>
  <script type="text/javascript" src="https://p13.zdassets.com/assets/zendesk_pci_hc.v4.js"></script>

  <script type="text/javascript">
  /*

    Greetings sourcecode lurker!

    This is for internal Zendesk and legacy usage,
    we don't support or guarantee any of these values
    so please don't build stuff on top of them.

  */

  HelpCenter = {};
  HelpCenter.account = {"subdomain":"bittrex","environment":"production","name":"Bittrex Support"};
  HelpCenter.user = {"identifier":"da39a3ee5e6b4b0d3255bfef95601890afd80709","email":null,"name":null,"role":"anonymous","avatar_url":"https://assets.zendesk.com/hc/assets/default_avatar.png","organizations":[],"groups":[]};
  HelpCenter.internal = {"asset_url":"//p13.zdassets.com/hc/assets/","current_session":{"locale":"en-us","csrf_token":"dgKJw/eXQotcbWNbgkjYzQGwjMHmvCVxGy1mxx3RuOlckqxDsCG4qJjUpSchzcN2gxzr+MOVRZCyd2/YURdPuw==","shared_csrf_token":null},"settings":{"zopim_enabled":true,"spam_filter_enabled":true},"current_record_id":null,"current_record_url":null,"current_record_title":null,"search_results_count":null,"current_text_direction":"ltr","current_brand_url":"https://bittrex.zendesk.com","current_host_mapping":"support.bittrex.com","current_path":null,"authentication_domain":"https://bittrex.zendesk.com","show_autocomplete_breadcrumbs":true,"user_info_changing_enabled":false,"has_user_profiles_enabled":true,"has_anonymous_kb_voting":false,"has_advanced_upsell":false,"has_multi_language_help_center":true,"mobile_device":false,"mobile_site_enabled":false,"show_at_mentions":false,"has_copied_content":false,"embeddables_config":{"embeddables_web_widget":false,"embeddables_automatic_answers":true,"embeddables_connect_ipms":false},"embeddables_domain":"zendesk.com","answer_bot_subdomain":"static","plans_url":"https://support.bittrex.com/hc/admin/plan?locale=en-us","manage_content_url":"https://support.bittrex.com/hc/en-us","arrange_content_url":"https://support.bittrex.com/hc/admin/arrange_contents?locale=en-us","general_settings_url":"https://support.bittrex.com/hc/admin/general_settings?locale=en-us","user_segments_url":"https://support.bittrex.com/hc/admin/user_segments?locale=en-us","import_articles_url":"https://support.bittrex.com/hc/admin/import_articles?locale=en-us","has_community_enabled":false,"has_multiselect_field":true,"has_groups":true,"has_internal_sections":true,"has_organizations":true,"has_tag_restrictions":true,"has_answer_bot_web_form_enabled":false,"has_answer_bot_embeddable_standalone":true,"billing_url":"/access/return_to?return_to=https://bittrex.zendesk.com/billing","has_answer_bot":true,"has_guide_docs_importer":false,"answer_bot_management_url":"https://support.bittrex.com/hc/admin/answer_bot?locale=en-us","is_account_owner":false,"has_theming_templates":false,"theming_center_url":"https://support.bittrex.com/theming","theming_cookie_key":"hc-da39a3ee5e6b4b0d3255bfef95601890afd80709-preview","is_preview":false};
</script>

  <script src="//p13.zdassets.com/hc/assets/hc_enduser-99ecbf6c6034819f1a2d9f538431319f.js"></script>
  

  <script type="text/javascript">
    (function() {
  var Tracker = {};

  Tracker.track = function(eventName, data) {
    var url = "https://support.bittrex.com/hc/tracking/events?locale=en-us";

    var payload = {
      "event": eventName,
      "data": data,
      "referrer": document.referrer
    };

    var xhr = new XMLHttpRequest();

    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    xhr.send(JSON.stringify(payload));
  };

    Tracker.track("front_page_viewed", "BAh7AA==--50d1974e05bddc24bd4b43467913737de44d3c7e");
})();

  </script>
</body>
</html>