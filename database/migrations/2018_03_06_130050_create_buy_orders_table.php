<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_orders', function (Blueprint $table) {
            $table->increments('id');
           
            $table->integer('buyer_id')->unsigned();
            
            $table->foreign('buyer_id')
                  ->references('id')->on('users') // The id of the buyer coming from users table 
                  ->onDelete('cascade');

            // $table->foreign('seller_id')
            //        ->reference('id')->on('user')   // The Id of the seller coming from users table
            //        ->onDelete('cascade');
            $table->integer('loan_rate')->default(2); // The loan rate default 2
            $table->integer('bought_amount'); // The amount the buyer bought;
            $table->integer('bought_price'); // The price at 
            
            $table->smallInteger('status') ;// 1= Completed, 2 = partially completed, 3= canceled 
         

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyers');
    }
}
