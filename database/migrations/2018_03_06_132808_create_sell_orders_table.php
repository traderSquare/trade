<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('seller_id')->unsigned();
            
            $table->integer('sell_price'); // The price at which the trader want to sell one asset
            $table->integer('sell_amount');// The amount to sell 
            $table->integer('loan_rate')->default(2); // The loan rate default 2

            $table->foreign('seller_id')  // The id of the user who's placing the sell order
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            
            $table->smallInteger('status'); 
            $table->integer('pair_id'); 


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_orders');
    }
}
