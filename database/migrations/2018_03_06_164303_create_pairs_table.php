<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pairs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pair_name')->unique();
            $table->integer('last_price');
            $table->integer('market_rate');
            
            $table->integer('24_hour_last_price');
            $table->integer('15_min_last_price');
            $table->integer('pair_key');
            $table->dateTime('last_trade'); // The date at which the latest trade was made
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pair');
    }
}
