<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([ 
            'email' => 'test@trade.com', 
            'password'=> Hash::make('user1'), 
            'permissions'=> 'user',
             'first_name' => 'user1_name', 
             'last_name' => 'user1_last_name' ]);
       
             DB::table('users')->insert([ 
                'email' => 'test1@trade.com', 
                'password'=> Hash::make('user2'), 
                'permissions'=> 'user',
                 'first_name' => 'user2_name', 
                 'last_name' => 'user2_last_name' ]);

            //TODO : make also a migration to set user as activated or not inside the "activations" table
    }
}
